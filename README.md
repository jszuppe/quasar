# **quasar** #
---

Program for fast analisys of astronomical objects' spectra.

## Description ##
---

**quasar** can:

* Load FITS files (with astronomical objects's spectra) to database.
* Perform analisys of astronomical objects' spectra.

## Requirements ##
---

* [boost] - version 1.55 or newer (1.55, 1.56 tested).
* [Mongo C++ driver] - version 1.0.0-rc0 of the legacy driver or newer (1.0.0-rc0 tested). Compiled with c++11 flag.
* OpenCL library and headers ([AMD APP SDK 2.9.1](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/) tested).
* cl.hpp (OpenCL C++ Bindings) version 1.2.6 (in AMD APP SDK there is version 1.2.5 and you need to replace it with [this](https://www.khronos.org/registry/cl/api/1.2/cl.hpp) from khronos ([1.2.6 API](https://www.khronos.org/registry/cl/specs/opencl-cplusplus-1.2.pdf)).
* [CFITSIO] - version 3.37 or newer (3.37 tested).
* [CCfits] - version 2.4 or newer (2.4 tested).
* [turtle] - version 1.26 or newer (1.26 tested).

## Installation ##
---

### Linux ###

```
git clone git@bitbucket.org:jszuppe/quasar.git
# or git clone https://<your-username>@bitbucket.org/jszuppe/quasar.git
cd quasar
```

Change paths to libraries and headers in Makefile if needed.

```
make
./bin/quasar --help
```

## Running tests ##
---

* Online MongoDB database on localhost:27017 is needed to run certain tests.
* MONGODB_TEST_PERFORMANCE macro definition is needed to run MongoDB performance tests (see Makefile or test/test_defines.hpp).
* LOAD_FILES_TO_DB_TEST_PERFORMANCE macro definition is needed to run loading astr. obj. FITS files to database task performance tests (see Makefile or test/test_defines.hpp).

### Linux ###

```
make test # build and start tests
# -- or --
make test_norun  # build
./bin/test  # start tests
```

## Usage ##
---

```
quasar --help
quasar --task_help task-name
quasar database-address [options] [--task task-name [task-options]]
```

*database-address* can be:

| database-address | Description                    |
| ------------- | ------------------------------ |
| `foo`      | foo database on localhost:27017.   |
| `192.168.0.10/foo`   | foo database on 192.168.0.10:27017     |
| `192.168.0.10:9999/foo`   | foo database on 192.168.0.10:9999     |

*task* can be:

* load - loading astronomical objects from files to database.
* para - start parameterization process for astronomical object set.


## License ##
---

To be determined

[CFITSIO]:http://heasarc.gsfc.nasa.gov/fitsio/fitsio.html
[CCfits]:http://heasarc.gsfc.nasa.gov/fitsio/CCfits/
[boost]:http://www.boost.org/
[Mongo C++ driver]:https://github.com/mongodb/mongo-cxx-driver
[turtle]:http://turtle.sourceforge.net/
