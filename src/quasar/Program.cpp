#include "Program.hpp"

Program::Program(int argc, char * argv[]) :
			argc(argc),
			argv(argv),
			name("quasar"),
			general(boost::program_options::options_description("General options")),
			special(boost::program_options::options_description("Special options")),
			allowed(boost::program_options::options_description("All options"))
{
	setupOptions();
}

Program::~Program()
{

}

void Program::setupOptions()
{
	namespace po = boost::program_options;

	this->general.add_options()
	("help,h",
			"Produce help message.")
	("task_help",
			po::value<TaskType>()->value_name("task-name"),
			"Produce help message with task options description.")
	("task,t",
			po::value<TaskType>()->value_name("task-name"),
			"Task to execute: load [loadFilesToDb] or para [parameterization].")
			;

	this->special.add_options()
	("verbose,v",
			po::bool_switch()->default_value(false),
			"Be more verbose; show extra informations.")
//	("silent,s",
//			po::bool_switch()->default_value(false),
//			"Run silently.")
			;

	this->allowed.add(general).add(special);

	this->hidden.add_options()
	("database_address",
			po::value<db::connectionParams>()->required(),
			"Full database address.")
	/* Opcja jest ukryta i nieużywana, bo obecnie jest jedna implementacja połączenia do bazy danych */
	("database_type",
			po::value<std::string>()->default_value("mongodb"),
			"Database type (default: mongodb).")
	("dummy",
			po::value<std::vector<std::string>>()->multitoken(),
			"Dummy to collect positional options.")
			;

	this->all.add(this->allowed).add(this->hidden);

	this->positional.add("database_address", 1);
	this->positional.add("dummy", -1);
}

int Program::run()
{
	namespace po = boost::program_options;
	try
	{
		po::variables_map vm;
		auto parsed_options = po::command_line_parser(this->argc, this->argv)
				.options(this->all)
				.positional(this->positional)
				.allow_unregistered()
				.run();
		po::store(parsed_options, vm);

		if (vm.count("help") || vm.count("task_help"))
		{
			std::cout << this->name << "\n";
			printUsage();
			std::cout << "\n" << this->general;
			std::cout << "\n" << this->special;
			if (vm.count("task_help"))
			{
				auto task_options_parser = TaskOptionsFactory().getParser(vm["task_help"].as<TaskType>());
				auto task_options = task_options_parser->getOptionsDescription();
				std::cout << "\n" << task_options;
			}
			std::cout << std::endl;
			return EXIT_SUCCESS;
		}

		// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
		// jeżeli coś jest nie tak z opcjami uruchomienia.
		po::notify(vm);

		bool verbose = false;
		if (vm["verbose"].as<bool>())
		{
			verbose = true;
		}

//		if (vm["silent"].as<bool>())
//		{
//			// SILENT
//		}

		// Testowanie połączenia
		if (!vm.count("task") && vm.count("database_address"))
		{
			try
			{
				auto cp = vm["database_address"].as<db::connectionParams>();
				auto c = db::buildConnection(cp);
				c->connect();
				std::cout << "Connection test successful.\n";

				return EXIT_SUCCESS;
			}
			catch (const std::exception& e)
			{
				std::cout << "Connection test failed.\n";
				if (verbose)
				{
					common::print_exception(e);
				}
			}
		}

		// Wywołanie zadania
		if (vm.count("task") && vm.count("database_address"))
		{
			try
			{
				auto task_options = po::collect_unrecognized(
						parsed_options.options,
						po::collect_unrecognized_mode::include_positional);
				// database-address przekazuje dalej
				//task_options.erase(task_options.begin());

				auto taskType = vm["task"].as<TaskType>();

				std::cout << "Trying to perform task: " << to_string(taskType) << std::endl;

				auto taskOptions = TaskOptionsFactory().create(taskType, task_options);
				auto task = TaskFactory().create(taskType, taskOptions);
				task->execute();

				return EXIT_SUCCESS;
			} catch (const std::exception& e)
			{
				std::cout << "\nProblem occurred: " << e.what() << "\n";
				std::cout << "Use --help option to see help.\n";
				if (verbose)
				{
					common::print_exception(e);
				}
			}
		}

		return EXIT_FAILURE;
	}
	catch (po::error& e)
	{
		std::cout << "\nProgram options error occurred: " << e.what() << "\n";
		std::cout << "Use --help option to see help.\n";
		return EXIT_FAILURE;
	}
}

void Program::printUsage()
{
	std::cout << "USAGE:"
			<< "\t" << this->name << " --help\n"
			<< "\t" << this->name << " database-address [options] [-t task [task-options]] \n";

	std::cout << "\ndatabase-address can be:\n";
	std::cout.width(25);
	std::cout << std::left << "  foo";
	std::cout << " foo database on localhost:27017\n";
	std::cout.width(25);
	std::cout << std::left << "  192.168.0.10/foo";
	std::cout << " foo database on 192.168.0.10:27017\n";
	std::cout.width(25);
	std::cout << std::left << "  192.168.0.10:9999/foo";
	std::cout << " foo database on 192.168.0.10:9999\n";

	std::cout << "\ntask can be:\n";
	std::cout << "  load - loading astronomical objects from files to database.\n";
	std::cout << "  para - start parameterization process for astronomical object set.\n";
}
