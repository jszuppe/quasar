#pragma once

#include <vector>
#include <memory>
#include <string>
#include <CL/cl.hpp>

/**
 * Klasa definiująca pojedynczy obiekt astronomiczny.
 *
 */
class Quasar
{

	public:

		/**
		 * Rodzaj liczby zmiennoprzecinkowej używanej do opisy widma kwazaru.
		 */
		typedef cl_float QFLOAT;
		/**
		 * Typ danych opisujący widmo kwazaru.
		 */
		typedef std::vector<QFLOAT> qdata;

		/**
		 * Struktura zawierająca parametry kwazaru.
		 * TODO: Dokładniejszy opis parametrów.
		 */
		struct qparams
		{
				std::string name;
				std::string type;

				float z;
				/**
				 * Right ascension
				 */
				float ra;
				/**
				 * Declination
				 */
				float dec;

				/**
				 * Modified Julian Date
				 */
				unsigned int mjd;
				unsigned int plate;
				unsigned int fiber;
				float a;
				float b;
		}typedef qparams;

		typedef std::shared_ptr<qdata> sptr_qdata;
		typedef std::shared_ptr<qparams> sptr_qparams;

		Quasar(qdata *, qparams *);
		Quasar(sptr_qdata, sptr_qparams);

		Quasar(qdata *, qdata *, qparams *);
		Quasar(sptr_qdata, sptr_qdata, sptr_qparams);

		~Quasar(void);

		unsigned int getDataSize() const;
		sptr_qdata getData() const;
		sptr_qdata getError() const;
		sptr_qparams getParams() const;

		std::string getName() const;
		std::string getType() const;
		unsigned int getModifiedJulianDate() const;

	private:

		/**
		 * Widmo.
		 */
		sptr_qdata data;
		/**
		 * Błąd.
		 */
		sptr_qdata err;
		/**
		 * Parametry.
		 */
		sptr_qparams params;
};

