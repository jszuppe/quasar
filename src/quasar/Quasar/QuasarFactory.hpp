#pragma once

#include "Quasar.hpp"

#include "../common/common.hpp"

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include <CCfits/CCfits>

#include <memory>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>

/*
 *	Domyślny przewidywany maksymalny rozmiar widma kwazaru (inaczej - liczba punktów opisujących widmo).
 */
#define MAX_QUASAR_DATA_SIZE 4096

/**
 *	Fabryka obiektów Quasar.
 *	@see Quasar	
 */
namespace QuasarFactory
{

Quasar createQuasarFromFit(const boost::filesystem::path& filePath);

Quasar::qparams * _getQuasarParamsFromFit(std::shared_ptr<CCfits::FITS> fit);
Quasar::qdata * _getQuasarDataFromFit(std::shared_ptr<CCfits::FITS> fit);
Quasar::qdata * _getQuasarErrFromFit(std::shared_ptr<CCfits::FITS> fit);

}
