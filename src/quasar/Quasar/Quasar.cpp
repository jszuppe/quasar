#include "Quasar.hpp"

/**
 *
 * @param data wskaźnik na widmo kwazaru.
 * @param params wskaźnik na parametry opisujące kwazar.
 */
Quasar::Quasar(qdata * data, qparams * params) :
			data(sptr_qdata(data)),
			err(sptr_qdata(new qdata())),
			params(sptr_qparams(params))
{

}

Quasar::Quasar(sptr_qdata data, sptr_qparams params) :
			data(data),
			err(sptr_qdata(new qdata())),
			params(params)
{

}

Quasar::Quasar(qdata* data, qdata* err, qparams* params) :
			data(sptr_qdata(data)),
			err(sptr_qdata(err)),
			params(sptr_qparams(params))
{

}

Quasar::Quasar(sptr_qdata data, sptr_qdata err, sptr_qparams params) :
			data(data),
			err(err),
			params(params)
{

}

Quasar::~Quasar(void)
{

}

/**
 * @return liczba punktów opisujących widmo kwazaru.
 */
unsigned int Quasar::getDataSize() const
{
	return this->data->size();
}

Quasar::sptr_qdata Quasar::getData() const
{
	return this->data;
}

Quasar::sptr_qdata Quasar::getError() const
{
	return this->err;
}

Quasar::sptr_qparams Quasar::getParams() const
{
	return this->params;
}

std::string Quasar::getName() const
{
	return this->params->name;
}

std::string Quasar::getType() const
{
	return this->params->type;
}

unsigned int Quasar::getModifiedJulianDate() const
{
	return this->params->mjd;
}
