#include "QuasarFactory.hpp"

namespace QuasarFactory
{

template <typename From, typename To>
struct static_caster
{
    To operator()(From p) {return static_cast<To>(p);}
};

Quasar createQuasarFromFit(const boost::filesystem::path& filePath)
{
	std::shared_ptr<CCfits::FITS> file;
	file.reset(new CCfits::FITS(filePath.string(), CCfits::Read));
	return Quasar(_getQuasarDataFromFit(file), _getQuasarErrFromFit(file), _getQuasarParamsFromFit(file));
}

Quasar::qparams * _getQuasarParamsFromFit(std::shared_ptr<CCfits::FITS> fit)
{
	Quasar::qparams * params = new Quasar::qparams();

	int mjd, plate, fiber = 0;
	double a, b, z, ra, dec = 0;\
	std::string type;
	fit->pHDU().readKey("MJD", mjd);
	fit->pHDU().readKey("FIBERID", fiber);
	fit->pHDU().readKey("PLATEID", plate);
	fit->pHDU().readKey("COEFF0", b);
	fit->pHDU().readKey("COEFF1", a);
	fit->pHDU().readKey("Z", z);
	fit->pHDU().readKey("RAOBJ", ra);
	fit->pHDU().readKey("DECOBJ", dec);
	fit->pHDU().readKey("OBJTYPE", type);

	params->name = common::quasar::coord2name(ra, dec);
	params->type = type;
	params->a = static_cast<float>(a);
	params->b = static_cast<float>(b);
	params->z = static_cast<float>(z);
	params->ra = static_cast<float>(ra);
	params->dec = static_cast<float>(dec);
	params->mjd = static_cast<unsigned int>(mjd);
	params->plate = static_cast<unsigned int>(plate);
	params->fiber = static_cast<unsigned int>(fiber);

	return params;
}

Quasar::qdata * _getQuasarDataFromFit(std::shared_ptr<CCfits::FITS> fit)
{
	Quasar::qdata * data = new Quasar::qdata();

	std::valarray<double> fluxo;
	long n(fit->pHDU().axis(0));
	fit->pHDU().read(fluxo, {1,1}, n);

	fluxo *= 1.0e-17;
	data->reserve(n);
	std::transform(std::begin(fluxo), std::end(fluxo), std::back_inserter(*data), static_caster<double,float>());

	return data;
}

Quasar::qdata * _getQuasarErrFromFit(std::shared_ptr<CCfits::FITS> fit)
{
	Quasar::qdata * err = new Quasar::qdata();

	std::valarray<double> ferr;
	long n(fit->pHDU().axis(0));
	fit->pHDU().read(ferr, {1,3}, n);

	ferr *= 1.0e-17;
	err->reserve(n);
	std::transform(std::begin(ferr), std::end(ferr), std::back_inserter(*err), static_caster<double,float>());

	return err;
}

}
