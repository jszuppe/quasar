#pragma once

#include "QuasarCursor.hpp"
#include "Exception.hpp"

#include "../Quasar/Quasar.hpp"

#include <string>
#include <ctime>
#include <cstdint>
#include <memory>
#include <vector>

namespace db
{

/**
 * Klasa reprezentuje zestaw kwazarów po stronie bazy danych. Umożliwia:
 * <ul>
 * 	<li>Pobranie/dodanie kwazarów.</li>
 * 	<li>Pobranie/dodanie lambdy.</li>
 * </ul>
 * TODO: lepszy opis
 */
class QuasarSet
{
	public:
		QuasarSet(const std::string&, const std::time_t, const std::time_t, const size_t size);
		virtual ~QuasarSet();

		std::time_t getDate() const;
		const std::string& getDescription() const;
		std::time_t getInsertDate() const;
		const std::string& getName() const;
		size_t getSize() const;

		virtual std::shared_ptr<QuasarCursor> getQuasarCursor() = 0;
		virtual std::vector<Quasar> getQuasars(size_t n = 0) = 0;
		virtual void addQuasars(std::vector<Quasar>) = 0;

	protected:
		const std::string name;
		const std::time_t date;
		const std::time_t insertDate;
		const std::string description;
		/**
		 * Liczba kwazarów w zestawie.
		 */
		size_t size;

};

} /* namespace DB */
