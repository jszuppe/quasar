#pragma once

#include <stdexcept>

namespace db
{

/**
 * Reprezentuje błędy powstałe przy obsłudze bazy danych.
 */
class Exception: public std::exception
{
	public:
		Exception(const std::string&);
		virtual ~Exception();
		virtual const char* what() const noexcept;

	private:
		const std::string msg;
};

class RuntimeError: public Exception
{
	public:
		RuntimeError(const std::string&);
		virtual ~RuntimeError();
};

class LogicError: public Exception
{
	public:
		LogicError(const std::string&);
		virtual ~LogicError();
};

class NotFound: public RuntimeError
{
	public:
		NotFound(const std::string&);
		virtual ~NotFound();
};

class NotImplemented: public RuntimeError
{
	public:
		NotImplemented(const std::string& msg = "Not implemented yet.");
		virtual ~NotImplemented();
};

} /* namespace DB */
