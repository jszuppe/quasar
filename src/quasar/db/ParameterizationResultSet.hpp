#pragma once

#include "Exception.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/algorithm/specparameterization/SpecParameterization.hpp"
#include "quasar/algorithm/fefitwin/FeFitWin.hpp"

#include <vector>
#include <ctime>

class ParameterizationOptions;

namespace db
{

class ParameterizationResultSet
{
	public:
		typedef algorithm::ocl::SpecParameterization::Results ParaResults;

		struct ParameterizationOptions
		{
				/**
				 * Nazwa zestawu obiektów astronomicznych dla których przeprowadzona będzie parametryzacja.
				 */
				std::string setName;
				/**
				 * Liczba obiektów astronomicznych, dla których przeprowadzona będzie parametryzacja.
				 * Wartość zero oznacza parametryzacje wszystkich obiektów z zestawu.
				 */
				size_t size;
				/**
				 * Elementy do dopasowania.
				 */
				std::vector<SpectralLine> elements;
				std::string elementsFile;
				/**
				 * Okna widmowe do dopasowania kontinuum (zakresy długości fal w A).
				 */
				std::vector<cl_float2> continuumWindows;
				std::string continuumWindowsFile;
				/**
				 * Długość fali na jakiej podana będzie amplituda dopasowanej funkcji potęgowej.
				 */
				cl_float ampWavelength;
				/**
				 * Szablon żelaza.
				 */
				FeTemplate feTemplate;
				std::string feTemplateFile;
				/**
				 * Okna zdominowane przez emisje żelaza (zakresy długości fal w A).
				 */
				std::vector<cl_float2> feWindows;
				std::string feWindowsFile;
				/**
				 * Parametry dopasowania szablonu żelaza.
				 */
				algorithm::ocl::FeFitWin::FitParameters feFitParameters;
		};

		ParameterizationResultSet(const std::string& name, const std::time_t date,
				const ParameterizationOptions options, const std::string& description = "");
		virtual ~ParameterizationResultSet();

		virtual void saveResults(std::vector<Quasar> astroObjects, ParaResults results) = 0;

	protected:
		const std::string name;
		const std::time_t date;
		const ParameterizationOptions options;
		const std::string description;
};

} /* namespace db */
