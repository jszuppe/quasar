#pragma once

#include "QuasarSet.hpp"
#include "ParameterizationResultSet.hpp"
#include "db_type.hpp"

#include <memory>
#include <vector>

namespace db
{

typedef struct connectionParams
{
		std::string host;
		int port;
		std::string database;

		connectionParams()
		: host(""), port(0), database("")
		{

		}

		connectionParams(std::string host, int port, std::string database) :
					host(host), port(port), database(database)
		{

		}

} connectionParams;

/**
 *
 */
class Connection
{
	public:
		typedef std::vector<std::shared_ptr<QuasarSet>> QuasarSets;

		Connection(const connectionParams&);
		Connection(const std::string& host, const int port, const std::string& database);
		virtual ~Connection();

		/**
		 * Rozpoczyna połączenie.
		 */
		virtual void connect() = 0;

		/**
		 * Zwraca zestaw kwazarów o podanej nazwie.
		 * @param qs_name nazwa zestawu kwazarów.
		 * @return wskaźnik na zestaw kwazarów. Pusty wskaźnik w przypadku braku zestawu o podanej nazwie
		 */
		virtual std::shared_ptr<QuasarSet> getQuasarSetByName(const std::string& qs_name) = 0;

		/**
		 * Pobiera z bazy danych wszystkie zestawy kwazarów.
		 * @return wskaźnik na wektor zestawów kwazarów.
		 */
		virtual std::unique_ptr<QuasarSets> getQuasarSets() = 0;

		/**
		 * Zapisuje zestaw kwazarów w bazie danych.
		 * @param qs zestaw kwazarów do zapisuj.
		 */
		virtual void saveQuasarSet(std::shared_ptr<QuasarSet> qs) = 0;

		/**
		 * Usuwa zestaw kwazarów wraz z jego kwazarami i lambdą.
		 * @param qs zestaw kwazarów do usunięcia.
		 */
		virtual void deleteQuasarSet(std::shared_ptr<QuasarSet> qs) = 0;

		/**
		 * Tworzy i zwraca nowy, pusty zestaw kwazarów.
		 * @param qs_name nazwa nowego zestawu kwazarów.
		 * @param qs_date data.
		 * @param insertDate data dodania zestawu do bazy danych.
		 * @param size rozmiar zestawu kwazarów.
		 * @return wskaźnik na nowy zestaw kwazarów.
		 */
		virtual std::shared_ptr<QuasarSet> getNewQuasarSet(const std::string& qs_name, const std::time_t qs_date,
				const std::time_t insertDate = std::time(nullptr), const size_t size = 0) const = 0;

		/**
		 *
		 * @param prs
		 */
		virtual void saveParaResultSet(std::shared_ptr<ParameterizationResultSet> prs) = 0;

		/**
		 *
		 * @param name
		 * @param date
		 * @param size
		 * @param options
		 * @param description
		 * @return
		 */
		virtual std::shared_ptr<ParameterizationResultSet> getNewParaResultSet(const std::string& name,
				const std::time_t date, db::ParameterizationResultSet::ParameterizationOptions options,
				const std::string& description = "") const = 0;

		/**
		 * @return prawdę jeżeli połączenie działa; wpp. - fałsz.
		 */
		virtual bool isConnected() const = 0;

		/**
		 * @return typ bazy danych.
		 */
		virtual db::DBType getDBType() const = 0;

		const std::string& getDatabase() const;
		const std::string& getHost() const;
		int getPort() const;

	protected:
		const std::string host;
		const int port;
		/**
		 * Nazwa używanej bazy danych.
		 */
		const std::string database;

	private:
		void validateMembers() const;
};

}
/* namespace DB */
