#include "QuasarSet.hpp"

namespace db
{

/**
 * Konstruktor.
 * @param name nazwa zestawu kwazarów.
 * @param date data identyfikująca zestaw (np. utworzenia).
 * @param insertDate data wprowadzenia do bazy danych.
 * @param size liczba kwazarów w zestawie.
 */
QuasarSet::QuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
		const size_t size) :
			name(name),
			date(date),
			insertDate(insertDate),
			size(size)
{
	if (this->name.empty())
	{
		throw db::LogicError("QuasarSet name cannot be an empty string.");
	}
}

QuasarSet::~QuasarSet()
{

}

std::time_t QuasarSet::getDate() const
{
	return date;
}

const std::string& QuasarSet::getDescription() const
{
	return description;
}

std::time_t QuasarSet::getInsertDate() const
{
	return insertDate;
}

const std::string& QuasarSet::getName() const
{
	return name;
}

size_t QuasarSet::getSize() const
{
	return size;
}

} /* namespace DB */
