#include "Exception.hpp"

namespace db
{

Exception::Exception(const std::string& msg) : msg(msg)
{

}

Exception::~Exception()
{

}

const char* Exception::what() const noexcept
{
	return this->msg.c_str();
}


RuntimeError::RuntimeError(const std::string& msg) :
		 Exception(msg)
{

}

RuntimeError::~RuntimeError()
{

}

LogicError::LogicError(const std::string& msg) :
		 Exception(msg)
{

}

LogicError::~LogicError()
{

}

NotFound::NotFound(const std::string& msg) :
		 RuntimeError(msg)
{

}

NotFound::~NotFound()
{

}

NotImplemented::NotImplemented(const std::string& msg) :
		 RuntimeError(msg)
{

}

NotImplemented::~NotImplemented()
{

}

} /* namespace DB */
