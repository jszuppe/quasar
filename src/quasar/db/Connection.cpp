#include "Connection.hpp"

namespace db
{

/**
 *
 * @param params parametry połączenia: host, port i baza danych.
 */
Connection::Connection(const connectionParams& params) :
			host(params.host),
			port(params.port),
			database(params.database)
{
	validateMembers();
}

/**
 *
 * @param host nazwa hosta, na którym jest baza danych.
 * @param port port.
 * @param database nazwa bazy danych.
 */
Connection::Connection(const std::string& host, const int port, const std::string& database) :
			host(host),
			port(port),
			database(database)
{
	validateMembers();
}

Connection::~Connection()
{

}

void Connection::validateMembers() const
{
	if (this->host.empty())
	{
		throw db::LogicError("Host name cannot be an empty string.");

	}
	if (this->database.empty())
	{
		throw db::LogicError("Database name cannot be an empty string.");

	}
}

/**
 * Zwraca nazwę używanej bazy danych.
 * @return nazwa bazy danych.
 */
const std::string& Connection::getDatabase() const
{
	this->getDBType();
	return this->database;
}

const std::string& Connection::getHost() const
{
	return this->host;
}

int Connection::getPort() const
{
	return this->port;
}

}
