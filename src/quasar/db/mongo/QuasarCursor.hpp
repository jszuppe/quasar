#pragma once

#include "db_mongo.hpp"
#include "bson_helper.hpp"

#include "../QuasarCursor.hpp"

#include <stdexcept>

namespace db_mongo
{

class QuasarCursor: public db::QuasarCursor
{
	public:
		QuasarCursor(const std::shared_ptr<mongo::DBClientConnection>, const std::string&, const mongo::Query&);
		~QuasarCursor();

		std::vector<Quasar> next(const size_t);
		bool more();

	private:
		/**
		 * Kursor.
		 */
		const std::auto_ptr<mongo::DBClientCursor> cursor;
};

} /* namespace db_mongo */
