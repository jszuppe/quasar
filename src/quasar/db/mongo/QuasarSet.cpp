#include "QuasarSet.hpp"

namespace db_mongo
{

QuasarSet::QuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
		const size_t size) :
			db::QuasarSet(name, date, insertDate, size)
{

}

QuasarSet::QuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
		const size_t size, const mongo::OID& oid) :
			db::QuasarSet(name, date, insertDate, size)
{
	this->setOID(oid);
}

QuasarSet::QuasarSet(const mongo::BSONObj& obj) :
			db::QuasarSet(obj.getField("name").String(), common::to_time_t(obj.getField("date").Date()),
					common::to_time_t(obj.getField("insertDate").Date()),
						static_cast<unsigned long>(obj.getField("size").Long()))
{
	this->setOID(MONGO_COMMON_GET_OID(obj));
}

QuasarSet::QuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
		const size_t size, const mongo::OID& oid, const std::string& database,
		const std::shared_ptr<mongo::DBClientConnection> connection) :
			db::QuasarSet(name, date, insertDate, size)
{
	this->setOID(oid);
	this->database = database;
	this->connection = connection;
}

QuasarSet::QuasarSet(const mongo::BSONObj& obj, const std::string& database,
		const std::shared_ptr<mongo::DBClientConnection> connection) :
			db::QuasarSet(obj.getField("name").String(), common::to_time_t(obj.getField("date").Date()),
					common::to_time_t(obj.getField("insertDate").Date()),
						static_cast<unsigned long>(obj.getField("size").Long()))
{
	this->setOID(MONGO_COMMON_GET_OID(obj));
	this->database = database;
	this->connection = connection;
}

QuasarSet::~QuasarSet()
{

}

std::shared_ptr<db::QuasarCursor> QuasarSet::getQuasarCursor()
{
	std::string ns = common::namespaceBuilder({ this->database, _quasar_col_name() });
	auto query = mongo::BSONObjBuilder().append("quasar_set_oid", getOID()).obj();
	return std::shared_ptr<db::QuasarCursor>(new QuasarCursor(this->connection, ns, query));
}

/**
 * Pobiera n pierwszych z bazy danych kwazary należące do tego zestawu.
 * Dla n = 0 pobierane są wszystkie kwazary.
 * @param n liczba kwazarów do pobrania (domyślnie n = 0, tj. wszystkie kwazary).
 * @return
 */
std::vector<Quasar> QuasarSet::getQuasars(size_t n)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki.
	// Jeżeli tak to są opakowywane w DB::Exception.
	// Dodatkowo należy sprawdzać poprzez getLastError(), czy nie było błędów.
	// Niestety dokumentacja mongo-cxx-driver nie precyzuje, czy zawsze w razie błędów rzucane są wyjątki,
	// a różne pomoce wręcz sugerują, że nie jest tak.
	std::vector<Quasar> quasar_vector;
	try
	{
		std::string ns = common::namespaceBuilder({ this->database, _quasar_col_name() });

		auto query = mongo::BSONObjBuilder().append("quasar_set_oid", getOID()).obj();
		auto cursor = this->connection->query(ns, query);

		std::string e;
		if (!(e = this->connection->getLastError()).empty())
		{
			throw db::RuntimeError(e);
		}

		unsigned long long i = 0;
		const bool loadAll = n == 0;
		while (cursor->more() && (loadAll || i < n))
		{
			// nextSafe() rzuca wyjątek w przypadku błędu,
			// więc pomijam sekwencje ze sprawdzaniem getLastError().
			mongo::BSONObj q_bson = cursor->nextSafe();
			auto q = common::quasarFromBSONObj(q_bson);
			quasar_vector.push_back(*q);
			i++;
		}
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("QuasarSet::getQuasars failed."));
	}
	return quasar_vector;
}

/**
 * Dodaje do tego zestawu podane kwazary i zapisuje do bazy danych.
 * @param quasars wektor kwazarów.
 */
void QuasarSet::addQuasars(std::vector<Quasar> quasars)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki.
	// Jeżeli tak to są opakowywane w DB::Exception.
	// Dodatkowo należy sprawdzać poprzez getLastError(), czy nie było błędów.
	// Niestety dokumentacja mongo-cxx-driver nie precyzuje, czy zawsze w razie błędów rzucane są wyjątki,
	// a różne pomoce wręcz sugerują, że nie jest tak.
	auto quasars_i = quasars.cbegin();
	auto quasars_end = quasars.cend();
	int i = 0;

	std::vector<mongo::OID> insertedObjOIDs;

	try
	{
		while (quasars_i != quasars_end)
		{
			auto ns = common::namespaceBuilder({ this->database, _quasar_col_name() });

			// Obiekt Mongo
			auto obj = common::quasarToBSONObj(*quasars_i, getOID());

			// Zapis w bazie
			this->connection->insert(ns, *obj);

			// Zapis OID w wektorze zapisanych
			insertedObjOIDs.push_back(MONGO_COMMON_GET_OID((*obj)));

			std::string e;
			if (!(e = this->connection->getLastError()).empty())
			{
				throw db::RuntimeError(e);
			}

			quasars_i++;
			i++;
		}

		// Aktualizacja stanu QuasarSet
		this->size += quasars.size();
		update();

	} catch (const std::exception& e)
	{
		deleteQuasars(insertedObjOIDs);
		std::throw_with_nested(db::RuntimeError("QuasarSet::addQuasars failed."));
	}
}

/**
 * Zwraca obiekt BSONObj opisujący ten zestaw kwazarów.
 * @return obiekt BSONObj.
 */
mongo::BSONObj QuasarSet::getBSONObj() const
{
	using namespace mongo;
	BSONObjBuilder bob;
	if (!this->oid.isSet())
	{
		bob.genOID();
	}
	else
	{
		bob.append("_id", this->oid);
	}
	bob.append("name", this->name);
	bob.append("date", common::to_Date_t(date));
	bob.append("insertDate", common::to_Date_t(insertDate));
	bob.append("size", static_cast<long long>(this->size));
	return bob.obj();
}

const mongo::OID& QuasarSet::getOID() const
{
	if (!this->oid.isSet())
	{
		throw OIDInvalid();
	}
	return this->oid;
}

void QuasarSet::setOID(const mongo::OID oid)
{
	if (!oid.isSet())
	{
		throw OIDInvalid();
	}
	this->oid = oid;
}

void QuasarSet::setDatabase(const std::string& database)
{
	if (this->database.empty())
	{
		this->database = database;
	}
}

void QuasarSet::setConnection(const std::shared_ptr<mongo::DBClientConnection> connection)
{
	if (this->connection == nullptr)
	{
		this->connection = connection;
	}
}

/**
 * Zapisuje nowy stan zestawu do bazy danych.
 */
void QuasarSet::update()
{
	try
	{
		auto ns = common::namespaceBuilder({ this->database, _quasarset_col_name() });
		auto query = mongo::BSONObjBuilder().append("_id", getOID()).obj();
		this->connection->update(ns, query, getBSONObj());

		std::string e;
		if (!(e = this->connection->getLastError()).empty())
		{
			throw db::RuntimeError(e);
		}

	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("QuasarSet::update failed."));
	}
}

/**
 *
 */
void QuasarSet::deleteQuasars(std::vector<mongo::OID>& oids)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki.
	// Jeżeli tak to są opakowywane w DB::Exception.
	// Dodatkowo należy sprawdzać poprzez getLastError(), czy nie było błędów.
	// Niestety dokumentacja mongo-cxx-driver nie precyzuje, czy zawsze w razie błędów rzucane są wyjątki,
	// a różne pomoce wręcz sugerują, że nie jest tak.
	try
	{
		auto ns = common::namespaceBuilder({ this->database, _quasar_col_name() });
		for (auto& oid : oids)
		{

			auto query = mongo::BSONObjBuilder().append("_id", oid).obj();
			this->connection->remove(ns, query, true);

			std::string e;
			if (!(e = this->connection->getLastError()).empty())
			{
				throw db::RuntimeError(e);
			}
		}

	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("QuasarSet::deleteAllQuasars failed."));
	}
}

} /* namespace db_mongo */
