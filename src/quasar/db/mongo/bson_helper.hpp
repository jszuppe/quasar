#pragma once

// MONGO
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#endif
#include <mongo/client/dbclient.h>
#include <mongo/bson/bsonmisc.h>
// MONGO

#include <CL/cl.h>

#include "quasar/Quasar/Quasar.hpp"
#include "../ParameterizationResultSet.hpp"

namespace db_mongo
{

namespace common
{

std::shared_ptr<Quasar> quasarFromBSONObj(const mongo::BSONObj&);
std::shared_ptr<Quasar::qparams> quasarParamsFromBSONObj(const mongo::BSONObj&);
std::shared_ptr<Quasar::qdata> quasarDataFromBSONObj(const mongo::BSONObj&, const unsigned int size = 0);
std::shared_ptr<mongo::BSONObj> quasarToBSONObj(const Quasar&, const mongo::OID&);
std::shared_ptr<mongo::BSONObj> quasarParamsToBSONObj(const Quasar::sptr_qparams);
std::shared_ptr<mongo::BSONObj> quasarDataToBSONObj(const Quasar::sptr_qdata);
mongo::BSONObj paraOptionsToBSONObj(const db::ParameterizationResultSet::ParameterizationOptions& options);
mongo::BSONObj arrayToBSONObj(const cl_float* array, size_t n);

} /* namespace common */

} /* namespace db_mongo */
