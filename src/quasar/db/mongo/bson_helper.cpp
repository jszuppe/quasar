#include "bson_helper.hpp"

namespace db_mongo
{

namespace common
{

std::shared_ptr<Quasar::qparams> quasarParamsFromBSONObj(const mongo::BSONObj& obj)
{
	std::shared_ptr<Quasar::qparams> params(new Quasar::qparams());
	params->name = obj.getField("name").String();
	params->type = obj.getField("type").String();
	params->z = static_cast<float>(obj.getField("z").Double());
	params->ra = static_cast<float>(obj.getField("ra").Double());
	params->dec = static_cast<float>(obj.getField("dec").Double());
	params->mjd = static_cast<unsigned int>(obj.getField("mjd").Int());
	params->plate = static_cast<unsigned int>(obj.getField("plate").Int());
	params->fiber = static_cast<unsigned int>(obj.getField("fiber").Int());
	params->a = static_cast<float>(obj.getField("a").Double());
	params->b = static_cast<float>(obj.getField("b").Double());
	return params;
}

std::shared_ptr<Quasar::qdata> quasarDataFromBSONObj(const mongo::BSONObj& obj, const unsigned int size)
{
	std::shared_ptr<Quasar::qdata> qdata(new Quasar::qdata());
	if (size > 0)
	{
		qdata->reserve(size);
	}
	mongo::BSONObjIterator fields(obj);
	while (fields.more())
	{
		qdata->push_back(static_cast<Quasar::QFLOAT>((fields.next().numberDouble())));
	}
	return qdata;
}

std::shared_ptr<Quasar> quasarFromBSONObj(const mongo::BSONObj& obj)
{
	auto size = static_cast<unsigned int>(obj.getField("size").Int());
	auto qdata = quasarDataFromBSONObj(obj.getField("values").Obj(), size);
	auto qerror = quasarDataFromBSONObj(obj.getField("error").Obj(), size);
	auto qparams = quasarParamsFromBSONObj(obj.getField("params").Obj());
	return std::shared_ptr<Quasar>(new Quasar(qdata, qerror, qparams));
}

/**
 * Odwzorowuje parametry kwazaru w obiekt BSONObj.
 * @param params parametry kwazaru.
 * @return parametry kwazaru jako BSONObj.
 */
std::shared_ptr<mongo::BSONObj> quasarParamsToBSONObj(const Quasar::sptr_qparams params)
{
	mongo::BSONObjBuilder bob;
	bob.append("name", params->name);
	bob.append("type", params->type);
	bob.append("z", params->z);
	bob.append("ra", params->ra);
	bob.append("dec", params->dec);
	bob.append("mjd", params->mjd);
	bob.append("plate", params->plate);
	bob.append("fiber", params->fiber);
	bob.append("a", params->a);
	bob.append("b", params->b);
	return std::make_shared<mongo::BSONObj>(bob.obj());
}

/**
 * Odwzorowuje wektor danych o widmie kwazaru na obiekt BSONObj.
 * @param data widmo kwazaru.
 * @return widmo kwazaru jako obiekt BSONObj.
 */
std::shared_ptr<mongo::BSONObj> quasarDataToBSONObj(const Quasar::sptr_qdata data)
{
	mongo::BSONArrayBuilder abob;
	if (data != nullptr)
	{
		for (auto val : *data)
		{
			abob.append(val);
		}
	}
	return std::make_shared<mongo::BSONObj>(abob.obj());
}

/**
 * Odwzorowuje kwazar na obiekt BSONObj.
 * @param q kwazar.
 * @return kwazar opisany w BSONObj.
 */
std::shared_ptr<mongo::BSONObj> quasarToBSONObj(const Quasar& q, const mongo::OID& quasar_set_oid)
{
	mongo::BSONObjBuilder bob;
	bob.genOID();
	bob.append("quasar_set_oid", quasar_set_oid);
	bob.append("params", *quasarParamsToBSONObj(q.getParams()));
	auto data = q.getData();
	auto err = q.getError();
	bob.append("size", static_cast<unsigned int>(data->size()));
	bob.appendArray("values", *quasarDataToBSONObj(data));
	bob.appendArray("error", *quasarDataToBSONObj(err));
	return std::make_shared<mongo::BSONObj>(bob.obj());
}

/**
 *
 * @param options
 * @return
 */
mongo::BSONObj paraOptionsToBSONObj(const db::ParameterizationResultSet::ParameterizationOptions& options)
{
	mongo::BSONObjBuilder paraOptions;

	paraOptions.append("ampWavelength", options.ampWavelength);
	// Okna kontinuum
	paraOptions.append("continuumWindowsFile", options.continuumWindowsFile);
	{
		mongo::BSONArrayBuilder continuumWindows;
		for (auto& i : options.continuumWindows)
		{
			continuumWindows.append(arrayToBSONObj(i.s, 2));
		}
		paraOptions.appendArray("continuumWindows", continuumWindows.obj());
	}
	// Elementy
	paraOptions.append("elementsFile", options.elementsFile);
	{
		mongo::BSONArrayBuilder elements;
		for (auto& i : options.elements)
		{
			mongo::BSONObjBuilder element;
			element.append("name", i.name);
			element.appendArray("range", arrayToBSONObj(i.range.s, 2));
			element.appendArray("fitGuess", arrayToBSONObj(i.fitGuess.s, 3));
			elements.append(element.obj());
		}
		paraOptions.appendArray("elements", elements.obj());
	}
	// Szablon żelaza
	paraOptions.append("feTemplateFile", options.feTemplateFile);
//	{
//		mongo::BSONObjBuilder feTemplate;
//		mongo::BSONArrayBuilder values;
//		for (auto& i : options.feTemplate.values)
//		{
//			values.append(static_cast<double>(i));
//		}
//		mongo::BSONArrayBuilder wavels;
//		for (auto& i : options.feTemplate.wavelengths)
//		{
//			wavels.append(static_cast<double>(i));
//		}
//		feTemplate.appendArray("values", values.obj());
//		feTemplate.appendArray("wavelengths", wavels.obj());
//		paraOptions.append("feTemplate", feTemplate.obj());
//	}
	// Okna
	paraOptions.append("feWindowsFile", options.feWindowsFile);
	{
		mongo::BSONArrayBuilder feWindows;
		for (auto& i : options.feWindows)
		{
			feWindows.append(arrayToBSONObj(i.s, 2));
		}
		paraOptions.appendArray("feWindows", feWindows.obj());
	}
	// Parametry algorytmu parametryzacji
	{
		mongo::BSONObjBuilder feFitParameters;
		feFitParameters.append("feScaleRate", options.feFitParameters.feScaleRate);
		feFitParameters.append("fwhmn", options.feFitParameters.fwhmn);
		feFitParameters.append("fwhmt", options.feFitParameters.fwhmt);
		feFitParameters.append("fitType", static_cast<int>(options.feFitParameters.fitType));
		feFitParameters.appendArray("feFitRange", arrayToBSONObj(options.feFitParameters.feFitRange.s, 2));
		paraOptions.append("feFitParameters", feFitParameters.obj());
	}
	return paraOptions.obj();
}

mongo::BSONObj arrayToBSONObj(const cl_float* array, size_t n)
{
	mongo::BSONArrayBuilder abob;
	for (size_t j = 0; j < n; j++)
	{
		abob.append(static_cast<double>(array[j]));
	}
	return abob.obj();
}

}

}
