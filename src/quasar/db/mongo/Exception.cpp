/*
 * Exception.cpp
 *
 *  Created on: 8 paź 2014
 *      Author: jakub
 */

#include "Exception.hpp"

namespace db_mongo
{

OIDAlreadySet::OIDAlreadySet() :
			LogicError("OID already set.")
{

}

OIDAlreadySet::~OIDAlreadySet()
{

}

OIDInvalid::OIDInvalid() :
			RuntimeError("OID invalid.")
{

}

OIDInvalid::~OIDInvalid()
{

}

} /* namespace db_mongo */
