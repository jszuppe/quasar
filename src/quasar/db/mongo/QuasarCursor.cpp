#include "QuasarCursor.hpp"

namespace db_mongo
{

QuasarCursor::QuasarCursor(const std::shared_ptr<mongo::DBClientConnection> connection, const std::string& ns,
		const mongo::Query& query)
try :
			cursor(connection->query(ns, query))
{
	std::string e;
	if (!(e = connection->getLastError()).empty())
	{
		throw db::RuntimeError(e);
	}
}
catch (const std::exception& e)
{
	std::throw_with_nested(db::RuntimeError("QuasarCursor::QuasarCursor failed."));
}

QuasarCursor::~QuasarCursor()
{

}

/**
 * Pobiera n kwazarów z zwraca wskaźnik shared_ptr na wektor kwazarów.
 * Metoda pobierze maksymalnie n kwazarów, ale może mniej jeżeli (już) nie ma tylu kwazarów.
 * @param n liczba kwazarów do pobrania.
 * @return shared_ptr na wektor kwazarów.
 */
std::vector<Quasar> QuasarCursor::next(const size_t n)
{
	std::vector<Quasar> quasar_vector;
	try
	{
		unsigned int i = 0;
		while (cursor->more() && i < n)
		{
			// nextSafe() rzuca wyjątek w przypadku błędu,
			// więc pomijam sekwencje ze sprawdzaniem getLastError().
			mongo::BSONObj q_bson = cursor->nextSafe();
			auto q = common::quasarFromBSONObj(q_bson);
			quasar_vector.push_back(*q);
			i++;
		}
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("QuasarSet::getQuasars failed."));
	}
	return quasar_vector;
}

bool QuasarCursor::more()
{
	return this->cursor->more();
}

} /* namespace db_mongo */
