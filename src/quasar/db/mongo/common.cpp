#include "common.hpp"

namespace db_mongo
{

namespace common
{

/**
 * Tworzy nazwę mongo namespace.
 * @param next kolejne nazwy tworzące namespace w odpowiedniej kolejności.
 * @return utworzona nazwa namespace.
 */
std::string namespaceBuilder(std::initializer_list<std::string> names)
{
	std::string p;
	std::string ns;
	for (std::string s : names)
	{
		ns += p + s;
		p = ".";
	}
	return ns;
}

/**
 *
 * @param t data std::time_t
 * @return data mongo::Date_t
 */
mongo::Date_t to_Date_t(const std::time_t t)
{
	std::chrono::seconds s(t);
	return mongo::Date_t(std::chrono::duration_cast<std::chrono::milliseconds>(s).count());
}

/**
 *
 * @param d data mongo::Date_t
 * @return data std::time_t
 */
std::time_t to_time_t(const mongo::Date_t d)
{
	std::chrono::milliseconds ms(d.millis);
	return std::time_t(std::chrono::duration_cast<std::chrono::seconds>(ms).count());
}

} /* namespace common */

} /* namespace db_mongo */
