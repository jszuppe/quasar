#include "common.hpp"

namespace common
{

std::string progname;
bool prognameSet = false;

void set_progname(std::string name)
{
	if (prognameSet)
	{
		throw std::logic_error("Can not set program name second time.");
	}
	if (name.empty())
	{
		throw std::logic_error("Program name can not be an empty string.");
	}
	prognameSet = true;
	progname = name;
}

std::string get_progname()
{
	return progname;
}

std::string get_progdir()
{
	auto path = boost::filesystem::path(progname);
	if (path.has_parent_path())
	{
		return boost::filesystem::path(progname).parent_path().string() + "/";
	}
	return "";
}

void print_exception(const std::exception& e)
{
	std::cerr << "Exception: " << e.what() << std::endl;
	try
	{
		std::rethrow_if_nested(e);
	} catch (const std::exception& e)
	{
		print_exception(e);
	} catch (...)
	{
	}
}

std::string get_file_content(const std::string filename)
{
	std::ifstream in;
	in.exceptions(std::ifstream::badbit | std::ifstream::failbit);
	try
	{
		in.open(filename.c_str(), std::ios::in | std::ios::binary);
		if (in.is_open())
		{
			in.exceptions(std::ifstream::badbit);
			std::string contents;
			in.seekg(0, std::ios::end);
			contents.resize(in.tellg());
			in.seekg(0, std::ios::beg);
			in.read(&contents[0], contents.size());
			in.close();
			return contents;
		}
		else
		{
			throw std::logic_error("Error occurred while opening file: " + filename);
		}
	} catch (...)
	{
		throw std::logic_error("Error occurred while opening file: " + filename);
	}
}

namespace quasar
{

/**
 * Tworzy nazwę obiektu bazując na współrzędnych
 * według oficjalnej konwencji nazewnictwa IAU z SDSS:
 * 		SDSS JHHMMSS.ss+DDMMSS.s
 * @param ra
 * @param dec
 * @param prefix
 * @return nazwa obiektu na podstawie współrzędnych.
 */
std::string coord2name(double ra, double dec, std::string prefix)
{
	std::vector<double> r(3), d(3);
	r[0] = floor(ra / (360. / 24.));
	r[1] = floor(fmod(ra / (360. / 24.), 1.) * 60.);
	r[2] = fmod(fmod(ra / (360. / 24.), 1.) * 60., 1.) * 60.;

	d[0] = floor(fabs(dec));
	d[1] = floor(fmod(fabs(dec), 1.) * 60);
	d[2] = fmod(fmod(fabs(dec), 1.) * 60, 1.) * 60.;

	std::string sign = "+";
	if (dec < 0)
	{
		sign = "-";
	}

	std::vector<std::string> sr(3), sd(3);
	for (int i = 0; i < 2; i++)
	{
		if (r[i] > 10)
			sr[i] = std::to_string(r[i]).substr(0, 2);
		else
			sr[i] = "0" + std::to_string(r[i]).substr(0, 1);

		if (d[i] > 10)
			sd[i] = std::to_string(d[i]).substr(0, 2);
		else
			sd[i] = "0" + std::to_string(d[i]).substr(0, 1);
	}
	if (r[2] > 10)
		sr[2] = std::to_string(r[2]).substr(0, 5);
	else
		sr[2] = "0" + std::to_string(r[2]).substr(0, 4);
	if (d[2] > 10)
		sd[2] = std::to_string(d[2]).substr(0, 4);
	else
		sd[2] = "0" + std::to_string(d[2]).substr(0, 3);

	std::string result1 = "";
	std::string result2 = "";
	for (int i = 0; i < 3; i++)
	{
		result1 += sr[i];
		result2 += sd[i];
	}

	return prefix + result1 + sign + result2;
}

}

namespace file
{

/**
 *	Wczytywanie liczb z pliku.
 *	@param dataFilePath ścieżka do pliku z danymi.
 */
std::vector<cl_float> loadCLFloat(const boost::filesystem::path& dataFilePath)
{
	boost::iostreams::mapped_file_source file;
	file.open(dataFilePath.string());

	std::vector<cl_float> vf;
	if (file.is_open())
	{
		// Wyczytywanie danych
		using boost::spirit::qi::phrase_parse;
		using boost::spirit::qi::ascii::space;
		using boost::spirit::qi::float_;
		using boost::spirit::qi::_1;
		using boost::phoenix::push_back;

		// używam boost::spirit::qi::real_parser<cl_float>, żeby bity było dobrze ustawione we float
		// chociaż domyślny parser (float_) powinien też działać.
		phrase_parse(file.data(), file.data() + file.size(), *(boost::spirit::qi::real_parser<cl_float>()), space,
				vf);

		file.close();
	}
	else
		throw std::logic_error("File error. Absolute path:" + boost::filesystem::absolute(dataFilePath).string());

	return vf;
}

/**
 * Wczytuje szablon żelaza.
 * @param filePath
 * @return
 */
FeTemplate loadFeTemplate(const boost::filesystem::path& filePath)
{
	auto vl = loadCLFloat(filePath);

	std::vector<cl_float> values;
	std::vector<cl_float> wavelengths;

	// Nieparzyste
	for (size_t i = 1; i < vl.size(); i += 2)
	{
		values.push_back(vl[i]);
	}
	// Parzyste
	for (size_t i = 0; i < vl.size(); i += 2)
	{
		wavelengths.push_back(vl[i]);
	}

	if (wavelengths.size() != values.size())
	{
		throw std::logic_error("Invalid Fe template: lambda.size() != values.size().");
	}

	return
	{	values, wavelengths};
}

std::vector<SpectralLine> loadSpectralLines(const boost::filesystem::path& filePath)
{
	std::vector<SpectralLine> elements;

	// Wczytanie całego pliku
	auto data = std::move(loadStringData(filePath));
	while(!data->eof())
	{
		std::string line;
		safeGetline(*data, line);
		std::istringstream linestream(line);

		if(linestream.eof())
			continue;

		std::string name;
		cl_float2 range= {0.0f ,0.0f};
		cl_float4 fitGuess = {0.0f, 0.0f, 0.0f, 0.0f};

		linestream >> name;
		linestream >> range.s[0];
		linestream >> range.s[1];
		linestream >> fitGuess.s[0];
		linestream >> fitGuess.s[1];
		linestream >> fitGuess.s[2];

		if(!linestream.fail())
			elements.push_back({name, range, fitGuess});
	}

	return elements;
}
std::vector<cl_float2> loadWindows(const boost::filesystem::path& filePath)
{
	std::vector<cl_float2> windows;

	auto wins = loadCLFloat(filePath);
	for(size_t i = 0; i < wins.size(); i+=2)
	{
		windows.push_back({ { wins[i], wins[i+1]} });
	}

	return windows;
}

/**
 *	Wczytywanie pliku do obiektu klasy istringstream.
 *	@param filePath plik, który wczytujemy.
 */
std::unique_ptr<std::istringstream> loadStringData(const boost::filesystem::path& filePath)
{
	std::ifstream file(filePath.string(), std::ios::in | std::ios::binary);
	if (file.is_open())
	{
		std::string params;
		file.seekg(0, std::ios::end);
		params.resize(file.tellg());
		file.seekg(0, std::ios::beg);
		file.read(&params[0], params.size());

		return std::unique_ptr<std::istringstream>(new std::istringstream(params));
	}
	throw std::logic_error("File error. Absolute path:" + boost::filesystem::absolute(filePath).string());
	return nullptr;
}

/**
 * Pobiera linie niczym std::getline(), ale jest odporny na CRLF itp.
 * Źródło: http://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
 * @param is potok.
 * @param t napis, do którego wczytamy linie.
 */
std::istream& safeGetline(std::istream& is, std::string& t)
{
	t.clear();

	// The characters in the stream are read one-by-one using a std::streambuf.
	// That is faster than reading them one-by-one using the std::istream.
	// Code that uses streambuf this way must be guarded by a sentry object.
	// The sentry object performs various tasks,
	// such as thread synchronization and updating the stream state.

	std::istream::sentry se(is, true);
	std::streambuf* sb = is.rdbuf();

	for (;;)
	{
		int c = sb->sbumpc();
		switch (c)
		{
			case '\n':
				return is;
			case '\r':
				if (sb->sgetc() == '\n')
					sb->sbumpc();
				return is;
			case EOF:
				// Also handle the case when the last line has no line ending
				if (t.empty())
					is.setstate(std::ios::eofbit);
				return is;
			default:
				t += (char) c;
		}
	}
	return is;
}

}

}
