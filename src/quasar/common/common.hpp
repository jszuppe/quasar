#pragma once

#include "../Quasar/Quasar.hpp"

#include "quasar/algorithm/FeTemplate.hpp"
#include "quasar/algorithm/SpectralLine.hpp"

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_real.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <fstream>
#include <sstream>
#include <iostream>

#include <stdexcept>
#include <memory>
#include <chrono>

#include <CL/cl.h>

namespace common
{

void set_progname(std::string);
std::string get_progname();
std::string get_progdir();
void print_exception(const std::exception& e);
std::string get_file_content(const std::string filename);

template<typename TimeT = std::chrono::milliseconds>
struct measure
{
		template<typename F, typename ...Args>
		static typename TimeT::rep execution(F func, Args&&... args)
		{
			auto start = std::chrono::system_clock::now();

			// Now call the function with all the parameters you need.
			func(std::forward<Args>(args)...);

			auto duration = std::chrono::duration_cast<TimeT>(std::chrono::system_clock::now() - start);

			return duration.count();
		}
};

namespace quasar
{

std::string coord2name(double ra, double dec, std::string prefix = "SDDS J");

}

namespace file
{

FeTemplate loadFeTemplate(const boost::filesystem::path& filePath);
std::vector<SpectralLine> loadSpectralLines(const boost::filesystem::path& filePath);
std::vector<cl_float2> loadWindows(const boost::filesystem::path& filePath);
std::vector<cl_float> loadCLFloat(const boost::filesystem::path& dataFilePath);
std::unique_ptr<std::istringstream> loadStringData(const boost::filesystem::path& filePath);

std::istream& safeGetline(std::istream& is, std::string& t);

}

}
