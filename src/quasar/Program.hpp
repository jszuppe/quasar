#pragma once

#include "common/common.hpp"

#include "db/db.hpp"
#include "db/db_type.hpp"

#include "task/TaskType.hpp"
#include "task/TaskFactory.hpp"
#include "task/TaskOptionsFactory.hpp"

#include "validators.hpp"

#include <boost/program_options.hpp>

#include <iostream>
#include <vector>
#include <string>


class Program
{
	public:
		Program(int argc, char ** argv);
		virtual ~Program();
		virtual int run();

	private:
		int argc;
		char ** argv;

		const std::string name;

		boost::program_options::options_description general;
		boost::program_options::options_description special;
		boost::program_options::options_description allowed;
		boost::program_options::options_description hidden;
		boost::program_options::options_description all;

		boost::program_options::positional_options_description positional;

		void setupOptions();
		void printUsage();
};
