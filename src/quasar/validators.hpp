#pragma once

#include "db/Connection.hpp"
#include "task/TaskType.hpp"
#include "algorithm/fefitwin/FeFitWin.hpp"
#include <CL/cl.h>

#include <boost/program_options.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include <ctime>
#include <algorithm>
#include <string>

void validate(boost::any& v, std::vector<std::string> const& values, TaskType* /* target_type */,
		int);

void validate(boost::any& v, std::vector<std::string> const& values, cl_float2* /* target_type */,
		int);

namespace db
{

void validate(boost::any& v, std::vector<std::string> const& values, connectionParams* /* target_type */,
		int);

}

namespace std
{

void validate(boost::any& v, std::vector<std::string> const& values, time_t* /* target_type */,
		int);

}

namespace algorithm
{

namespace ocl
{

void validate(boost::any& v, std::vector<std::string> const& values, FeFitWin::Type* /* target_type */,
		int);

} // namespace ocl

} // namespace algorithm

