#include "validators.hpp"

namespace db
{

void validate(boost::any& v, const std::vector<std::string>& values, db::connectionParams* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	std::string host_regex(
			"(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?(?:\\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?)*\\.?");
	boost::regex r("^((" + host_regex + ")(:([0-9]{1,6}))?/)?([^/\\.\"\\*<>:\\|\\?]+)$");

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	const std::string& s = po::validators::get_single_string(values);

	if (values.empty())
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}

	std::string host("localhost");
	std::string db;
	unsigned int port = 27017;

	boost::smatch match;
	if (boost::regex_match(s, match, r))
	{
		if (!match[2].str().empty())
		{
			host = match[2].str();
		}
		if (!match[4].str().empty())
		{
			port = boost::lexical_cast<unsigned int>(match[4]);
		}
		if (!match[5].str().empty())
		{
			db = match[5].str();
		}
	}
	else
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}

	v = boost::any(db::connectionParams(host, port, db));
}

} // namespace db

void validate(boost::any& v, std::vector<std::string> const& values, TaskType* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	const std::string& s = po::validators::get_single_string(values);

	try
	{
		TaskType tt = to_taskType(s);
		v = boost::any(tt);
	} catch (const std::exception& e)
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}
}

namespace std
{

void validate(boost::any& v, std::vector<std::string> const& values, time_t* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	const std::string& s = po::validators::get_single_string(values);

	boost::regex r("^([0-9]{1,2})[:/\\.]([0-9]{1,2})[:/\\.]([0-9]{4})$");

	boost::smatch match;
	if (boost::regex_match(s, match, r))
	{
		struct std::tm tm = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nullptr };

		tm.tm_mday = std::min(31, std::max(1, boost::lexical_cast<int>(match[1])));
		tm.tm_mon = std::min(11, std::max(0, boost::lexical_cast<int>(match[2]) - 1));
		tm.tm_year = std::max(0, boost::lexical_cast<int>(match[3]) - 1900);

		std::time_t date = std::mktime(&tm);
		v = boost::any(date);
	}
	else
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}

}

} // namespace std

namespace algorithm
{

namespace ocl
{

void validate(boost::any& v, std::vector<std::string> const& values, FeFitWin::Type* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	const std::string& s = po::validators::get_single_string(values);

	try
	{
		std::string low_s = s;
		std::transform(low_s.begin(), low_s.end(), low_s.begin(), ::tolower);

		FeFitWin::Type type;
		if (low_s.compare("full") == 0)
		{
			type = FeFitWin::Type::FULL;
		}
		else if (low_s.compare("fwin") == 0)
		{
			type = FeFitWin::Type::FWIN;
		}
		else if (low_s.compare("win") == 0)
		{
			type = FeFitWin::Type::WIN;
		}
		else
		{
			throw po::validation_error(po::validation_error::invalid_option_value);
		}

		v = boost::any(type);
	} catch (const std::exception& e)
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}
}

} // namespace ocl

} // namespace algorithm

void validate(boost::any& v, std::vector<std::string> const& values, cl_float2* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	if(values.size() != 2)
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}

	try
	{
		cl_float2 value;

		cl_float first = static_cast<cl_float>(boost::lexical_cast<float>(values[0]));
		cl_float sec = static_cast<cl_float>(boost::lexical_cast<float>(values[1]));

		value = { { first, sec } };

		v = boost::any(value);
	} catch (const std::exception& e)
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}
}
