#pragma once

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <string>

struct SpectralLine
{
		/**
		 * Nazwa.
		 */
		std::string name;
		/**
		 * Zakres długości fal dla elementu.
		 */
		cl_float2 range;
		/**
		 * Początkowe współczynniki dopasowania.
		 */
		cl_float4 fitGuess;
};
