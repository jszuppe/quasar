#pragma once

#include "defines.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/common/common.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <algorithm>
#include <vector>
#include <utility>

#define CHECK_BUILD_PROGRAM_FAILURE( QUEUE, PROGRAM ) \
		if (e.err() == CL_BUILD_PROGRAM_FAILURE) \
		{ \
			cl::Device device = QUEUE.getInfo<CL_QUEUE_DEVICE>(); \
			std::cout << " \n\t\t\tBUILD LOG\n"; \
			std::cout << " ************************************************\n"; \
			std::cout << "Build Status: " << PROGRAM.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device) << std::endl; \
			std::cout << "Build Options:\t" << PROGRAM.getBuildInfo<CL_PROGRAM_BUILD_OPTIONS>(device) << std::endl; \
			std::cout << "Build Log:\t " << PROGRAM.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl; \
			std::cout << " ************************************************\n"; \
		}

namespace algorithm
{

namespace utils
{

/**
 * Synchroniczne (blokujące) kopiowanie danych
 *
 * @tparam DataType
 * @param cqueue
 * @param buffer
 * @param sizes
 * @param events
 * @return
 */
template<typename DataType>
std::vector<std::vector<DataType>> copy_back(const cl::CommandQueue& cqueue, const cl::Buffer& buffer,
		const std::vector<cl_uint>& sizes, const std::vector<cl::Event>* events = nullptr)
{
	if (events != nullptr)
	{
		cl::Event::waitForEvents(*events);
	}

	std::vector<std::vector<DataType>> vectors;
	vectors.reserve(sizes.size());

	size_t offset = 0;
	for (auto& size : sizes)
	{
		size_t byteSize = size * sizeof(DataType);

		std::vector<cl_float> data(size);

		// Mapowanie
		DataType *pointer = static_cast<DataType*>(
				cqueue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_READ, offset, byteSize, 0, 0, 0));
		// Kopiowanie
		std::copy(pointer, pointer + size, data.begin());
		// Koniec mapowania
		cl::Event endEvent;
		cqueue.enqueueUnmapMemObject(buffer, pointer, 0, &endEvent);
		endEvent.wait();

		// Czytamy po kolei z buffer, więc zwiększamy offset
		// Obecnie rezerwujemy zawsze ASTRO_OBJ_SPEC_SIZE * <liczba kwazarów>
		offset += ASTRO_OBJ_SPEC_SIZE * sizeof(cl_float);
		vectors.push_back(std::move(data));
	}

	return vectors;
}

template<typename DataType>
cl::Buffer make_buffer(const cl::Context& context, cl_mem_flags flags, const std::vector<DataType>& vec)
{
	// Rozmiar w bajtach
	size_t byteSize = vec.size() * sizeof(DataType);
	cl::Buffer buffer(context, flags, byteSize);
	return buffer;
}

template<typename DataType>
cl::Buffer make_buffer(const cl::Context& context, cl_mem_flags flags, const size_t size)
{
	// Rozmiar w bajtach
	size_t byteSize = size * sizeof(DataType);
	cl::Buffer buffer(context, flags, byteSize);
	return buffer;
}

std::vector<std::string> getSources(const std::vector<std::string>& filenames);

std::vector<cl_uint> sizes(std::vector<Quasar> astronomicalObjs);

cl::Buffer make_qbuffer(const cl::Context& context, cl_mem_flags flags, const size_t n);

cl::Buffer make_qbuffer_copy(const cl::Context& context, const cl::CommandQueue& cqueue, cl_mem_flags flags,
		const size_t n, cl::Buffer& src,
		const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

void copy(const cl::CommandQueue& cqueue, std::vector<Quasar> astronomicalObjs, cl::Buffer& spectrumsMatrix, cl::Buffer& errorsMatrix,
		const std::vector<cl::Event>* events = nullptr);



}  // namespace utils

} // namespace algorithm
