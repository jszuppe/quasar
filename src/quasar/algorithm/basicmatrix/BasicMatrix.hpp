#pragma once

#include "../defines.hpp"

#include "../utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>

namespace algorithm
{

namespace ocl
{

class BasicMatrix
{
	public:
		BasicMatrix(const cl::Context&, const cl::CommandQueue&);
		virtual ~BasicMatrix();

		void log10(cl::Buffer& matrix, const size_t row_size, const size_t rows,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void transpose(cl::Buffer& matrix, const size_t width, const size_t heigth,
				cl::Buffer& tmatrix,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		void minus(cl::Buffer& matrix, const size_t row_size, const size_t rows,
				cl_float subtrahend,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		void multiplyCol(cl::Buffer& matrix, const size_t row_size, const size_t rows,
				cl::Buffer& vector, cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		void minus(cl::Buffer& input, const size_t row_size, const size_t rows,
				cl::Buffer& subtrahend_matrix, cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void divide(cl::Buffer& dividend_matrix, const size_t row_size, const size_t rows,
				cl::Buffer& divisor_matrix, cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);


	private:

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		/** Kernele OpenCL odpowiadające metodom */

		cl::Kernel log10Kernel;
		static const char* log10KernelName;

		cl::Kernel transposeKernel;
		static const char* transposeKernelName;

		cl::Kernel minusScalarKernel;
		static const char* minusScalarKernelName;

		cl::Kernel minusMatrixKernel;
		static const char* minusMatrixKernelName;

		cl::Kernel divideMatrixKernel;
		static const char* divideMatrixKernelName;

		cl::Kernel multiplyColKernel;
		static const char* multiplyColKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
