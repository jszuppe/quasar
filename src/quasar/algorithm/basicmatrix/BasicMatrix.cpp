#include "BasicMatrix.hpp"

namespace algorithm
{

namespace ocl
{

const char* BasicMatrix::log10KernelName = "matrix_log10";
const char* BasicMatrix::transposeKernelName = "matrix_transpose";
const char* BasicMatrix::minusScalarKernelName = "matrix_minus_scalar";
const char* BasicMatrix::minusMatrixKernelName = "matrix_minus_matrix";
const char* BasicMatrix::divideMatrixKernelName = "matrix_divide_matrix";
const char* BasicMatrix::multiplyColKernelName = "matrix_multiply_col_vector";

std::vector<std::string> BasicMatrix::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
BasicMatrix::BasicMatrix(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources())
{
	try
	{
		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/basicmatrix/kernels.cl" });
		}
		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernele
		this->log10Kernel = cl::Kernel(this->program, this->log10KernelName);
		this->transposeKernel = cl::Kernel(this->program, this->transposeKernelName);
		this->minusScalarKernel = cl::Kernel(this->program, this->minusScalarKernelName);
		this->minusMatrixKernel = cl::Kernel(this->program, this->minusMatrixKernelName);
		this->divideMatrixKernel = cl::Kernel(this->program, this->divideMatrixKernelName);
		this->multiplyColKernel = cl::Kernel(this->program, this->multiplyColKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

BasicMatrix::~BasicMatrix()
{

}

/**
 * Wykonuje operacje log10 dla każdego elementu macierzy.
 *
 * Dla elementów mniejszych od zera ustawia NaN.
 *
 * @param input (cl_float) macierz wejściowa.
 * @param row_size długość wiersza.
 * @param rows ilość wierszy macierzy input.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::log10(cl::Buffer& input, const size_t row_size, const size_t rows,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->log10Kernel.setArg(arg++, sizeof(cl_float *), &input);
	this->log10Kernel.setArg(arg++, static_cast<cl_uint>(row_size));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = row_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < row_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(rows, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->log10Kernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Odejmuje i-ty element z macierzy input z i-tym elementem z macierzy
 * subtrahend_matrix, wynik zapisuje w macierzy output.
 *
 * UWAGI:
 *
 * - output może być tym samym buforem co input.
 *
 * @param input (cl_float) macierz wejściowa.
 * @param row_size długość wiersza.
 * @param rows ilość wierszy macierzy input.
 * @param subtrahend_matrix (cl_float) macierz, którą odejmujemy od input.
 * @param output (cl_float) wynik.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::minus(cl::Buffer& input, const size_t row_size, const size_t rows,
		cl::Buffer& subtrahend_matrix, cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->minusMatrixKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->minusMatrixKernel.setArg(arg++, static_cast<cl_uint>(row_size));
	this->minusMatrixKernel.setArg(arg++, sizeof(cl_float *), &subtrahend_matrix);
	this->minusMatrixKernel.setArg(arg++, sizeof(cl_float *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = row_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < row_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(rows, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->minusMatrixKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Odejmuje od macierzy skalar (czyli od każdego elementu macierzy).
 *
 * @param input (cl_float) macierz wejściowa.
 * @param row_size długość wiersza.
 * @param rows ilość wierszy macierzy input.
 * @param subtrahend wartość odejmowana.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::minus(cl::Buffer& input, const size_t row_size, const size_t rows,
		cl_float subtrahend,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->minusScalarKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->minusScalarKernel.setArg(arg++, static_cast<cl_uint>(row_size));
	this->minusScalarKernel.setArg(arg++, subtrahend);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = row_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < row_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(rows, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->minusScalarKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Mnoży każdy element z i-tej kolumny macierzy input przez i-ty element wektora
 * vector.
  *
 * UWAGI:
 *
 * - output może być tym samym buforem co input.
 *
 * @param input (cl_float) macierz wejściowa.
 * @param row_size długość wiersza.
 * @param rows ilość wierszy macierzy input.
 * @param vector (cl_float) wektor przez którego elementy mnożymy kolumny
 * @param output (cl_float) wynik.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::multiplyCol(cl::Buffer& input, const size_t row_size, const size_t rows,
		cl::Buffer& vector, cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->multiplyColKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->multiplyColKernel.setArg(arg++, static_cast<cl_uint>(row_size));
	this->multiplyColKernel.setArg(arg++, sizeof(cl_float *), &vector);
	this->multiplyColKernel.setArg(arg++, sizeof(cl_float *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = row_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < row_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(rows, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->multiplyColKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param matrix
 * @param width
 * @param height
 * @param tmatrix
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::transpose(cl::Buffer& matrix, const size_t width, const size_t height,
		cl::Buffer& tmatrix,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	// Rozmiar pomocniczych bloków pamięci lokalnej w jednym wymiarze.
	// Bloki są dwuwymiarowe.
	size_t BLOCK_DIM = 16;

	unsigned int arg = 0;
	this->transposeKernel.setArg(arg++, sizeof(cl_float *), &matrix);
	this->transposeKernel.setArg(arg++, sizeof(cl_float *), &tmatrix);
	this->transposeKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->transposeKernel.setArg(arg++, static_cast<cl_uint>(height));
	// Rozmiar ustawiam na 16 x 17, żeby uniknąć konflików bloków pamięci lokalnej
	this->transposeKernel.setArg(arg++, cl::Local(sizeof(cl_float) * BLOCK_DIM * (BLOCK_DIM + 1)));

	size_t global_size_d1 = width;
	size_t global_size_d2 = height;

	size_t remainder = global_size_d1 % BLOCK_DIM;
	if (remainder != 0)
	{
		global_size_d1 += BLOCK_DIM - remainder;
	}
	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	remainder = global_size_d2 % BLOCK_DIM;
	if (remainder != 0)
	{
		global_size_d2 += BLOCK_DIM - remainder;
	}
	if (global_size_d2 < height)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1, global_size_d2);
	cl::NDRange local(BLOCK_DIM, BLOCK_DIM);

	this->queue.enqueueNDRangeKernel(
			this->transposeKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Dzieli i-ty element z macierzy dividend_matrix przez i-ty elementem z macierzy
 * divisor_matrix, wynik zapisuje w macierzy output.
 *
 * UWAGI:
 *
 * - output może być tym samym buforem co dividend_matrix.
 *
 * @param dividend_matrix (cl_float) macierz wejściowa, której elementy dzielimy.
 * @param row_size długość wiersza.
 * @param rows ilość wierszy macierzy input.
 * @param divisor_matrix (cl_float) macierz przez której elementy dzielimy.
 * @param output (cl_float) wynik.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void BasicMatrix::divide(cl::Buffer& dividend_matrix, const size_t row_size, const size_t rows,
		cl::Buffer& divisor_matrix, cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->divideMatrixKernel.setArg(arg++, sizeof(cl_float *), &dividend_matrix);
	this->divideMatrixKernel.setArg(arg++, static_cast<cl_uint>(row_size));
	this->divideMatrixKernel.setArg(arg++, sizeof(cl_float *), &divisor_matrix);
	this->divideMatrixKernel.setArg(arg++, sizeof(cl_float *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = row_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < row_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(rows, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->divideMatrixKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}


} /* namespace ocl */

} /* namespace algorithm */

