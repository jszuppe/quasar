#pragma once

#include "../defines.hpp"

#include "../utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>

namespace algorithm
{

namespace ocl
{

class Tools
{
	public:
		Tools(const cl::Context&, const cl::CommandQueue&);
		virtual ~Tools();

		void convolve(cl::Buffer& input, const size_t row_size, const size_t rows, cl::Buffer& rows_sizes,
				cl::Buffer& filter, const size_t filterSize, cl::Buffer& out,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void copyIfNotInf(cl::Buffer& input, const size_t width, const size_t height,
				cl::Buffer& output, const size_t output_height,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void countIfNotInf(cl::Buffer& input, const size_t width, const size_t height,
				cl::Buffer& cols_count,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void reglin(cl::Buffer& xs, cl::Buffer& ys, const size_t width, const size_t height,
				cl::Buffer& cols_sizes, cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void reglinYax(cl::Buffer& xs, cl::Buffer& ys, const size_t width, const size_t height,
				cl::Buffer& cols_sizes, cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void chisq(cl::Buffer& fs, cl::Buffer& ys, cl::Buffer& errors,
				const size_t width, const size_t height, cl::Buffer& cols_sizes,
				cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void trapz(cl::Buffer& ys, cl::Buffer& xs,
				const size_t width, const size_t height, cl::Buffer& cols_sizes,
				cl::Buffer& output,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

	private:

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		/** Kernele, czyli różne narzędzia */

		cl::Kernel convolveKernel;
		static const char* convolveKernelName;

		cl::Kernel copyIfNotInfKernel;
		static const char* copyIfNotInfKernelName;

		cl::Kernel countIfNotInfKernel;
		static const char* countIfNotInfKernelName;

		cl::Kernel reglinKernel;
		static const char* reglinKernelName;

		cl::Kernel reglinYaxKernel;
		static const char* reglinYaxKernelName;

		cl::Kernel chisqKernel;
		static const char* chisqKernelName;

		cl::Kernel trapzKernel;
		static const char* trapzKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
