#include "Tools.hpp"

namespace algorithm
{

namespace ocl
{

const char* Tools::convolveKernelName = "convolve";
const char* Tools::copyIfNotInfKernelName = "copyIfNotInf";
const char* Tools::countIfNotInfKernelName = "countIfNotInf";
const char* Tools::reglinKernelName = "reglin";
const char* Tools::reglinYaxKernelName = "reglin_yax";
const char* Tools::chisqKernelName = "chisq";
const char* Tools::trapzKernelName = "integrate_trapz";

std::vector<std::string> Tools::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
Tools::Tools(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources())
{
	try
	{
		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/tools/kernels.cl" });
		}
		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernele
		this->convolveKernel = cl::Kernel(this->program, this->convolveKernelName);
		this->copyIfNotInfKernel = cl::Kernel(this->program, this->copyIfNotInfKernelName);
		this->countIfNotInfKernel = cl::Kernel(this->program, this->countIfNotInfKernelName);
		this->reglinKernel = cl::Kernel(this->program, this->reglinKernelName);
		this->reglinYaxKernel = cl::Kernel(this->program, this->reglinYaxKernelName);
		this->chisqKernel = cl::Kernel(this->program, this->chisqKernelName);
		this->trapzKernel = cl::Kernel(this->program, this->trapzKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

Tools::~Tools()
{

}

/**
 * Prosty splot z sygnałem i filtrem.
 *
 * Sygnały: wiersze macierzy input.
 * Filtr: tablica liczb zmiennoprzecinkowych.
 *
 * @param input (cl_float) macierz wejściowa.
 * @param width
 * @param height
 * @param rows_sizes (cl_uint) tablica ilości znaczących elementów w kolejnych wierszach macierzy input.
 * @param filter (cl_float) filtr (tablica 1D).
 * @param filterSize rozmiar filtru.
 * @param out macierz, do którego zapisywane są wyniki splotu. Musi być przynajmniej takiej wielości jak input (może to być to input).
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::convolve(cl::Buffer& input, const size_t width, const size_t height, cl::Buffer& rows_sizes,
		cl::Buffer& filter, const size_t filterSize, cl::Buffer& out,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->convolveKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->convolveKernel.setArg(arg++, sizeof(cl_uint *), &rows_sizes);
	this->convolveKernel.setArg(arg++, sizeof(cl_float *), &filter);
	this->convolveKernel.setArg(arg++, static_cast<cl_uint>(filterSize));
	this->convolveKernel.setArg(arg++, sizeof(cl_float *), &out);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.
	cl::NDRange global(width, height);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->convolveKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Kopiuje do kolejnych wierszy macierzy output wartości z odpowiadających wierszy
 * macierzy input wartości, które są różne od +/-INFINITY.
 *
 *
 * @param input (cl_float) macierz wejściowa.
 * @param width
 * @param height
 * @param output (cl float) macierz wynikowa.
 * @param output_height
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::copyIfNotInf(cl::Buffer& input, const size_t width, const size_t height,
		cl::Buffer& output, const size_t output_height,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->copyIfNotInfKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->copyIfNotInfKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->copyIfNotInfKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->copyIfNotInfKernel.setArg(arg++, sizeof(cl_float *), &output);
	this->copyIfNotInfKernel.setArg(arg++, static_cast<cl_uint>(output_height));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	// Poprawka jeżeli rows nie dzieli się przez device_max_wg_size.
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->copyIfNotInfKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Zlicza ilość elementów różnych od INFINITY dla każdej kolumny macierzy input.
 *
 * @param input (cl_float) macierz wejściowa.
 * @param width
 * @param height
 * @param cols_count (cl_uint) tablica wynikowa ze ilością elementów różnych od INFINITY dla każdej kolumny macierzy input.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::countIfNotInf(cl::Buffer& input, const size_t width, const size_t height,
		cl::Buffer& cols_count,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->countIfNotInfKernel.setArg(arg++, sizeof(cl_float *), &input);
	this->countIfNotInfKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->countIfNotInfKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->countIfNotInfKernel.setArg(arg++, sizeof(cl_uint *), &cols_count);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	if (global_size_d1 % device_max_wg_size != 0)
	{
		float rate = static_cast<float>(width) / static_cast<float>(device_max_wg_size);
		global_size_d1 = device_max_wg_size * (static_cast<size_t>(rate) + 1);
	}

	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->countIfNotInfKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Oblicza współczynniki prostej regresji liniowej y = a * x + b, metodą najmniejszych kwadratów.
 * Współczynniki są wyznaczane dla każdej pary kolumn z xs i ys.
 *
 * @param xs (cl_float) macierz wejściowa x'ów.
 * @param ys (cl_float) macierz wejściowa y'ów.
 * @param width
 * @param height
 * @param cols_sizes (cl_uint) tablica ilości znaczących elementów w kolejnych kolumnach macierzy xs.
 * @param output (cl_float8) tablica wyników, tj. współczynników, błędów i sum - (a, b, sia2, sib2, siy2, x_sum, y_sum, x^2_sum).
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::reglin(cl::Buffer& xs, cl::Buffer& ys, const size_t width, const size_t height, cl::Buffer& cols_sizes,
		cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->reglinKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->reglinKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->reglinKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->reglinKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->reglinKernel.setArg(arg++, sizeof(cl_uint *), &cols_sizes);
	this->reglinKernel.setArg(arg++, sizeof(cl_float8 *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	if (global_size_d1 % device_max_wg_size != 0)
	{
		float rate = static_cast<float>(width) / static_cast<float>(device_max_wg_size);
		global_size_d1 = device_max_wg_size * (static_cast<size_t>(rate) + 1);
	}

	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->reglinKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Oblicza współczynniki prostej regresji liniowej y = a * x, metodą najmniejszych kwadratów.
 * Współczynniki są wyznaczane dla każdej pary kolumn z xs i ys.
 *
 * @param xs (cl_float) macierz wejściowa x'ów.
 * @param ys (cl_float) macierz wejściowa y'ów.
 * @param width
 * @param height
 * @param cols_sizes (cl_uint) tablica ilości znaczących elementów w kolejnych kolumnach macierzy xs.
 * @param output (cl_float8) tablica wyników, tj. współczynników, błędów i sum - (a, b, sia2, sib2, siy2, x_sum, y_sum, x^2_sum).
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::reglinYax(cl::Buffer& xs, cl::Buffer& ys, const size_t width, const size_t height, cl::Buffer& cols_sizes,
		cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->reglinYaxKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->reglinYaxKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->reglinYaxKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->reglinYaxKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->reglinYaxKernel.setArg(arg++, sizeof(cl_uint *), &cols_sizes);
	this->reglinYaxKernel.setArg(arg++, sizeof(cl_float8 *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	if (global_size_d1 % device_max_wg_size != 0)
	{
		float rate = static_cast<float>(width) / static_cast<float>(device_max_wg_size);
		global_size_d1 = device_max_wg_size * (static_cast<size_t>(rate) + 1);
	}

	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->reglinYaxKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Test chisq sprawdzający dopasowanie wartości z odpowiadających sobie kolumn z macierzy
 * fs i ys. Błędy pomiaru dla każdego y są podane w macierzy błędów.
 *
 * output[i] = sum( (f[i][...] - y[i][...])^2 / (errors[i][...])^2)
 *
 * @param fs (cl_float) macierz wejściowa f'ów.
 * @param ys (cl_float) macierz wejściowa y'ów.
 * @param errors (cl_float) macierz błędów.
 * @param width
 * @param height
 * @param cols_sizes (cl_uint) tablica ilości znaczących elementów w kolejnych kolumnach macierzy fs, ys, errors.
 * @param output (cl_float) tablica wyników dopasowania chisq f do y.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::chisq(cl::Buffer& fs, cl::Buffer& ys, cl::Buffer& errors,
		const size_t width, const size_t height, cl::Buffer& cols_sizes,
		cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{

	unsigned int arg = 0;
	this->chisqKernel.setArg(arg++, sizeof(cl_float *), &fs);
	this->chisqKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->chisqKernel.setArg(arg++, sizeof(cl_float *), &errors);
	this->chisqKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->chisqKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->chisqKernel.setArg(arg++, sizeof(cl_uint *), &cols_sizes);
	this->chisqKernel.setArg(arg++, sizeof(cl_float *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	if (global_size_d1 % device_max_wg_size != 0)
	{
		float rate = static_cast<float>(width) / static_cast<float>(device_max_wg_size);
		global_size_d1 = device_max_wg_size * (static_cast<size_t>(rate) + 1);
	}

	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->chisqKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Oblicza przybliżenie całki za pomocą wzoru trapezów (trapezodial rule) dla wielu funkcji jednocześnie.
 *
 * Wartości całkowanych funkcji f(x) znajdują się w kolumnach macierzy ys,
 * zaś argumenty tych funkcji w odpowiadających kolumnach macierzy xs.
 *
 * @param xs (cl_float) macierz wejściowa y'ów.
 * @param xs (cl_float) macierz wejściowa x'ów.
 * @param width
 * @param height
 * @param cols_sizes (cl_uint) tablica ilości znaczących elementów w kolejnych kolumnach macierzy ys.
 * @param output (cl_float) wektor wyników, tj. całek.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Tools::trapz(cl::Buffer& ys, cl::Buffer& xs, const size_t width, const size_t height, cl::Buffer& cols_sizes,
		cl::Buffer& output,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->trapzKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->trapzKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->trapzKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->trapzKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->trapzKernel.setArg(arg++, sizeof(cl_uint *), &cols_sizes);
	this->trapzKernel.setArg(arg++, sizeof(cl_float8 *), &output);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na 1 wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = width;

	if (global_size_d1 % device_max_wg_size != 0)
	{
		float rate = static_cast<float>(width) / static_cast<float>(device_max_wg_size);
		global_size_d1 = device_max_wg_size * (static_cast<size_t>(rate) + 1);
	}

	if (global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->trapzKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

