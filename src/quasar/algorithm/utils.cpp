#include "utils.hpp"

namespace algorithm
{

namespace utils
{

std::vector<std::string> getSources(const std::vector<std::string>& filenames)
{
	std::vector<std::string> sources;
	for (const auto& s : filenames)
	{
		sources.push_back(common::get_file_content(s));
	}
	return sources;
}

/**
 * Zwraca wektor rozmiarów kolejnych kwazarów (ich widma).
 * @param vector wektor kwazarów, których rozmiary zwracamy.
 * @return wektor rozmiarów (size) kolejnych kwazarów.
 */
std::vector<cl_uint> sizes(std::vector<Quasar> astronomicalObjs)
{
	std::vector<cl_uint> s;
	s.reserve(astronomicalObjs.size());
	for (auto& q : astronomicalObjs)
	{
		s.push_back(static_cast<cl_uint>(q.getDataSize()));
	}
	return s;
}

/**
 *
 * @param context kontekst OpenCL
 * @param flags flagi pamięci OpenCL
 * @param n ilość kwazarów
 * @return bufor cl::Buffer
 */
cl::Buffer make_qbuffer(const cl::Context& context, cl_mem_flags flags, const size_t n)
{
	// Rozmiar widma pojedynczego kwazara
	size_t dataSize = ASTRO_OBJ_SPEC_SIZE;
	// Rozmiar w bajtach
	size_t byteSize = n * dataSize * sizeof(cl_float);
	cl::Buffer buffer(context, flags, byteSize);
	return buffer;
}

/**
 *
 * @param context kontekst OpenCL
 * @param flags flagi pamięci OpenCL
 * @param size ilość kwazarów
 * @return bufor cl::Buffer
 */
cl::Buffer make_qbuffer_copy(const cl::Context& context, const cl::CommandQueue& cqueue, cl_mem_flags flags,
		const size_t n, cl::Buffer& src,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	auto copy = make_qbuffer(context, flags, n);
	// Rozmiar widma pojedynczego kwazara
	size_t dataSize = ASTRO_OBJ_SPEC_SIZE;
	// Rozmiar w bajtach
	size_t byteSize = n * dataSize * sizeof(cl_float);
	cqueue.enqueueCopyBuffer(src, copy, 0, 0, byteSize, events, event);
	return copy;
}

/**
 * Synchroniczne (blokujące) kopiowanie kwazarów do buforu cl::Buffer.
 *
 * @param cqueue kolejka poleceń OpenCL.
 * @param astronomicalObjs obiekty astronomiczne, których dane kopiujemy do buforów OpenCK.
 * 							Do zakończenia funkcji obiekty z tego wektora nie mogą być zmieniane.
 * @param spectrumsMatrix bufor, do którego kopiujemy widmo.
 * @param errorsMatrix bufor, do którego kopiujemy błędy pomiaru widma.
 * @param events zdarzenia, na które czekamy przed rozpoczęciem kopiowania..
 */
void copy(const cl::CommandQueue& cqueue, std::vector<Quasar> astronomicalObjs, cl::Buffer& spectrumsMatrix, cl::Buffer& errorsMatrix,
		const std::vector<cl::Event>* events)
{
	if (events != nullptr)
	{
		cl::Event::waitForEvents(*events);
	}
	size_t offset = 0;
	for (auto& obj : astronomicalObjs)
	{
		//
		// WIDMO
		//

		auto data = obj.getData();
		size_t byteSize = data->size() * sizeof(cl_float);

		// Mapowanie
		cl_float *pointer = static_cast<cl_float*>(
				cqueue.enqueueMapBuffer(spectrumsMatrix, CL_TRUE, CL_MAP_WRITE, offset, byteSize, 0, 0, 0));
		// Kopiowanie
		std::copy(data->begin(), data->begin()+ASTRO_OBJ_SPEC_SIZE, pointer);
		// I koniec mapowania
		cl::Event endSpecEvent;
		cqueue.enqueueUnmapMemObject(spectrumsMatrix, pointer, 0, &endSpecEvent);

		endSpecEvent.wait();

		//
		// BŁĄD
		//

		data = obj.getError();

		// Mapowanie
		pointer = static_cast<cl_float*>(
				cqueue.enqueueMapBuffer(errorsMatrix, CL_TRUE, CL_MAP_WRITE, offset, byteSize, 0, 0, 0));
		// Kopiowanie
		std::copy(data->begin(), data->begin()+ASTRO_OBJ_SPEC_SIZE, pointer);
		// I koniec mapowania
		cl::Event endErrEvent;
		cqueue.enqueueUnmapMemObject(errorsMatrix, pointer, 0, &endErrEvent);

		endErrEvent.wait();

		// Zapisujemy po kolei w buffer, więc zwiększamy offset
		// Obecnie rezerwujemy zawsze ASTRO_OBJ_SPEC_SIZE * <liczba kwazarów>
		offset += ASTRO_OBJ_SPEC_SIZE * sizeof(cl_float);
	}
}

}  // namespace utils

} // namespace algorithm
