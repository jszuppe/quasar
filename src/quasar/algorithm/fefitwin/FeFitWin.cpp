#include "FeFitWin.hpp"

namespace algorithm
{

namespace ocl
{

const char* FeFitWin::reduceChisqsKernelName = "reduce_chisqs";

std::vector<std::string> FeFitWin::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
FeFitWin::FeFitWin(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources()),
			tools(Tools(context, queue)),
			specTools(SpecTools(context, queue)),
			basicMatrix(BasicMatrix(context, queue))
{
	try
	{
		if (queue.getInfo<CL_QUEUE_PROPERTIES>() & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)
		{
			throw std::logic_error("OpenCL command queue is out-of-order; in-order expected.");
		}

		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/fefitwin/kernels.cl" });
		}

		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernele
		this->reduceChisqsKernel = cl::Kernel(this->program, this->reduceChisqsKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

FeFitWin::~FeFitWin()
{

}

/**
 *
 * @param spectrumsData dane o widmach - macierze widm, długości fal [A], błędów, kontinuum. (Dane są ułożone w kolumnach macierzy.)
 * @param spectrumsBuffers dane o widmach w buforach cl::Buffer - macierze widm, długości fal, błędów, kontinuum. (Dane są ułożone w kolumnach macierzy.)
 * @param size ilość widm.
 * @param feTemplate szablon żelaza.
 * @param feWindows okna zdominowane przez emisje żelaza [A].
 * @param fitParameteres ustawienia algorytmu fitowania.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @return wynik fitowania.
 */
FeFitWin::Results FeFitWin::run(const Data spectrumsData, const Buffers spectrumsBuffers, const size_t size,
		FeTemplate feTemplate,
		std::vector<cl_float2> feWindows,
		FitParameters fitParameters,
		const std::vector<cl::Event>* events)
{
	if (events != nullptr)
	{
		cl::Event::waitForEvents(*events);
	}

	// ________________________________________________________________________ //
	// __bufory ze spectrumsBuffer_____________________________________________ //
	// ________________________________________________________________________ //

	cl::Buffer spectrumsMatrix = spectrumsBuffers.spectrumsMatrix;
	cl::Buffer wavelengthsMatrix = spectrumsBuffers.wavelengthsMatrix;
	cl::Buffer errorsMatrix = spectrumsBuffers.errorsMatrix;
	cl::Buffer continuumsMatrix = spectrumsBuffers.continuumsMatrix;
	cl::Buffer sizes = spectrumsBuffers.sizes;

	// ________________________________________________________________________ //
	// __sprawadzanie poprawności przekazanych danych__________________________ //
	// ________________________________________________________________________ //

	if (feWindows.empty())
	{
		throw std::logic_error("Vector with Fe windows can not be empty.");
	}

	if (feTemplate.wavelengths.empty())
	{
		throw std::logic_error("Fe template lambda can not be an empty vector.");
	}

	if (feTemplate.values.empty())
	{
		throw std::logic_error("Fe template values can not be an empty vector.");
	}

	// ________________________________________________________________________ //
	// __przygotowanie/przetwarzanie szablonu żelaza___________________________ //
	// ________________________________________________________________________ //

	// Macierz, w której w każdej kolumnie będzie znajdował się szablon żelaza
	// "dopasowany" do długości fal dla każdego wima.
	//
	// Można powiedzieć, że wycinamy ten kawałek szablonu, który jest w zakresie długości wal widma
	// i jeszcze odpowiednio go dopasowujemy do długości fal widma (interpolacja).
	//
	auto templateFeMatrix = calcFeTemplateMatrix(wavelengthsMatrix, sizes, size, feTemplate, fitParameters);

	// ________________________________________________________________________ //
	// __zachowanie kopii obliczonego szablonu żelaza__________________________ //
	// ________________________________________________________________________ //

	auto templateFeMatrixCopy = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	queue.enqueueCopyBuffer(templateFeMatrix, templateFeMatrixCopy, 0, 0,
			size * ASTRO_OBJ_SPEC_SIZE * sizeof(cl_float));

	// ________________________________________________________________________ //
	// __jeżeli continuum nie zostało odjęte od widm to odejmujemy_____________ //
	// ________________________________________________________________________ //

	if (!fitParameters.isSubC)
	{
		basicMatrix.minus(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, spectrumsMatrix);
	}

	// ________________________________________________________________________ //
	// __oblicznia odpowiednie dla wybranego typu obszaru______________________ //
	// ________________________________________________________________________ //

	if (fitParameters.fitType == Type::WIN || fitParameters.fitType == Type::FWIN)
	{
		// cl::Buffer na okna
		auto feWindowsBuffer = utils::make_buffer<cl_float2>(context, CL_MEM_READ_ONLY, feWindows.size());
		cl::copy(queue, feWindows.begin(), feWindows.end(), feWindowsBuffer);

		// Filtrowanie danych po oknach (długości fal)

		// Widmo, długości fal, błędy.
		//
		specTools.filterWithWavelengthWindows(spectrumsMatrix, wavelengthsMatrix, errorsMatrix, sizes, size,
				feWindowsBuffer, feWindows.size());

		// Żelazo i kontinuum
		//
		// specs jest już przefiltrowane, więc w "złych" miejscach jest INIFNITY
		specTools.filterInfs(spectrumsMatrix, continuumsMatrix, templateFeMatrix, sizes, size);
	}
	if (fitParameters.fitType == Type::FWIN)
	{
		// Filtrowanie zer, usuwanie danych z tych długości fal
		// dla których wartość szablonu żelaza (tj. z odpowiadającej kolumny templateFeMatrix) jest równa zero.

		// Widmo, długości fal, błędy.
		//
		specTools.filterZeros(templateFeMatrix, wavelengthsMatrix, errorsMatrix, sizes, size);

		// Żelazo i kontinuum
		//
		// specs jest już przefiltrowane, więc w "złych" miejscach jest INIFNITY
		specTools.filterInfs(templateFeMatrix, continuumsMatrix, spectrumsMatrix, sizes, size);
	}

	// ________________________________________________________________________ //
	// __Skracanie danych po filtracji_________________________________________ //
	// ________________________________________________________________________ //

	// Jeżeli filtrowaliśmy dane, no to warto działać tylko na tych które przeszły
	// filtrowanie.
	auto spectrumsMatrix_filtered = spectrumsMatrix;
	auto wavelengthsMatrix_filtered = wavelengthsMatrix;
	auto errorsMatrix_filtered = errorsMatrix;
	auto continuumsMatrix_filtered = continuumsMatrix;
	auto templateFeMatrix_filtered = templateFeMatrix;
	auto sizes_fewindows_filtered = sizes;
	size_t max_spectrum_filtered_size = ASTRO_OBJ_SPEC_SIZE;

	if (fitParameters.fitType == Type::WIN || fitParameters.fitType == Type::FWIN)
	{
		// Rozmiary widm po filtrowaniu.
		sizes_fewindows_filtered = utils::make_buffer<cl_uint>(context, CL_MEM_READ_WRITE, size);
		// Zliczanie elementów po przefiltrowaniu.
		tools.countIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, sizes_fewindows_filtered);

		// Pobranie rozmiarów i wybranie największego (max), następnie
		// obliczenie najbliżej wielokrotności 64 większej od maksimum.
		// Potrzebne, żeby adresy elementów początkowych były wielokrotnością 64;
		std::vector<cl_uint> sizes_filtered_vec(size, 0);
		cl::copy(queue, sizes_fewindows_filtered, sizes_filtered_vec.begin(), sizes_filtered_vec.end());
		auto max = *std::max_element(sizes_filtered_vec.begin(), sizes_filtered_vec.end());

		max_spectrum_filtered_size = max > 0 ? max : 64;
		size_t remainder = max_spectrum_filtered_size % 64;
		if (remainder != 0)
		{
			max_spectrum_filtered_size += 64 - remainder;
		}
		if (max_spectrum_filtered_size < max)
		{
			throw std::runtime_error("Error in calculating buffer size.");
		}

		tools.copyIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, spectrumsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, wavelengthsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(errorsMatrix, size, ASTRO_OBJ_SPEC_SIZE, errorsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(continuumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(templateFeMatrix, size, ASTRO_OBJ_SPEC_SIZE, templateFeMatrix, max_spectrum_filtered_size);

		spectrumsMatrix_filtered = spectrumsMatrix;
		wavelengthsMatrix_filtered = wavelengthsMatrix;
		errorsMatrix_filtered = errorsMatrix;
		continuumsMatrix_filtered = continuumsMatrix;
		templateFeMatrix_filtered = templateFeMatrix;
	}

	// ________________________________________________________________________ //
	// __obliczanie współczynników skalujących dla szablonów żelaza____________ //
	// ________________________________________________________________________ //

	cl::Buffer scaleRates = calcFeTemplateMatrixScaleRates(spectrumsMatrix_filtered, templateFeMatrix_filtered,
			sizes_fewindows_filtered, size, max_spectrum_filtered_size, fitParameters);

	// ___________________________________________________________________________ //
	// __wymnożenie szablonu żelaza przez współczynniki skalujące_________________ //
	// ___________________________________________________________________________ //

	// przefiltrowanego
	basicMatrix.multiplyCol(templateFeMatrix_filtered, size, max_spectrum_filtered_size, scaleRates,
			templateFeMatrix_filtered);

	// pełnego szablonu żelaza
	basicMatrix.multiplyCol(templateFeMatrixCopy, size, ASTRO_OBJ_SPEC_SIZE, scaleRates, templateFeMatrixCopy);

	// ________________________________________________________________________ //
	// __jakość dopasowania dla danych z okien (przefiltrowanych)______________ //
	// ________________________________________________________________________ //

	cl::Buffer reducedChisqs_filtered = calcReducedChisqs(spectrumsMatrix_filtered, templateFeMatrix_filtered, errorsMatrix_filtered,
			sizes_fewindows_filtered, size, max_spectrum_filtered_size);

	// ________________________________________________________________________ //
	// __załadowanie oryginalnych danych potrzebnych do dalszych obliczeń______ //
	// ________________________________________________________________________ //

	// Załadowanie pełnych wersji widm, długości fal, błędów i kontinuum
	cl::copy(queue, spectrumsData.spectrumsMatrixHost.begin(), spectrumsData.spectrumsMatrixHost.end(),
			spectrumsMatrix);
	cl::copy(queue, spectrumsData.wavelengthsMatrixHost.begin(), spectrumsData.wavelengthsMatrixHost.end(),
			wavelengthsMatrix);
	cl::copy(queue, spectrumsData.errorsMatrixHost.begin(), spectrumsData.errorsMatrixHost.end(),
			errorsMatrix);
	cl::copy(queue, spectrumsData.continuumsMatrixHost.begin(), spectrumsData.continuumsMatrixHost.end(),
			continuumsMatrix);

	// jeżeli continuum nie zostało odjęte od widm to odejmujemy
	if (!fitParameters.isSubC)
	{
		basicMatrix.minus(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, spectrumsMatrix);
	}

	// ________________________________________________________________________ //
	// __jakość dopasowania dla pełnego widma__________________________________ //
	// ________________________________________________________________________ //

	cl::Buffer reducedChisqs_full = calcReducedChisqs(spectrumsMatrix, templateFeMatrixCopy, errorsMatrix,
			sizes, size, ASTRO_OBJ_SPEC_SIZE);

	// ________________________________________________________________________ //
	// __pole powierzchni dla pełnego szablonu żelaza__________________________ //
	// ________________________________________________________________________ //

	cl::Buffer ews_full = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, size);
	{
		// templateFeMatrix już nie jest potrzebne, więc można go wykorzystać.
		auto temp = templateFeMatrix;
		basicMatrix.divide(templateFeMatrixCopy, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, temp);
		tools.trapz(temp, wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, sizes, ews_full);
	}

	// ________________________________________________________________________ //
	// __przywrócenie pełnej wersji templateFeMatrix z kopii___________________ //
	// ________________________________________________________________________ //

	queue.enqueueCopyBuffer(templateFeMatrixCopy, templateFeMatrix, 0, 0,
			size * ASTRO_OBJ_SPEC_SIZE * sizeof(cl_float));

	// ________________________________________________________________________ //
	// __jakość dopasowania i ew (pole pow) dla głównego pasma żelaza__________ //
	// ________________________________________________________________________ //

	cl::Buffer reducedChisqs_feRange;
	cl::Buffer ews_feRange;


	if (fitParameters.fitType == Type::WIN || fitParameters.fitType == Type::FWIN)
	{
		// ________________________________________________________________________ //
		// __fltrowanie danych, tylko główne pasmo żelaza__________________________ //
		// ________________________________________________________________________ //

		auto sizes_feRange_filtered = utils::make_buffer<cl_uint>(context, CL_MEM_READ_WRITE, size);
		std::vector<cl_float2> feFitRangeVec(1, fitParameters.feFitRange);

		auto feFitRangeBuffer = utils::make_buffer<cl_float2>(context, CL_MEM_READ_ONLY, feFitRangeVec.size());
		cl::copy(queue, feFitRangeVec.begin(), feFitRangeVec.end(), feFitRangeBuffer);

		specTools.filterWithWavelengthWindows(spectrumsMatrix, wavelengthsMatrix, errorsMatrix, sizes, size,
				feFitRangeBuffer, feFitRangeVec.size());
		specTools.filterInfs(spectrumsMatrix, continuumsMatrix, templateFeMatrix, sizes, size);

		// Zliczanie elementów po przefiltrowaniu.
		tools.countIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, sizes_feRange_filtered);

		// ________________________________________________________________________ //
		// __Skracanie danych po filtracji_________________________________________ //
		// ________________________________________________________________________ //

		// Pobranie rozmiarów i wybranie największego (max), następnie
		// obliczenie najbliżej wielokrotności 64 większej od maksimum.
		// Potrzebne, żeby adresy elementów początkowych były wielokrotnością 64;
		std::vector<cl_uint> sizes_filtered_vec(size, 0);
		cl::copy(queue, sizes_feRange_filtered, sizes_filtered_vec.begin(), sizes_filtered_vec.end());
		auto max = *std::max_element(sizes_filtered_vec.begin(), sizes_filtered_vec.end());

		max_spectrum_filtered_size = max > 0 ? max : 64;
		size_t remainder = max_spectrum_filtered_size % 64;
		if (remainder != 0)
		{
			max_spectrum_filtered_size += 64 - remainder;
		}
		if (max_spectrum_filtered_size < max)
		{
			throw std::runtime_error("Error in calculating buffer size.");
		}

		tools.copyIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, spectrumsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, wavelengthsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(errorsMatrix, size, ASTRO_OBJ_SPEC_SIZE, errorsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(continuumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, max_spectrum_filtered_size);
		tools.copyIfNotInf(templateFeMatrix, size, ASTRO_OBJ_SPEC_SIZE, templateFeMatrix,
				max_spectrum_filtered_size);

		// ________________________________________________________________________ //
		// __jakość dopasowania dla danych z głównego pasma żelaza_________________ //
		// ________________________________________________________________________ //

		reducedChisqs_feRange = calcReducedChisqs(spectrumsMatrix, templateFeMatrix, errorsMatrix,
				sizes_feRange_filtered, size, max_spectrum_filtered_size);

		// ________________________________________________________________________ //
		// __pole powierzchni dla pełnego szablonu żelaza__________________________ //
		// ________________________________________________________________________ //

		{
			ews_feRange = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, size);
			basicMatrix.divide(templateFeMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, templateFeMatrix);
			tools.trapz(templateFeMatrix, wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, sizes_feRange_filtered, ews_feRange);
		}
	}
	// fitParameters.fitType == Type::FULL
	else
	{
		reducedChisqs_feRange = reducedChisqs_full;
		ews_feRange = ews_full;
	}

	// ________________________________________________________________________ //
	// __kopiowanie wyników do wektorów________________________________________ //
	// ________________________________________________________________________ //


	std::vector<cl_float> scaleRatesVec(size);
	std::vector<cl_uint> sizes_fewindows_filteredVec(size);
	std::vector<cl_float> reducedChisqs_filteredVec(size);
	std::vector<cl_float> reducedChisqs_fullVec(size);
	std::vector<cl_float> ews_fullVec(size);
	std::vector<cl_float> reducedChisqs_feRangeVec(size);
	std::vector<cl_float> ews_feRangeVec(size);

	cl::copy(queue, scaleRates, scaleRatesVec.begin(), scaleRatesVec.end());
	cl::copy(queue, sizes_fewindows_filtered, sizes_fewindows_filteredVec.begin(), sizes_fewindows_filteredVec.end());
	cl::copy(queue, reducedChisqs_filtered, reducedChisqs_filteredVec.begin(), reducedChisqs_filteredVec.end());
	cl::copy(queue, reducedChisqs_full, reducedChisqs_fullVec.begin(), reducedChisqs_fullVec.end());
	cl::copy(queue, ews_full, ews_fullVec.begin(), ews_fullVec.end());
	cl::copy(queue, reducedChisqs_feRange, reducedChisqs_feRangeVec.begin(), reducedChisqs_feRangeVec.end());
	cl::copy(queue, ews_feRange, ews_feRangeVec.begin(), ews_feRangeVec.end());

	FeFitWin::Results results = {
			templateFeMatrixCopy,
			scaleRatesVec,
			sizes_fewindows_filteredVec, reducedChisqs_filteredVec,
			reducedChisqs_fullVec, ews_fullVec,
			reducedChisqs_feRangeVec, ews_feRangeVec
	};
	return results;
}

/**
 *
 * @param wavelengthsMatrix
 * @param sizes
 * @param size
 * @param feTemplate
 * @param fitParameters
 * @return
 */
cl::Buffer FeFitWin::calcFeTemplateMatrix(cl::Buffer wavelengthsMatrix, cl::Buffer sizes, size_t size,
		FeTemplate feTemplate, FitParameters fitParameters)
{
	std::vector<cl_float> templateFeValuesVec;
	if (fitParameters.fwhmn > fitParameters.fwhmt)
	{
		// Prędkość światła [m/s]
		const float C = 299792458.0f;

		float sincov = std::pow(std::pow(fitParameters.fwhmn, 2.0f) - std::pow(fitParameters.fwhmt, 2.0f), 0.5f)
				/ 2.0f;
		sincov /= pow((2.0f * std::log(2.0f)), 0.5f) * C / 1e3f;

		std::vector<cl_float> feGauss(feTemplate.wavelengths);
		std::transform(feGauss.begin(), feGauss.end(), feGauss.begin(),
				[](cl_float x) -> cl_float
				{
					return std::log10(x);
				});

		// pik gaussa z offsetem
		cl_float a = 1.0f / sincov / std::pow(2 * M_PI, 0.5);
		cl_float b = feGauss[feGauss.size() / 2];
		cl_float c = sincov;
		cl_float d = 0.0f;
		std::transform(feGauss.begin(), feGauss.end(), feGauss.begin(),
				[&](cl_float x) -> cl_float
				{
					return a * std::exp(-0.5f * (x-b) * (x-b)/pow(c, 2.0f)) + d;
				});

		templateFeValuesVec = convolve(feTemplate.values, feGauss);
	}
	else
	{
		templateFeValuesVec = std::vector<cl_float>(feTemplate.values);
	}

	auto templateFeMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	{
		auto zeros = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
		{
			std::vector<cl_float> zerosVec(size * ASTRO_OBJ_SPEC_SIZE, 0.0f);
			cl::copy(queue, zerosVec.begin(), zerosVec.end(), zeros);
		}
		auto templateFeValues = utils::make_buffer<cl_float>(context, CL_MEM_READ_ONLY, templateFeValuesVec.size());
		auto templateFeLambda = utils::make_buffer<cl_float>(context, CL_MEM_READ_ONLY, feTemplate.wavelengths.size());

		cl::copy(queue, templateFeValuesVec.begin(), templateFeValuesVec.end(), templateFeValues);
		cl::copy(queue, feTemplate.wavelengths.begin(), feTemplate.wavelengths.end(), templateFeLambda);

		// Dodawanie szablonu żelaza do zer z interpolacją zgodnie z długościami fal widm.
		specTools.addSpectrum(zeros, wavelengthsMatrix, sizes, size,
				templateFeLambda, templateFeValues, templateFeValuesVec.size(),
				templateFeMatrix);
	}
	return templateFeMatrix;
}

/**
 *
 * @param templateFeMatrix
 * @param spectrumsMatrix
 * @param sizes
 * @param size
 * @param max_spectrum_size
 * @param fitParameters
 * @return
 */
cl::Buffer FeFitWin::calcFeTemplateMatrixScaleRates(cl::Buffer spectrumsMatrix, cl::Buffer templateFeMatrix,
		cl::Buffer sizes, size_t size, size_t max_spectrum_size,
		FitParameters fitParameters)
{
	// obliczanie współczynników a dla prostej regresji liniowej y = a * x
	// prosta dla dopasowania szablonu żelaza do widma
	auto reglinYaxResults = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, size);
	tools.reglinYax(templateFeMatrix, spectrumsMatrix, size, max_spectrum_size,
			sizes, reglinYaxResults);

	// Wymnożenie współczynników z reglinYaxResults przez stała wartość skalującą feScaleRate
	// podaną w parametrach algorytmu
	std::vector<cl_float> reglinYaxResultsVec(size);
	cl::copy(queue, reglinYaxResults, reglinYaxResultsVec.begin(), reglinYaxResultsVec.end());
	std::transform(reglinYaxResultsVec.begin(), reglinYaxResultsVec.end(), reglinYaxResultsVec.begin(),
			[&](cl_float x)
			{	return x * fitParameters.feScaleRate;});
	cl::copy(queue, reglinYaxResultsVec.begin(), reglinYaxResultsVec.end(), reglinYaxResults);

	return reglinYaxResults;
}

/**
 *
 * @param spectrumsMatrix
 * @param errorsMatrix
 * @param sizes
 * @param size
 * @param templateFeMatrix
 * @return
 */
cl::Buffer FeFitWin::calcReducedChisqs(cl::Buffer& fs, cl::Buffer& ys, cl::Buffer errors,
		cl::Buffer sizes, size_t size, size_t max_spectrum_size)
{
	// Obliczenie chi kwadrat
	auto chisqResults = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, size);
	tools.chisq(fs, ys, errors, size, max_spectrum_size, sizes, chisqResults);
	// Podzielenie chi^2 przez ilość elementów
	// w celu uzyskanie ZREDUKOWANEGO CHI KWADRAT
	reduceChisqs(chisqResults, sizes, size);
	return chisqResults;
}

/**
 * Dokonuje splotu dwóch wektorów.
 *
 * @param signal
 * @param kernel
 * @param same jeżeli prawda to zwracany wektor będzie tej samej długości co signal; w przeciwnym wypadku signal.size() + kernel.size() - 1
 * @return wynik splotu (wektor)
 */
std::vector<cl_float> FeFitWin::convolve(std::vector<cl_float> signal, std::vector<cl_float> kernel, bool same)
{
	std::vector<cl_float> result(signal.size() + kernel.size() - 1, 0);

	for (size_t n = 0; n < result.size(); n++)
	{
		size_t kmin, kmax, k;

		kmin = (n >= kernel.size() - 1) ? n - (kernel.size() - 1) : 0;
		kmax = (n < signal.size() - 1) ? n : signal.size() - 1;

		for (k = kmin; k <= kmax; k++)
		{
			result[n] += signal[k] * kernel[n - k];
		}
	}

	if (same)
	{
		size_t kernel_center = kernel.size() % 2 == 0 ? (kernel.size() - 1) / 2 : kernel.size() / 2;
		result.erase(result.begin(), result.begin() + kernel_center);
		result.erase(result.end() - (kernel.size() - 1 - kernel_center), result.end());
	}

	return result;
}

/**
 *
 * @param chisqs
 * @param sizes_filtered
 * @param size
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void FeFitWin::reduceChisqs(cl::Buffer& chisqs, cl::Buffer& sizes_filtered, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->reduceChisqsKernel.setArg(arg++, sizeof(cl_float *), &chisqs);
	this->reduceChisqsKernel.setArg(arg++, sizeof(cl_uint *), &sizes_filtered);
	this->reduceChisqsKernel.setArg(arg++, static_cast<cl_uint>(size));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->reduceChisqsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

