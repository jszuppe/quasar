#include "common/common.hpp"
#include "Program.hpp"

#include <iostream>

int main(int argc, char ** argv)
{
	try
	{
		common::set_progname(std::string(argv[0]));
		Program program(argc, argv);
		return program.run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Unhandled Exception reached the top of main:\n";
		common::print_exception(e);
		std::cerr << "The application will now exit." << std::endl;

		return EXIT_FAILURE;
	}
	catch (...)
	{
		std::cerr << "Unhandled exception of unknown type reached the top of main.\n"
				<< "Application will now exit."
				<< std::endl;

		return EXIT_FAILURE;
	}
}
