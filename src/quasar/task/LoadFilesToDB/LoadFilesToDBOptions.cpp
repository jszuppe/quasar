#include "LoadFilesToDBTask.hpp"

/**
 * Konstruktor ustawia quasarSetDate na aktualny czas.
 */
LoadFilesToDBOptions::LoadFilesToDBOptions()
{
	this->quasarSetDate = std::time(nullptr);
}

LoadFilesToDBOptions::~LoadFilesToDBOptions()
{

}

/**
 * Ustawia połączenie.
 * @param connection połączenie.
 * @return
 */
LoadFilesToDBOptions& LoadFilesToDBOptions::setConnection(std::shared_ptr<db::Connection> connection)
{
	this->connection = connection;
	return *this;
}

/**
 * Ustawia nazwę zestawu kwazarów pod którym zostaną zapisane kwazary po wczytaniu.
 * @param quasarSetName nazwa zestawu.
 * @return
 */
LoadFilesToDBOptions& LoadFilesToDBOptions::setQuasarSetName(const std::string& quasarSetName)
{
	this->quasarSetName = quasarSetName;
	return *this;
}

/**
 * Ustawia date zestawu kwazarów. Nie jest to data wczytanie do bazy danych.
 * @param quasarSetDate data zestawu kwazarów.
 * @return
 */
LoadFilesToDBOptions& LoadFilesToDBOptions::setQuasarSetDate(const std::time_t quasarSetDate)
{
	this->quasarSetDate = quasarSetDate;
	return *this;
}

/**
 * Ustawia obiekt wczytujący pliki z kwazarami.
 * @param filesLoader obiekt odpowiedzialny za wczytywania danych.
 * @return
 */
LoadFilesToDBOptions& LoadFilesToDBOptions::setFilesLoader(std::shared_ptr<QuasarFilesLoader> filesLoader)
{
	this->filesLoader = filesLoader;
	return *this;
}
