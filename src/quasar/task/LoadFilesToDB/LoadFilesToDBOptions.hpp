#pragma once

#include "../Task.hpp"
#include "../TaskOptions.hpp"

#include "quasar/QuasarFilesLoader/FitFilesLoader.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"

#include <ctime>
#include <string>

// forward declaration na potrzeby LoadFilesToDBOptions.
class LoadFilesToDBTask;

/**
 * Klasa opcji wykonania dla zadania LoadFilesToDBTask.
 */
class LoadFilesToDBOptions: public TaskOptions
{
		friend class LoadFilesToDBTask;

	public:
		LoadFilesToDBOptions();
		~LoadFilesToDBOptions();

		LoadFilesToDBOptions& setConnection(std::shared_ptr<db::Connection>);
		LoadFilesToDBOptions& setQuasarSetName(const std::string&);
		LoadFilesToDBOptions& setQuasarSetDate(const std::time_t);
		LoadFilesToDBOptions& setFilesLoader(std::shared_ptr<QuasarFilesLoader>);

	private:
		std::shared_ptr<db::Connection> connection;

		std::string quasarSetName;
		std::time_t quasarSetDate;

		std::shared_ptr<QuasarFilesLoader> filesLoader;
};
