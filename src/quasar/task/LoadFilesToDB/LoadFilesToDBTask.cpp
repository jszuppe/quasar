#include "LoadFilesToDBTask.hpp"

/**
 * Konstruktor. Przekazane opcje zadania są sprawdzane pod względem wymaganych
 * opcji.
 * @param options opcje zadania.
 */
LoadFilesToDBTask::LoadFilesToDBTask(const LoadFilesToDBOptions options) :
			options(std::make_shared<LoadFilesToDBOptions>(options))
{
	this->checkOptions();
}

/**
 * Konstruktor. Przekazane opcje zadania są sprawdzane pod względem wymaganych
 * opcji.
 * @param options wskaźnik shared_ptr<TaskOptions> wskazujący na obiekt opcji zadania
 * klasy LoadFilesToDBOptions.
 */
LoadFilesToDBTask::LoadFilesToDBTask(std::shared_ptr<TaskOptions> options) :
			options(std::static_pointer_cast<LoadFilesToDBOptions>(options))
{
	this->checkOptions();
}

LoadFilesToDBTask::~LoadFilesToDBTask()
{

}

/**
 * Sprwadza obiekt opcji zadania pod względem wyspecyfikowania wymaganych opcji.
 */
void LoadFilesToDBTask::checkOptions()
{
	if (this->options->quasarSetName.empty())
	{
		throw std::logic_error("The parameter 'quasarSetName' cannot be an empty string.");
	}
	if (this->options->connection == nullptr)
	{
		throw std::logic_error("The parameter 'connection' cannot be NULLPTR.");
	}
	if (this->options->filesLoader == nullptr)
	{
		throw std::logic_error("The parameter 'filesLoader' cannot be an empty string.");
	}
}

/**
 * Ustanawia połączenie.
 * @return zainicjalizowany obiekt połączenia z bazą danych.
 */
std::shared_ptr<db::Connection> LoadFilesToDBTask::setupConnection()
{
	if (!this->options->connection->isConnected())
	{
		std::cout << "\tConnection isn't initialized. Trying to initialize connection to host now.\n";
		this->options->connection->connect();
		std::cout << "\tConnection successful.\n";
	}
	return this->options->connection;
}

/**
 * Inicjalizuje obiekt wczytujący kwazary z plików (jeżeli nie był zainicjalizowany)
 * i resetuje go (tak by wczytane były wszystkie kwazary z folderu).
 * @return gotowy obiekt wczytujący kwazary z plików.
 */
std::shared_ptr<QuasarFilesLoader> LoadFilesToDBTask::setupQuasarFilesLoader()
{
	if (!this->options->filesLoader->isInitialized())
	{
		this->options->filesLoader->init();
	}
	this->options->filesLoader->reset();
	return this->options->filesLoader;
}

/**
 * Wykonuje zadanie - wczytuje porcjami kwazary i zapisuje je w bazie danych.
 *
 * Na standardowe wyjście przekazywane są informacje o postępie prac.
 *
 * Zapis do bazy danych odbywa się asynchronicznie w stosunku do odczytów kolejnych
 * porcji kwazarów. (Uwaga: W razie wystąpienia błędu zestaw kwazarów jest usuwany
 * z bazy danych.)
 *
 * Obecnie rozmiar porcji jest na stałe ustalony na 5 tysięcy kwazarów; nie istnieje
 * obecnie potrzebna możliwości ustalania tego.
 */
void LoadFilesToDBTask::execute()
{
	checkExecuted();

	std::shared_ptr<db::Connection> connection;
	std::shared_ptr<QuasarFilesLoader> filesLoader;
	std::shared_ptr<db::QuasarSet> qset;

	try
	{
		std::cout
		<< "Loading astro. objects from " << options->filesLoader->getPath() << " folder to database ("
				<< options->connection->getHost() << ":"
				<< options->connection->getPort() << ", "
				<< options->connection->getDatabase() << ").\n";

		connection = setupConnection();
		filesLoader = setupQuasarFilesLoader();
		std::cout << "\t" << filesLoader->getSize() << " astro. objects have been found in " << options->filesLoader->getPath()
				<< " folder.\n";

		// Tworze pusty zestaw kwazarów
		qset = connection->getNewQuasarSet(options->quasarSetName, options->quasarSetDate);
		// Zapis do bazy danych
		connection->saveQuasarSet(qset);
		std::cout << "\tNew astro. object set (" << options->quasarSetName
				<< ") successfully created and saved to database.\n";

		// Strażnik, który usuwa zestaw kwazarów w razie jakiegoś błędu.
		Guard(guard, connection->deleteQuasarSet(qset););

		std::cout << "\tStart loading object and saving to database: ";
		unsigned int done = 0;
		unsigned int all = filesLoader->getSize();
		std::string progressStr = "0%";
		std::cout << progressStr << std::flush;

		unsigned int n = 5000;
		std::vector<Quasar> quasars, quasars_tmp;

		// Wczytuje pierwszą porcje kwazarów z folderu.
		quasars = filesLoader->loadn(n);
		while (!quasars.empty())
		{
			// Wizualizacja postępu
			done += quasars.size();
			int progress = int(float(done * 100) / float(all));
			std::cout << std::string(progressStr.length(), '\b');
			progressStr = std::to_string(progress) + "%";
			std::cout << progressStr << std::flush;

			// Uruchamiam asynchroniczne wpisanie do bazy danych.
			auto loadToDB = std::async(std::launch::async, &db::QuasarSet::addQuasars, qset.get(), std::move(quasars));
			// W tym samym czasie uruchamiam dalsze wczytywanie z folderu
			quasars_tmp = filesLoader->loadn(n);
			loadToDB.get();
			quasars = quasars_tmp;
		}
		// Wizualizacja postępu
		std::cout << std::string(progressStr.length(), '\b');
		std::cout << "100%\n" << std::flush;

		// dimiss guard
		guard.dismiss();
	} catch (const db::Exception& e)
	{
		std::throw_with_nested(std::runtime_error("Execution failed: Connection exception has occurred."));
	} catch (...)
	{
		std::throw_with_nested(std::runtime_error("Execution failed."));
	}
}
