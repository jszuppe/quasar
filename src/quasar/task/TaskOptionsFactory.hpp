#pragma once

#include "TaskType.hpp"
#include "TaskOptions.hpp"
#include "TaskOptionsParser.hpp"

#include "LoadFilesToDB/LoadFilesToDBOptionsParser.hpp"
#include "Parameterization/ParameterizationOptionsParser.hpp"

#include <memory>
#include <vector>
#include <string>

class TaskOptionsFactory
{
	public:
		TaskOptionsFactory();
		virtual ~TaskOptionsFactory();
		virtual std::shared_ptr<TaskOptions> create(TaskType);
		virtual std::shared_ptr<TaskOptions> create(TaskType, const std::vector<std::string>&);
		virtual std::shared_ptr<TaskOptionsParser> getParser(TaskType);
};

