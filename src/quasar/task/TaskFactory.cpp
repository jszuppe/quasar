#include "TaskFactory.hpp"

TaskFactory::TaskFactory()
{

}

TaskFactory::~TaskFactory()
{

}

std::unique_ptr<Task> TaskFactory::create(TaskType taskType, std::shared_ptr<TaskOptions> taskOptions)
{
	Task* task = nullptr;
	switch (taskType)
	{
		case TaskType::LoadFilesToDB:
			{
				task = static_cast<Task*>(new LoadFilesToDBTask(taskOptions));
			}
			break;
		case TaskType::Parameterization:
			{
				task = static_cast<Task*>(new ParameterizationTask(taskOptions));
			}
			break;
		default:
			{
			throw std::logic_error("Unknown task type.");
			}
			break;
	}
	return std::unique_ptr<Task>(task);
}

