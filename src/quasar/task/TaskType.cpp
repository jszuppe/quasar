#include "TaskType.hpp"

/**
 * Zamienia podany napis na TaskType.
 * Jeżeli nie nie ma TaskType, który odpowiada podanemu napisowi zwraca TaskType::Unknown.
 * @param str napis.
 * @return TaskType odpowiadający napisowi.
 */
TaskType to_taskType(const std::string& str)
{
	std::string low_str = str;
	std::transform(low_str.begin(), low_str.end(), low_str.begin(), ::tolower);

	if(low_str.compare("load") == 0 || low_str.compare("loadfilestodb") == 0)
	{
		return TaskType::LoadFilesToDB;
	}
	else if(low_str.compare("para") == 0 || low_str.compare("parameterization") == 0)
	{
		return TaskType::Parameterization;
	}
	throw std::logic_error("Unknown task type.");
}

std::string to_string(TaskType taskType)
{
	switch (taskType) {
		case TaskType::LoadFilesToDB:
			return "LoadFilesToDB";
			break;
		case TaskType::Parameterization:
			return "Parameterization";
			break;
		default:
			return "Unknown";
			break;
	}
}
