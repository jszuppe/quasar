#pragma once

#include "TaskOptions.hpp"

#include <string>
#include <vector>
#include <memory>

#include <boost/program_options.hpp>

/**
 * Klasa pomocnicza potrafiąca zamienić opcje podane w wektorze napisów w ustawienia
 */
class TaskOptionsParser
{
	public:
		TaskOptionsParser();
		virtual ~TaskOptionsParser();
		virtual std::shared_ptr<TaskOptions> parse(const std::vector<std::string>&) = 0;
		virtual std::string getOptionsDescription() const = 0;
};
