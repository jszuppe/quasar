#pragma once

#include <memory>
#include <utility>
#include <stdexcept>

class Task
{
	public:

		Task();
		virtual ~Task();
		virtual void execute() = 0;
		bool isExecuted() const;

	protected:
		bool executed;

		void checkExecuted() const;
};
