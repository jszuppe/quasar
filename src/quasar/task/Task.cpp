#include "Task.hpp"

Task::Task() : executed(false)
{

}

Task::~Task()
{

}

bool Task::isExecuted() const
{
	return this->executed;
}

void Task::checkExecuted() const
{
	if(executed)
	{
		throw std::logic_error("Task's been already executed.");
	}
}
