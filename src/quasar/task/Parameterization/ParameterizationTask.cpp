#include "ParameterizationTask.hpp"

/**
 * Konstruktor. Przekazane opcje zadania są sprawdzane pod względem wymaganych
 * opcji.
 * @param options opcje zadania.
 */
ParameterizationTask::ParameterizationTask(const ParameterizationOptions options) :
			options(std::make_shared<ParameterizationOptions>(options))
{
	this->checkOptions();
}

/**
 * Konstruktor. Przekazane opcje zadania są sprawdzane pod względem wymaganych
 * opcji.
 * @param options wskaźnik shared_ptr<TaskOptions> wskazujący na obiekt opcji zadania
 * klasy ParameterizationOptions.
 */
ParameterizationTask::ParameterizationTask(std::shared_ptr<TaskOptions> options) :
			options(std::static_pointer_cast<ParameterizationOptions>(options))
{
	this->checkOptions();
}

ParameterizationTask::~ParameterizationTask()
{

}

/**
 * Sprawdza obiekt opcji zadania pod względem wyspecyfikowania wymaganych opcji.
 */
void ParameterizationTask::checkOptions()
{
	if (this->options->astroObjSetName.empty())
	{
		throw std::logic_error("The parameter 'astroObjSetName' cannot be an empty string.");
	}
	if (this->options->readConnection == nullptr)
	{
		throw std::logic_error("The parameter 'readConnection' cannot be NULLPTR.");
	}
	if (this->options->writeConnection == nullptr)
	{
		throw std::logic_error("The parameter 'writeconnection' cannot be NULLPTR.");
	}
	if (this->options->continuumWindows.empty())
	{
		throw std::logic_error("The parameter 'continuumWindows' cannot be an empty vector.");
	}
	if (this->options->feWindows.empty())
	{
		throw std::logic_error("The parameter 'feWindows' cannot be an empty vector.");
	}
	if (this->options->spectralLines.empty())
	{
		throw std::logic_error("The parameter 'spectralLines' cannot be an empty vector.");
	}
	if (this->options->feTemplate.values.empty() || this->options->feTemplate.wavelengths.empty())
	{
		throw std::logic_error("The parameter 'feTemplate' cannot be empty.");
	}
	if (this->options->ampWavelength <= 0.0f)
	{
		throw std::logic_error("The parameter 'ampWavelength' cannot be less than or equal to zero.");
	}
	if (this->options->feFitParameters.fwhmn <= 0.0f)
	{
		throw std::logic_error("The parameter 'FWHMN' cannot be less than or equal to zero.");
	}
	if (this->options->feFitParameters.fwhmt <= 0.0f)
	{
		throw std::logic_error("The parameter 'FWHMT' cannot be less than or equal to zero.");
	}
	if ((this->options->feFitParameters.feFitRange.s[0] <= 0.0f
			|| this->options->feFitParameters.feFitRange.s[1] <= 0.0f)
			|| (this->options->feFitParameters.feFitRange.s[0] >= this->options->feFitParameters.feFitRange.s[1]))
	{
		throw std::logic_error("The parameter 'feFitRange' is not a correct wavelength range.");
	}
}

/**
 *
 */
void ParameterizationTask::execute()
{
	checkExecuted();

	std::shared_ptr<db::Connection> readConnection;
	std::shared_ptr<db::Connection> writeConnection;
	std::shared_ptr<db::QuasarSet> astroObjSet;

	namespace acl = algorithm::ocl;

	try
	{
		//
		// Połączenie
		//
		setupConnections();
		readConnection = this->options->readConnection;
		writeConnection = this->options->writeConnection;

		//
		// Stworzenie kontekstu i kolejki rozkazów
		//
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);

		if (platforms.empty())
		{
			throw std::runtime_error("No OpenCL platform");
		}

		auto platform = platforms[0];

		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		if (devices.empty())
		{
			throw std::runtime_error("No OpenCL GPU devices");
		}

		auto context = cl::Context(devices);
		auto queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		//
		//
		//
		acl::SpecParameterization parameterization(context, queue);

		// Odczyt zestawu
		astroObjSet = readConnection->getQuasarSetByName(options->astroObjSetName);
		std::cout << "\tAstronomical object set (" << options->astroObjSetName
				<< ") successfully found.\n";

		std::cout << "\tStart parameterization process: ";

		size_t done = 0;
		size_t all = astroObjSet->getSize();

		// Wizualizacja postępu
		std::string progressStr = "0%";
		std::cout << progressStr << std::flush;

		size_t n = 4000;
		auto cursor = astroObjSet->getQuasarCursor();
		std::vector<Quasar> astroObjs, astroObjs_next;

		// Przygotowanie zestawu wyników
		auto date = std::time(nullptr);
		std::string name = options->astroObjSetName + std::to_string(date);
		auto paraResultsSet = writeConnection->getNewParaResultSet(name, date,
			{
					options->astroObjSetName,
					options->astroObjN,
					options->spectralLines,
					options->spectralLinesFile,
					options->continuumWindows,
					options->continuumWindowsFile,
					options->ampWavelength,
					options->feTemplate,
					options->feTemplateFile,
					options->feWindows,
					options->feWindowsFile,
					options->feFitParameters
			});

		// Zapisanie zestawu
		writeConnection->saveParaResultSet(paraResultsSet);

		auto exec_time = common::measure<>::execution([&]()
		{
			// Pobranie danych
			astroObjs = cursor->next(n);
			std::future<void> saveResultsTask;

			while (done < all)
			{
				// Wizualizacja postępu
				int progress = int(float(done * 100) / float(all));
				std::cout << std::string(progressStr.length(), '\b');
				progressStr = std::to_string(progress) + "%";
				std::cout << progressStr << std::flush;

				done += astroObjs.size();

				if (astroObjs.size() > 0)
				{
					// Pobranie asynchronicznie kolejnej partii
					auto getNextQuasars = std::async(std::launch::async, &db::QuasarCursor::next, cursor.get(), n);

					// Algorytm
					auto results = parameterization.run(astroObjs, options->spectralLines, options->continuumWindows, options->ampWavelength,
							options->feWindows, options->feTemplate, options->feFitParameters);

					// Zakończenie algorytmu
					queue.finish();

					// Oczekiwanie na zrealizowanie poprzedniego zadania zapisu wyników (jeżeli było)
					if(saveResultsTask.valid())
						saveResultsTask.get();

					// Zapis asynchroniczny
					saveResultsTask = std::async(std::launch::async, &db::ParameterizationResultSet::saveResults, paraResultsSet.get(), astroObjs, results);

					// Czekanie na zrealizowanie zadania pobrania kolejnej partii danych
					astroObjs_next = getNextQuasars.get();
					astroObjs = astroObjs_next;
				}
			}

			// Oczekiwanie na zrealizowanie ostatniego zadania zapisu wyników
			if(saveResultsTask.valid())
				saveResultsTask.get();

		});

		// Wizualizacja postępu
		std::cout << std::string(progressStr.length(), '\b');
		std::cout << "100%\n" << std::flush;
		std::cout << "\tAstro. objects: " << all << std::endl;
		std::cout << "\tExecution time: " << exec_time << " ms" << std::endl;

	} catch (const cl::Error& e)
	{
		std::string error = "Execution failed: OpenCL error " + std::to_string(e.err()) + "  has occurred.";
		std::throw_with_nested(std::runtime_error(error.c_str()));
	} catch (const db::NotFound&)
	{
		std::rethrow_exception(std::current_exception());
	}
	catch (const db::Exception& e)
	{
		std::throw_with_nested(std::runtime_error("Execution failed: Connection exception has occurred."));
	} catch (...)
	{
		std::throw_with_nested(std::runtime_error("Execution failed."));
	}
}

/**
 * Ustanawia połączenie.
 * @return zainicjalizowany obiekt połączenia z bazą danych.
 */
void ParameterizationTask::setupConnections()
{
	if (!this->options->readConnection->isConnected())
	{
		this->options->readConnection->connect();
	}
	if (!this->options->writeConnection->isConnected())
	{
		this->options->writeConnection->connect();
	}
	std::cout << "\tConnection successful.\n";
}
