#pragma once

#include "../TaskOptionsParser.hpp"
#include "ParameterizationOptions.hpp"

#include "quasar/data/windows.hpp"
#include "quasar/data/spectralLines.hpp"
#include "quasar/algorithm/fefitwin/FeFitWin.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"

#include "quasar/validators.hpp"
#include "quasar/common/common.hpp"

#include <boost/program_options.hpp>

#include <sstream>

/**
 * Klasa odpowiedzialna za parsowanie opcji tekstowych (patrz boost::program_options)
 * do obiektu opcji zadania.
 */
class ParameterizationOptionsParser: public TaskOptionsParser
{
	public:
		ParameterizationOptionsParser();
		~ParameterizationOptionsParser();
		std::shared_ptr<TaskOptions> parse(const std::vector<std::string>& options);
		std::string getOptionsDescription() const;

	private:
		boost::program_options::options_description all;
		boost::program_options::options_description general;
		boost::program_options::options_description hidden;
};
