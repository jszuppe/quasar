#pragma once

#include "../Task.hpp"
#include "../TaskOptions.hpp"

// OpenCL
#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include "quasar/algorithm/SpectralLine.hpp"
#include "quasar/algorithm/FeTemplate.hpp"

#include "quasar/algorithm/specparameterization/SpecParameterization.hpp"
#include "quasar/algorithm/fefitwin/FeFitWin.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"

#include <ctime>
#include <string>

// forward declaration na potrzeby ParameterizationOptions.
class ParameterizationTask;

/**
 * Klasa opcji wykonania dla zadania ParameterizationTask.
 */
class ParameterizationOptions: public TaskOptions
{
		friend class ParameterizationTask;
		friend class db::ParameterizationResultSet;

	public:

		typedef algorithm::ocl::FeFitWin::FitParameters FeFitParameters;

		ParameterizationOptions();
		~ParameterizationOptions();

		ParameterizationOptions& setSpectralLines(std::vector<SpectralLine> spectralLines);
		ParameterizationOptions& setContinuumWindows(std::vector<cl_float2> continuumWindows);
		ParameterizationOptions& setAmpWavelength(cl_float ampWavelength);
		ParameterizationOptions& setFeWindows(std::vector<cl_float2> feWindows);
		ParameterizationOptions& setFeTemplate(FeTemplate feTemplate);
		ParameterizationOptions& setFeFitParameters(algorithm::ocl::FeFitWin::FitParameters feFitParameters);

		ParameterizationOptions& setSpectralLinesFile(std::string spectralLinesFile);
		ParameterizationOptions& setContinuumWindowsFile(std::string continuumWindowsFile);
		ParameterizationOptions& setFeWindowsFile(std::string feWindowsFile);
		ParameterizationOptions& setFeTemplateFile(std::string feTemplateFile);

		ParameterizationOptions& setReadConnection(std::shared_ptr<db::Connection> connection);
		ParameterizationOptions& setWriteConnection(std::shared_ptr<db::Connection> connection);
		ParameterizationOptions& setAstroObjSetName(const std::string& astroObjSetName);
		ParameterizationOptions& setAstroObjN(size_t astroObjN);

	private:

		/**
		 * Linie widmowe do dopasowania.
		 */
		std::vector<SpectralLine> spectralLines;
		std::string spectralLinesFile;
		/**
		 * Okna widmowe do dopasowania kontinuum (zakresy długości fal w A).
		 */
		std::vector<cl_float2> continuumWindows;
		std::string continuumWindowsFile;
		/**
		 * Długość fali na jakiej podana będzie amplituda dopasowanej funkcji potęgowej.
		 */
		cl_float ampWavelength;
		/**
		 * Szablon żelaza.
		 */
		FeTemplate feTemplate;
		std::string feTemplateFile;
		/**
		 * Okna zdominowane przez emisje żelaza (zakresy długości fal w A).
		 */
		std::vector<cl_float2> feWindows;
		std::string feWindowsFile;
		/**
		 * Parametry dopasowania szablonu żelaza.
		 */
		algorithm::ocl::FeFitWin::FitParameters feFitParameters;
		/**
		 * Nazwa zestawu obiektów astronomicznych dla których przeprowadzona będzie parametryzacja.
		 */
		std::string astroObjSetName;
		/**
		 * Liczba obiektów astronomicznych, dla których przeprowadzona będzie parametryzacja.
		 * Wartość zero oznacza parametryzacje wszystkich obiektów z zestawu.
		 */
		size_t astroObjN;
		/**
		 * Połączenie do bazy danych.
		 */
		std::shared_ptr<db::Connection> readConnection;
		std::shared_ptr<db::Connection> writeConnection;
};
