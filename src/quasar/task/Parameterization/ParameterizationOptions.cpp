#include "ParameterizationOptions.hpp"

/**
 *
 */
ParameterizationOptions::ParameterizationOptions()
{
	this->astroObjN = 0;
	this->ampWavelength = 3000.0f;
}

ParameterizationOptions::~ParameterizationOptions()
{

}

/**
 * Ustawia połączenie.
 * @param connection połączenie.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setReadConnection(std::shared_ptr<db::Connection> connection)
{
	this->readConnection = connection;
	return *this;
}

/**
 * Ustawia połączenie.
 * @param connection połączenie.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setWriteConnection(std::shared_ptr<db::Connection> connection)
{
	this->writeConnection = connection;
	return *this;
}

/**
 * Ustawia nazwę zestawu obiektów astronomicznych, dla których zostanie wykonana parametryzacja.
 * @param astroObjSetName nazwa zestawu.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setAstroObjSetName(const std::string& astroObjSetName)
{
	this->astroObjSetName = astroObjSetName;
	return *this;
}

/**
 * Ustawia liczbę obiektów astronomicznych, dla których przeprowadzona będzie parametryzacja.
 * Wartość zero oznacza parametryzacje wszystkich obiektów z zestawu.
 * @param astroObjN
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setAstroObjN(const size_t astroObjN)
{
	this->astroObjN = astroObjN;
	return *this;
}

/**
 * Ustawia linie widmowe, dla których zostanie wykonana parametryzacja.
 * @param spectralLines linie widmowe
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setSpectralLines(std::vector<SpectralLine> spectralLines)
{
	this->spectralLines = spectralLines;
	return *this;
}

/**
 * Ustawia okna widmowe do dopasowania kontinuum (zakresy długości fal w A).
 * @param continuumWindows okna widmowe do dopasowania kontinuum.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setContinuumWindows(std::vector<cl_float2> continuumWindows)
{
	this->continuumWindows = continuumWindows;
	return *this;
}

/**
 * Ustawia długość fali na jakiej podana będzie amplituda dopasowanej funkcji potęgowej.
 * @param ampWavelength długość fali na jakiej podana będzie amplituda dopasowanej funkcji potęgowej.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setAmpWavelength(cl_float ampWavelength)
{
	this->ampWavelength = ampWavelength;
	return *this;
}

/**
 * Ustawia zestaw okien zdominowanych przez emisje żelaza (zakresy długości fal w A).
 * @param feWindows okna zdominowane przez emisję żelaza.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setFeWindows(std::vector<cl_float2> feWindows)
{
	this->feWindows = feWindows;
	return *this;
}

/**
 * Ustawia szablon żelaza.
 * @param feTemplate szablon żelaza.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setFeTemplate(FeTemplate feTemplate)
{
	this->feTemplate = feTemplate;
	return *this;
}

/**
 * Ustawia parametry algorytmu dopasowania szablonu żelaza.
 * @param feFitParameters parametry algorytmu dopasowania szablonu żelaza.
 * @return
 */
ParameterizationOptions& ParameterizationOptions::setFeFitParameters(FeFitParameters feFitParameters)
{
	this->feFitParameters = feFitParameters;
	return *this;
}

ParameterizationOptions& ParameterizationOptions::setSpectralLinesFile(std::string spectralLinesFile)
{
	this->spectralLinesFile = spectralLinesFile;
	return *this;
}

ParameterizationOptions& ParameterizationOptions::setContinuumWindowsFile(std::string continuumWindowsFile)
{
	this->continuumWindowsFile = continuumWindowsFile;
	return *this;
}

ParameterizationOptions& ParameterizationOptions::setFeWindowsFile(std::string feWindowsFile)
{
	this->feWindowsFile = feWindowsFile;
	return *this;
}

ParameterizationOptions& ParameterizationOptions::setFeTemplateFile(std::string feTemplateFile)
{
	this->feTemplateFile = feTemplateFile;
	return *this;
}
