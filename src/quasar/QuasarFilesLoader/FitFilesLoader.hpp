#pragma once

#include "../Quasar/Quasar.hpp"
#include "../Quasar/QuasarFactory.hpp"
#include "QuasarFilesLoader.hpp"

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>

#define QUASAR_DATA_ROWS 4096

/**
 * Klasa służąca do wczytywania kwazarów z plików fit (dokładnie kwazarów z podanego folderu).
 */
class FitFilesLoader : public QuasarFilesLoader
{

	public:

		FitFilesLoader(std::string path);
		virtual ~FitFilesLoader(void);

		virtual void init();
		virtual void reset();

		virtual std::vector<Quasar> loadn(unsigned long n = 0);
		virtual std::size_t getSize();

	private:

		/**
		 * Rozszerzenia plików z danymi.
		 */
		const boost::filesystem::path quasarFileExt;
		/**
		 * Wektor zawierający wszystkie ścieżki do plików fit.
		 * Powstaje w konstruktorze: ładowane są wszystkie pliki z podanego folderu.
		 */
		std::vector<boost::filesystem::path> quasarFiles;
		/**
		 * Iterator po katalogu.
		 */
		std::vector<boost::filesystem::path>::const_iterator quasarFilesIterator;

		void findQuasarFiles();
		bool loadQuasars(long n);
};

