#pragma once

#include "../Quasar/Quasar.hpp"

#include <boost/filesystem/path.hpp>

#include <string>
#include <vector>

/**
 * Klasa służąca do wczytywania kwazarów z plików (dokładnie kwazarów z podanego folderu).
 */
class QuasarFilesLoader
{

	public:

		QuasarFilesLoader(std::string path);
		virtual ~QuasarFilesLoader(void);

		virtual void init() = 0;
		virtual void reset() = 0;

		virtual std::vector<Quasar> loadn(unsigned long n = 0) = 0;

		virtual std::size_t getSize() = 0;
		virtual bool isInitialized();

		virtual const std::string& getPath();

	protected:

		typedef Quasar::QFLOAT QFLOAT;
		/**
		 * Czy klasa została zainicjalizowana.
		 */
		bool initialized;
		/**
		 * Katalog z którego czytamy dane.
		 */
		const std::string dirPath;
};

