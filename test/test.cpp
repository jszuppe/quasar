#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TEST_QUASAR
#include <boost/test/unit_test.hpp>

#include "quasar/common/common.hpp"

struct Config
{
		Config()
		{
			std::cout << "***Global setup***" << std::endl;
			std::cout << "Progname: ";
			std::cout << boost::unit_test::framework::master_test_suite().argv[0] << std::endl;

			common::set_progname(std::string(boost::unit_test::framework::master_test_suite().argv[0]));

			std::cout << "Progdir: ";
			std::cout << common::get_progdir() << std::endl;
			std::cout << "***End global setup***" << std::endl;
		}
		~Config()
		{

		}
};

BOOST_GLOBAL_FIXTURE(Config);
