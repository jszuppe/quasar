
#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_COMMON
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"

#include "quasar/common/common.hpp"
#include "quasar/data/windows.hpp"
#include "quasar/data/spectralLines.hpp"


/**
 *
 */
BOOST_AUTO_TEST_SUITE(CommonTest)

const boost::filesystem::path contWindowsFilePath("test/test_data/cont_windows");
const boost::filesystem::path elementsFilePath("test/test_data/spectral_lines");

/**
 *
 */
BOOST_AUTO_TEST_CASE(LoadContWindowsTest)
{
	auto windows = common::file::loadWindows(contWindowsFilePath);

	BOOST_CHECK_EQUAL(windows.size(), data::contwinfull.size());

	for(size_t i; i < windows.size(); i++)
	{
		BOOST_CHECK_CLOSE(windows[i].s[0], data::contwinfull[i].s[0], 0.1);
		BOOST_CHECK_CLOSE(windows[i].s[1], data::contwinfull[i].s[1], 0.1);
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ElementsTest)
{
	auto spectralLines = common::file::loadSpectralLines(elementsFilePath);

	BOOST_CHECK_EQUAL(spectralLines.size(), data::spectralLines.size());

	for(size_t i; i < spectralLines.size(); i++)
	{
		// range
		BOOST_CHECK_CLOSE(spectralLines[i].range.s[0], data::spectralLines[i].range.s[0], 0.1);
		BOOST_CHECK_CLOSE(spectralLines[i].range.s[0], data::spectralLines[i].range.s[0], 0.1);

		// name
		BOOST_CHECK(spectralLines[i].name.compare(data::spectralLines[i].name) == 0);

		// fitGuess
		BOOST_CHECK_CLOSE(spectralLines[i].fitGuess.s[0], data::spectralLines[i].fitGuess.s[0], 0.1);
		BOOST_CHECK_CLOSE(spectralLines[i].fitGuess.s[1], data::spectralLines[i].fitGuess.s[1], 0.1);
		BOOST_CHECK_CLOSE(spectralLines[i].fitGuess.s[2], data::spectralLines[i].fitGuess.s[2], 0.1);
	}
}


BOOST_AUTO_TEST_SUITE_END()
