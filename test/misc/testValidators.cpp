#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_VALIDATORS
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"

#include "quasar/validators.hpp"
#include "quasar/common/common.hpp"

#include <boost/program_options.hpp>

/**
 *
 */
BOOST_AUTO_TEST_SUITE(ValidatorsTest)

/**
 * Negatywne testy parametrów połączenia.
 */
BOOST_AUTO_TEST_CASE(connectionParamsNTest)
{
	namespace po = boost::program_options;

	db::connectionParams * dummyCP;
	int dummyInt;

	boost::any v;

	// Brak
	BOOST_CHECK_THROW(db::validate(std::ref(v), { }, dummyCP, dummyInt), po::validation_error);

	// Powinien być jeden napis
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "a", "b", "c" }, dummyCP, dummyInt), po::validation_error);

	// Zły format
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foo:1" }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foo:1/" }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { ":1/foo" }, dummyCP, dummyInt), po::validation_error);

	// Zły format nazwy bazy danych
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foobar." }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foobar*" }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foobar?" }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foobar<" }, dummyCP, dummyInt), po::validation_error);
	BOOST_CHECK_THROW(db::validate(std::ref(v), { "foobar>" }, dummyCP, dummyInt), po::validation_error);
}

/**
 * Pozytywne testy parametrów połączenia.
 */
BOOST_AUTO_TEST_CASE(connectionParamsPTest)
{
	namespace po = boost::program_options;

	db::connectionParams * dummyCP;
	int dummyInt;

	{
		boost::any v;
		BOOST_CHECK_NO_THROW(db::validate(std::ref(v), { "bar" }, dummyCP, dummyInt));

		auto cp = boost::any_cast<db::connectionParams>(v);
		BOOST_CHECK_EQUAL(cp.host.compare("localhost"), 0);
		BOOST_CHECK_EQUAL(cp.port, 27017);
		BOOST_CHECK_EQUAL(cp.database.compare("bar"), 0);
	}
	{
		boost::any v;
		BOOST_CHECK_NO_THROW(db::validate(std::ref(v), { "foo/bar" }, dummyCP, dummyInt));

		auto cp = boost::any_cast<db::connectionParams>(v);
		BOOST_CHECK_EQUAL(cp.host.compare("foo"), 0);
		BOOST_CHECK_EQUAL(cp.port, 27017);
		BOOST_CHECK_EQUAL(cp.database.compare("bar"), 0);
	}
	{
		boost::any v;
		BOOST_CHECK_NO_THROW(db::validate(std::ref(v), { "foo:20/bar" }, dummyCP, dummyInt));

		auto cp = boost::any_cast<db::connectionParams>(v);
		BOOST_CHECK_EQUAL(cp.host.compare("foo"), 0);
		BOOST_CHECK_EQUAL(cp.port, 20);
		BOOST_CHECK_EQUAL(cp.database.compare("bar"), 0);
	}
	{
		boost::any v;
		BOOST_CHECK_NO_THROW(db::validate(std::ref(v), { "192.100.20.15:20715/bar" }, dummyCP, dummyInt));

		auto cp = boost::any_cast<db::connectionParams>(v);
		BOOST_CHECK_EQUAL(cp.host.compare("192.100.20.15"), 0);
		BOOST_CHECK_EQUAL(cp.port, 20715);
		BOOST_CHECK_EQUAL(cp.database.compare("bar"), 0);
	}
}

/**
 * Negatywne testy walidacji TaskType
 */
BOOST_AUTO_TEST_CASE(TaskTypeNTest)
{
	namespace po = boost::program_options;

	TaskType* dummyTT;
	int dummyInt;

	boost::any v;

	// Brak
	BOOST_CHECK_THROW(validate(std::ref(v), { }, dummyTT, dummyInt), po::validation_error);

	// Dwa napisy / niedozwolone
	BOOST_CHECK_THROW(validate(std::ref(v), { "foo", "bar" }, dummyTT, dummyInt), po::validation_error);

	// Nie ma takiego TaskType
	BOOST_CHECK_THROW(validate(std::ref(v), { "foobar" }, dummyTT, dummyInt), po::validation_error);

}

/**
 * Pozytywne testy walidacji TaskType
 */
BOOST_AUTO_TEST_CASE(TaskTypePTest)
{
	namespace po = boost::program_options;

	TaskType* dummyTT;
	int dummyInt;

	{
		boost::any v;
		BOOST_CHECK_NO_THROW(validate(std::ref(v), { "load" }, dummyTT, dummyInt));

		auto tt = boost::any_cast<TaskType>(v);
		BOOST_CHECK(tt == TaskType::LoadFilesToDB);
	}
	{
		boost::any v;
		BOOST_CHECK_NO_THROW(validate(std::ref(v), { "LoadFilesToDB" }, dummyTT, dummyInt));

		auto tt = boost::any_cast<TaskType>(v);
		BOOST_CHECK(tt == TaskType::LoadFilesToDB);
	}
}

/**
 * Negatywne testy walidacji std::time_t
 */
BOOST_AUTO_TEST_CASE(TimeTNTest)
{
	namespace po = boost::program_options;

	std::time_t * dummyTT;
	int dummyInt;

	boost::any v;

	// Brak
	BOOST_CHECK_THROW(std::validate(std::ref(v), { }, dummyTT, dummyInt), po::validation_error);

	// Dwa napisy / niedozwolone
	BOOST_CHECK_THROW(std::validate(std::ref(v), { "10.", "12.2013" }, dummyTT, dummyInt), po::validation_error);

	// Napis nie jest datą
	BOOST_CHECK_THROW(std::validate(std::ref(v), { "foobar" }, dummyTT, dummyInt), po::validation_error);

	// Napis nie jest pełną datą
	BOOST_CHECK_THROW(std::validate(std::ref(v), { "10.2014" }, dummyTT, dummyInt), po::validation_error);

}

/**
 * Pozytywne testy walidacji std::time_t
 */
BOOST_AUTO_TEST_CASE(TimeTPTest)
{
	namespace po = boost::program_options;

	std::time_t * dummyTT;
	int dummyInt;

	struct std::tm tm = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nullptr };
	tm.tm_mday = 31;
	tm.tm_mon = 12 - 1; // patrz tm_mon
	tm.tm_year = 2014 - 1900; // patrz tm_year

	{
		boost::any v;
		BOOST_CHECK_NO_THROW(std::validate(std::ref(v), { "31.12.2014" }, dummyTT, dummyInt));

		auto tt = boost::any_cast<std::time_t>(v);
		BOOST_CHECK_EQUAL(tt, std::mktime(&tm));
	}
	{
		boost::any v;
		BOOST_CHECK_NO_THROW(std::validate(std::ref(v), { "31/12/2014" }, dummyTT, dummyInt));

		auto tt = boost::any_cast<std::time_t>(v);
		BOOST_CHECK_EQUAL(tt, std::mktime(&tm));
	}
}

BOOST_AUTO_TEST_SUITE_END()
