#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_GUARD
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"

#include "quasar/common/guard.hpp"
#include "quasar/common/common.hpp"

#include <turtle/mock.hpp>
#include <boost/program_options.hpp>

/**
 *
 */
BOOST_AUTO_TEST_SUITE(GuardTest)

MOCK_CLASS(GuardTester)
{
	MOCK_METHOD(dummy, 1, int( int ), dummy)
};


/**
 *
 */
BOOST_AUTO_TEST_CASE(AutoGuardTest)
{
	GuardTester tester;

	MOCK_EXPECT( tester.dummy ).once().with(0).returns(0);
	{
		AutoGuard(tester.dummy(0););
		// Przy wyjściu z zasięgu powinna zostać zawołana metoda dummy(0)
		// na obiekcie tester.
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(AutoGuardTest2)
{
	GuardTester tester;

	MOCK_EXPECT( tester.dummy ).once().with(0).returns(0);
	{
		try
		{
			AutoGuard(tester.dummy(0););
			// Przy wyjściu z zasięgu powinna zostać zawołana metoda dummy(0)
			// na obiekcie tester mimo, że rzuciłem wyjątek.
			throw std::exception();
		} catch (...)
		{

		}
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(GuardTest)
{
	GuardTester tester;

	MOCK_EXPECT( tester.dummy ).once().with(0).returns(0);
	{
		Guard(guard, tester.dummy(0););
		// Przy wyjściu z zasięgu powinna zostać zawołana metoda dummy(0)
		// na obiekcie tester.
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(GuardDismissTest)
{
	GuardTester tester;

	MOCK_EXPECT( tester.dummy ).never();
	{
		Guard(guard, tester.dummy(0););
		guard.dismiss();
		// Przy wyjściu z zasięgu NIE powinna zostać zawołana metoda dummy(0)
		// na obiekcie tester.
	}
}

BOOST_AUTO_TEST_SUITE_END()
