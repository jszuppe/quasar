
#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_LOAD_FILES_TO_DB_TASK
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../mock/db.hpp"
#include "../mock/MockFilesLoader.hpp"

#include "quasar/task/LoadFilesToDB/LoadFilesToDBTask.hpp"
#include "quasar/task/LoadFilesToDB/LoadFilesToDBOptions.hpp"
#include "quasar/task/LoadFilesToDB/LoadFilesToDBOptionsParser.hpp"

#include "quasar/common/common.hpp"

#include <boost/program_options.hpp>

struct SF_LOAD_FILES_TO_DB_TASK
{
		SF_LOAD_FILES_TO_DB_TASK() :
					c(std::shared_ptr<db::Connection>(new MockConnection({ MONGODB_TEST_HOST, MONGODB_TEST_PORT,
					MONGODB_TEST_DB }))),
					qfl(std::shared_ptr<QuasarFilesLoader>(new MockFilesLoader("test/test_data/")))
		{
			mc = std::dynamic_pointer_cast<MockConnection>(c);
			setup_mock(mc.get());

			mqfl = std::dynamic_pointer_cast<MockFilesLoader>(qfl);
			setup_mock(mqfl.get());
		}
		~SF_LOAD_FILES_TO_DB_TASK()
		{

		}

		std::shared_ptr<db::Connection> c;
		std::shared_ptr<MockConnection> mc;

		std::shared_ptr<QuasarFilesLoader> qfl;
		std::shared_ptr<MockFilesLoader> mqfl;
};

/**
 * Test zadania wczytującego kwazary z plików do bazy danych.
 */
BOOST_FIXTURE_TEST_SUITE(LoadFilesToDBTaskTest, SF_LOAD_FILES_TO_DB_TASK)

/**
 *
 */
BOOST_AUTO_TEST_CASE(OptionsTest)
{
	LoadFilesToDBOptions options;
	BOOST_CHECK_NO_THROW(options = LoadFilesToDBOptions()
			.setConnection(c)
			.setQuasarSetName("qs_ParamsTest1")
			.setFilesLoader(qfl)
			);
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(OptionsParserParseNTest)
{
	LoadFilesToDBOptionsParser parser;

	namespace po = boost::program_options;

	// Brak wymaganych opcji.
	BOOST_CHECK_THROW(parser.parse({}), po::error);

	// Brak wymaganych opcji.
	BOOST_CHECK_THROW(parser.parse({"foo:20/bar"}), po::error);

	// Brak wymaganych opcji.
	BOOST_CHECK_THROW(parser.parse({"foo:20/bar", "--path", "path"}), po::error);

	// Brak wymaganych opcji.
	BOOST_CHECK_THROW(parser.parse({"foo:20/bar", "--path", "path", "-N"}), po::error);
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(OptionsParserParsePTest)
{
	LoadFilesToDBOptionsParser parser;

	namespace po = boost::program_options;

	// Wszystkie wymagane opcje
	BOOST_CHECK_NO_THROW(parser.parse({"foo:20/bar", "--path", "path", "-N", "name"}));

	//
	BOOST_CHECK_NO_THROW(parser.parse({"foobar", "--path", "path", "-D", "20.01.2013", "-N", "name"}));
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(CreateTaskTest)
{
	LoadFilesToDBOptions bad_params, good_params;

	bad_params = LoadFilesToDBOptions();
	good_params = LoadFilesToDBOptions();

	BOOST_CHECK_NO_THROW(bad_params
			.setFilesLoader(qfl));
	// Brakuje obiektu połączenia
	BOOST_CHECK_THROW(auto dummy = LoadFilesToDBTask(bad_params), std::logic_error);

	BOOST_CHECK_NO_THROW(bad_params
			.setConnection(c)
			.setFilesLoader(qfl));
	// Brakuje nazwy zestawu kwazarów.
	BOOST_CHECK_THROW(auto dummy = LoadFilesToDBTask(bad_params), std::logic_error);

	bad_params = LoadFilesToDBOptions();
	BOOST_CHECK_NO_THROW(bad_params
			.setConnection(c)
			.setQuasarSetName("qs_name"));
	// Brakuje obiektu do wczytywania.
	BOOST_CHECK_THROW(auto dummy = LoadFilesToDBTask(bad_params), std::logic_error);

	BOOST_CHECK_NO_THROW(good_params
			.setConnection(c)
			.setQuasarSetName("qs_ParamsTest1")
			.setFilesLoader(qfl));
	BOOST_CHECK_NO_THROW(auto dummy = LoadFilesToDBTask(good_params));
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ExecuteTaskTest)
{
	BOOST_MESSAGE("LoadFilesToDBTask RunTest");

	auto good_params = LoadFilesToDBOptions();
	BOOST_CHECK_NO_THROW(good_params
			.setConnection(c)
			.setQuasarSetName("qs_ExecuteTest1")
			.setFilesLoader(qfl));

	std::shared_ptr<LoadFilesToDBTask> task;
	BOOST_CHECK_NO_THROW(task.reset(new LoadFilesToDBTask(good_params)));

	// Konfiguracja zmockowanych obiektów
	// Połączenie
	MOCK_EXPECT(mc->saveQuasarSet).at_least(1);

	// QuasarFilesLoader
	// Tyle kwazarów wygenerujemy
	unsigned int quasars_number = 10000;
	MOCK_EXPECT(mqfl->getSize).returns(quasars_number);
	// Przypisanie funkcji, która będzie zwracać "wczytane" kwazary.
	MOCK_EXPECT(mqfl->loadn).at_least(1).calls(
			[&quasars_number](unsigned int n)
			{
				unsigned int k = quasars_number < n ? quasars_number : n;
				auto q = test::quasar::generateQuasars(k, 1, false);
				quasars_number -= k;
				return q;
			});

	try
	{
		task->execute();
	} catch (const std::exception& e)
	{
		BOOST_ERROR("\t" << e.what());
		common::print_exception(e);
	}
}

BOOST_AUTO_TEST_SUITE_END()
