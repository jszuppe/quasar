
#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_PARAMETERIZATION_TASK
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../mock/db.hpp"

#include "quasar/task/Parameterization/ParameterizationTask.hpp"
#include "quasar/task/Parameterization/ParameterizationOptions.hpp"

#include "quasar/common/common.hpp"

#include "quasar/data/windows.hpp"
#include "quasar/data/spectralLines.hpp"
#include "quasar/algorithm/fefitwin/FeFitWin.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#include <boost/program_options.hpp>
#include <ctime>

struct SF_PARAMETERIZATION_TASK
{
		SF_PARAMETERIZATION_TASK() :
					size(8000),
					c(std::shared_ptr<db::Connection>(new MockConnection({ MONGODB_TEST_HOST, MONGODB_TEST_PORT,
					MONGODB_TEST_DB }))),
					set(std::shared_ptr<db::QuasarSet>(new MockQuasarSet("test", time(nullptr), time(nullptr), size))),
					cursor(std::shared_ptr<db::QuasarCursor>(new MockQuasarCursor())),
					prs(std::shared_ptr<db::ParameterizationResultSet>(new MockParameterizationResultSet())),
					quasar(QuasarFactory::createQuasarFromFit("test/test_data/test.fit"))
		{
			mc = std::dynamic_pointer_cast<MockConnection>(c);
			mset = std::dynamic_pointer_cast<MockQuasarSet>(set);
			mcursor = std::dynamic_pointer_cast<MockQuasarCursor>(cursor);
			mprs = std::dynamic_pointer_cast<MockParameterizationResultSet>(prs);

			setup_mock(mc.get());

			this->feTemplate = common::file::loadFeTemplate("test/test_data/iron_emission_temp");
		}
		~SF_PARAMETERIZATION_TASK()
		{

		}

		size_t size;

		std::shared_ptr<db::Connection> c;
		std::shared_ptr<MockConnection> mc;

		std::shared_ptr<db::QuasarSet> set;
		std::shared_ptr<MockQuasarSet> mset;

		std::shared_ptr<db::QuasarCursor> cursor;
		std::shared_ptr<MockQuasarCursor> mcursor;

		std::shared_ptr<db::ParameterizationResultSet> prs;
		std::shared_ptr<MockParameterizationResultSet> mprs;


		Quasar quasar;
		FeTemplate feTemplate;
};

/**
 * Test zadania wczytującego kwazary z plików do bazy danych.
 */
BOOST_FIXTURE_TEST_SUITE(ParameterizationTaskTest, SF_PARAMETERIZATION_TASK)

/**
 *
 */
BOOST_AUTO_TEST_CASE(ExecuteTaskTest)
{
	BOOST_MESSAGE("ParameterizationTask RunTest");

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	cl_float lamp = 3000;
	ParameterizationOptions::FeFitParameters fitParameteres = { 1600.0f, 900.0f, 1.0f, { { 2200.0f, 2650.0f } }, false,
			acl::FeFitWin::Type::WIN };

	auto good_params = ParameterizationOptions();
	BOOST_CHECK_NO_THROW(good_params
			.setReadConnection(c)
			.setWriteConnection(c)
			.setAstroObjSetName("qs_ExecuteTest1")
			.setFeFitParameters(fitParameteres)
			.setAmpWavelength(lamp)
			.setContinuumWindows(data::contwinfull)
			.setFeWindows(data::fewinfull)
			.setFeTemplate(feTemplate)
			.setSpectralLines(data::spectralLines));

	std::shared_ptr<ParameterizationTask> task;
	BOOST_CHECK_NO_THROW(task.reset(new ParameterizationTask(good_params)));

	// Tyle obiektów będzie w zestawie
	size_t objs_number = size;

	// Konfiguracja zmockowanych obiektów

	// Kursor
	MOCK_EXPECT(mcursor->more).calls(
			[&objs_number]()
			{
				return objs_number != 0;
			}
			);

	MOCK_EXPECT(mcursor->next).at_least(1).calls(
			[&objs_number, this](const size_t n)
			{
				size_t k = objs_number < n ? objs_number : n;
				std::vector<Quasar> aobjs;
				for (size_t i = 0; i < k; i++)
				{
					aobjs.push_back(quasar);
				}
				objs_number -= k;
				return aobjs;
			}
			);

	// Zestaw
	MOCK_EXPECT(mset->getQuasarCursor).at_least(1).calls(
			[this]()
			{
				return cursor;
			}
			);

	// Połączenie
	MOCK_EXPECT(mc->getNewParaResultSet).exactly(1).calls(
			[this](const std::string&, const std::time_t, const db::ParameterizationResultSet::ParameterizationOptions, const std::string&)
			{
				return mprs;
			});
	MOCK_EXPECT(mprs->saveResults).at_least(1);
	MOCK_EXPECT(mc->saveParaResultSet).exactly(1);
	MOCK_EXPECT(mc->getQuasarSetByName).at_least(1).calls(
			[this](const std::string& name)
			{
				if(!name.empty())
				return set;
				return std::shared_ptr<db::QuasarSet>(nullptr);
			}
			);

	try
	{
		std::cout << "ParameterizationTaskTest:" << std::endl;
		auto execution_time = test::measure<>::execution([&]()
		{
			task->execute();
		});
		std::cout << "\texecute(), liczba obiektów: " << size << ", czas: " << execution_time << " ms = "
				<< (execution_time + 500) / 1000 << " s" << std::endl;
	} catch (const std::exception& e)
	{
		BOOST_ERROR("\t" << e.what());
		common::print_exception(e);
	}
}

BOOST_AUTO_TEST_SUITE_END()
