#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_CONTINUUM_REAL_DATA
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/continuum/Continuum.hpp"
#include "quasar/algorithm/spectools/SpecTools.hpp"
#include "quasar/algorithm/mavg/MAVG.hpp"
#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"

#include "quasar/algorithm/utils.hpp"
#include "quasar/data/windows.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_CONTINUUM_REAL_DATA
{
		SF_OCL_CONTINUUM_REAL_DATA() :
					quasar(QuasarFactory::createQuasarFromFit("test/test_data/test.fit"))
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

			this->spec = std::vector<cl_float>(*(quasar.getData()));
			this->error = std::vector<cl_float>(*(quasar.getError()));
		}
		~SF_OCL_CONTINUUM_REAL_DATA()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;

		Quasar quasar;

		std::vector<cl_float> spec;
		std::vector<cl_float> error;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(ContinuumRealDataTest, SF_OCL_CONTINUUM_REAL_DATA)

BOOST_AUTO_TEST_CASE(ContinuumRealDataTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep device_time = 0;

	size_t size = 4000;
	cl_float lamp = 3000.0f;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> errors_vec(size * QUASAR_DATA_SIZE, 0.01);
	std::vector<cl_uint> sizes_vec(size, spec.size());

	size_t i = 0;
	std::generate(specs_vec.begin(), specs_vec.end(),
			[&]()
			{
				float r = static_cast<cl_float>(i);
				if(i % QUASAR_DATA_SIZE == 0)
				{
					i = 0;
					r = static_cast<cl_float>(i);
				}
				if(i < spec.size())
				{
					r = spec[i];
				}
				i++;
				return r;
			});

	i = 0;
	std::generate(errors_vec.begin(), errors_vec.end(),
			[&]()
			{
				float r = static_cast<cl_float>(i);
				if(i % QUASAR_DATA_SIZE == 0)
				{
					i = 0;
				}
				if(i < error.size())
				{
					r = error[i];
				}
				i++;
				return r;
			});


	// Bufory
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

	acl::Continuum ctm(context, queue);
	acl::SpecTools stools(context, queue);
	acl::MAVG mavg(context, queue);
	acl::BasicMatrix bm(context, queue);

	// Generacja lambd
	{
		cl_float4 abz = { quasar.getParams()->a, quasar.getParams()->b, quasar.getParams()->z, 0.0f };
		std::vector<cl_float4> abz_(size, abz);

		auto abz_buffer = au::make_buffer(context, CL_MEM_READ_ONLY, abz_);
		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, abz_.begin(), abz_.end(), abz_buffer);
		});

		device_time += test::measure<>::execution([&]()
		{
			BOOST_CHECK_NO_THROW(stools.generateWavelengthsMatrix(abz_buffer, size, temp));
			BOOST_CHECK_NO_THROW(bm.transpose(temp, QUASAR_DATA_SIZE, size, lambdas));
			queue.finish();
		});
	}

	queue.finish();


	// Sma dla spec (i od razu transpozycja specs, bo jest jeszcze po wierszach)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, specs_vec.begin(), specs_vec.end(), specs);
		});

		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		device_time += test::measure<>::execution([&]()
		{
			BOOST_CHECK_NO_THROW(bm.transpose(specs, QUASAR_DATA_SIZE, size, temp));
			BOOST_CHECK_NO_THROW(mavg.simpleMAVG(temp, size, QUASAR_DATA_SIZE, specs, 20));
			queue.finish();
		});
	}

	queue.finish();

	//
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);

	copy_time += test::measure<>::execution([&]()
	{
		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
		cl::copy(queue, errors_vec.begin(), errors_vec.end(), temp);

		BOOST_CHECK_NO_THROW(bm.transpose(temp, QUASAR_DATA_SIZE, size, errors));
		queue.finish();
	});

	acl::Continuum::Results result;
	// Czas wykonania
	device_time += test::measure<>::execution([&]()
	{
		BOOST_CHECK_NO_THROW(result = ctm.run(specs, lambdas, errors, sizes, size, data::contwinfull, lamp));
		queue.finish();
	});

	queue.finish();

	{
		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
		queue.enqueueCopyBuffer(specs, temp, 0, 0, sizeof(cl_float) * size * QUASAR_DATA_SIZE);
		BOOST_CHECK_NO_THROW(bm.transpose(temp, size, QUASAR_DATA_SIZE, specs));

		queue.enqueueCopyBuffer(lambdas, temp, 0, 0, sizeof(cl_float) * size * QUASAR_DATA_SIZE);
		BOOST_CHECK_NO_THROW(bm.transpose(temp, size, QUASAR_DATA_SIZE, lambdas));

		queue.finish();
	}

	queue.finish();

	std::cout << "ContinuumRealDataTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\trun(), liczba: " << size << ", czas: " << device_time << " ms" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
