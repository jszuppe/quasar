#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_MAVG
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/mavg/MAVG.hpp"
#include "quasar/algorithm/utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <iterator>

std::vector<cl_float> host_sma(std::vector<cl_float> data,
		size_t width, size_t height, unsigned int window_width)
{
	std::vector<cl_float> result(width * height);
	for(size_t i = 0; i < width; i++)
	{
		size_t idx = i;
		size_t idx_end = i + width * height;
		cl_float lastSum = 0;
		for(size_t j = 0; j < window_width && idx < idx_end; j++)
		{
			lastSum += data[idx];
			result[idx] = lastSum / (static_cast<cl_float>(j+1));

			idx += width;
		}

		while(idx < idx_end)
		{
			lastSum = lastSum - data[idx-(window_width*width)] + data[idx];
			result[idx] = lastSum / static_cast<cl_float>(window_width);
			idx += width;
		}
	}
	return result;
}

struct SF_OCL_MAVG
{
		SF_OCL_MAVG()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

			namespace au = algorithm::utils;

			width = 4096;
			height = 4096;
			data = std::vector<cl_float>(width * height, 0);

			int i = 0;
			std::generate(data.begin(), data.end(),
						[&]()
						{
							i++;
							return static_cast<float>(i);
						});

			BOOST_CHECK_NO_THROW(in = au::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, width * height));
			BOOST_CHECK_NO_THROW(out = au::make_buffer<cl_float>(context, CL_MEM_WRITE_ONLY, width * height));

			cl::copy(queue, data.begin(), data.end(), in);
		}
		~SF_OCL_MAVG()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;

		cl::Buffer in;
		cl::Buffer out;

		size_t width;
		size_t height;
		std::vector<cl_float> data;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(MAVGTest, SF_OCL_MAVG)

/**
 *
 */
BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;

	BOOST_CHECK_NO_THROW(acl::MAVG sma(context, queue));
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(SmaTest)
{
	namespace acl = algorithm::ocl;
	namespace au = algorithm::utils;

	std::chrono::milliseconds::rep host_time = 0;

	acl::MAVG ma(context, queue);

	const unsigned int window = 10;
	// Wykonanie asynchroniczne
	cl::Event event;
	try
	{
		ma.simpleMAVG(in, width, height, out, window, nullptr, &event);
		queue.flush();
		event.wait();
		queue.finish();
	} catch (const cl::Error& e)
	{
		BOOST_FAIL(e.what());
	}

	std::vector<cl_float> host_results;
	host_time += test::measure<>::execution([&]()
			{
				host_results = host_sma(data, width, height, window);
			});
	std::vector<cl_float> results(width * height);
	cl::copy(queue, out, results.begin(), results.end());

	TEST_CLOSE_COLLECTION(results.begin(), results.end(), host_results.begin(), host_results.end(), 1);

	try
	{
		// Wykonanie synchroniczne
		ma.simpleMAVG(in, width, height, out, window);
		queue.flush();
		queue.finish();
	} catch (const cl::Error& e)
	{
		BOOST_FAIL(e.what());
	}

	// Czas wykonania
	double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
	time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());

	std::cout << "SMATest:" << std::endl;
	std::cout << "\tsma(), okno " << window << ", macierz: " << width << " x " << height << ", czas: " << time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_sma(), okno " << window << ", macierz: " << width << " x " << height << ", czas: " << host_time << " ms" << std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (time / 1000000)) * 100 << "%" << std::endl;
}

void event_tester(cl_event event, cl_int event_command_exec_status, void *user_data)
{
	if (event != nullptr && event_command_exec_status == CL_COMPLETE)
	{
		int* i = static_cast<int*>(user_data);
		std::cout << *i << std::endl;
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(MultipleSMATest)
{
	namespace acl = algorithm::ocl;
	namespace au = algorithm::utils;

	acl::MAVG ma(context, queue);
	const unsigned int window = 10;

	//Wykonanie synchroniczne
	try
	{
		const unsigned int tries = 10;
		std::vector<cl::Event> events(10);
		for (unsigned int i = 0; i < tries; i++)
		{
			ma.simpleMAVG(in, width, height, out, window);
			queue.flush();
			queue.finish();
		}
	} catch (const cl::Error& e)
	{
		BOOST_FAIL(e.what());
	}

	//Wykonanie asynchroniczne
	try
	{
		const unsigned int tries = 10;
		std::vector<cl::Event> events(10);
		std::vector<cl::Event> wait;

		//std::vector<int> check(200);
		//std::iota(check.begin(), check.end(), 0);

		for (unsigned int i = 0; i < tries; i++)
		{
			wait.clear();
			std::vector<cl::Event> * ptr = nullptr;
			if (i > 0)
			{
				// Sprawdzenie, czy sma wykonują się po kolei.
				// Zdarzenie w momencie zakończenia wypisze swój numer.
				// Numery powinny być kolejno od 0 do (tries-1).
				//events[i-1].setCallback(CL_COMPLETE, event_tester, static_cast<void*>(&check[i]));
				wait.push_back(events[i - 1]);
				ptr = &wait;
			}
			ma.simpleMAVG(in, width, height, out, window, ptr, &(events[i]));
			queue.flush();
		}
		cl::Event::waitForEvents(events);
	} catch (const cl::Error& e)
	{
		BOOST_FAIL(e.what() << " (Code: "<< e.err() << ")");
	}

	queue.flush();
	queue.finish();
}
BOOST_AUTO_TEST_SUITE_END()
