
#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_SPECPARAM
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/common/common.hpp"

#include "quasar/algorithm/fefitwin/FeFitWin.hpp"
#include "quasar/algorithm/continuum/Continuum.hpp"
#include "quasar/algorithm/spectools/SpecTools.hpp"
#include "quasar/algorithm/mavg/MAVG.hpp"
#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"
#include "quasar/algorithm/specparameterization/SpecParameterization.hpp"

#include "quasar/algorithm/utils.hpp"
#include "quasar/data/windows.hpp"
#include "quasar/data/spectralLines.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_SPECPARAM
{
		SF_OCL_SPECPARAM() :
					quasar(QuasarFactory::createQuasarFromFit("test/test_data/test.fit"))
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

			namespace acl = algorithm::ocl;

			this->feTemplate = common::file::loadFeTemplate("test/test_data/iron_emission_temp");
		}

		~SF_OCL_SPECPARAM()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;

		Quasar quasar;
		FeTemplate feTemplate;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(SpecParameterizationTest, SF_OCL_SPECPARAM)

BOOST_AUTO_TEST_CASE(SpecParameterizationTest)
{
	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<Quasar> aobjs;
	int size = 4000;
	for (int i = 0; i < size; i++)
	{
		aobjs.push_back(quasar);
	}

	cl_float lamp = 3000;
	acl::FeFitWin::FitParameters fitParameteres = { 1600.0f, 900.0f, 1.0f, { { 2200.0f, 2650.0f } }, false,
			acl::FeFitWin::Type::WIN };

	acl::SpecParameterization specp(context, queue);

	int64_t device_time = 0;
	int i_max = 1;
	for (int i = 0; i < i_max; i++)
	{
		device_time += test::measure<>::execution([&]()
		{
			specp.run(aobjs, data::spectralLines, data::contwinfull, lamp, data::fewinfull, feTemplate, fitParameteres);
			queue.finish();
		});
		queue.finish();

	}
	device_time /= i_max;

	std::cout << "SpecParameterizationTest:" << std::endl;
	std::cout << "\trun(), liczba: " << size << ", czas: " << device_time << " ms = " << (device_time + 500) / 1000
			<< " s" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
