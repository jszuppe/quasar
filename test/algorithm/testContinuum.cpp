#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_CONTINUUM
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/continuum/Continuum.hpp"
#include "quasar/algorithm/utils.hpp"
#include "quasar/data/windows.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_CONTINUUM
{
		SF_OCL_CONTINUUM()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		}
		~SF_OCL_CONTINUUM()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(ContinuumTest, SF_OCL_CONTINUUM)

/**
 *
 */
BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;
	BOOST_CHECK_NO_THROW(acl::Continuum ctm(context, queue));
}

BOOST_AUTO_TEST_CASE(ContinuumTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep device_time = 0;

	size_t size = 4096;
	cl_float lamp = 3000.0f;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> specs_lamba_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> errors_vec(size * QUASAR_DATA_SIZE, 0.01);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);

	float i = 999.0;
	int j = 0;
	std::generate(specs_lamba_vec.begin(), specs_lamba_vec.end(),
			[&]()
			{
				j++;
				if(j % (QUASAR_DATA_SIZE+1) == 0)
				{
					j = 1;
					i = 999.0 + ((float)rand()/(float)(RAND_MAX)) * 4000.0f;
					return i;
				}
				i += ((float)rand()/(float)(RAND_MAX)) * 1.0f;
				return i;
			});

	// Bufory
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);

	std::shared_ptr<acl::Continuum> ctm;
	BOOST_CHECK_NO_THROW(ctm.reset(new acl::Continuum(context, queue)));

	acl::Continuum::Results result;

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, specs_vec.begin(), specs_vec.end(), specs);
			cl::copy(queue, errors_vec.begin(), errors_vec.end(), errors);
			cl::copy(queue, specs_lamba_vec.begin(), specs_lamba_vec.end(), lambdas);
		});

		// Czas wykonania
		device_time += test::measure<>::execution([&]()
		{
			BOOST_CHECK_NO_THROW(result = ctm->run(specs, lambdas, errors, sizes, size, data::contwinfull, lamp));
			queue.finish();
		});
	}

	device_time /= max_i;
	copy_time /= max_i;

	std::cout << "ContinuumTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\trun(), liczba: " << size << ", czas: " << device_time << " ms" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
