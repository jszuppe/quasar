#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_FEFITWIN_REAL_DATA
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/common/common.hpp"

#include "quasar/algorithm/fefitwin/FeFitWin.hpp"
#include "quasar/algorithm/continuum/Continuum.hpp"
#include "quasar/algorithm/spectools/SpecTools.hpp"
#include "quasar/algorithm/mavg/MAVG.hpp"
#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"

#include "quasar/algorithm/utils.hpp"
#include "quasar/data/windows.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_FEFITWIN_REAL_DATA
{
		SF_OCL_FEFITWIN_REAL_DATA() :
					quasar(QuasarFactory::createQuasarFromFit("test/test_data/test.fit"))
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

			namespace acl = algorithm::ocl;

			this->spec = std::vector<cl_float>(*(quasar.getData()));
			this->error = std::vector<cl_float>(*(quasar.getError()));
			this->feTemplate = common::file::loadFeTemplate("test/test_data/iron_emission_temp");
		}
		~SF_OCL_FEFITWIN_REAL_DATA()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;

		Quasar quasar;
		FeTemplate feTemplate;

		std::vector<cl_float> spec;
		std::vector<cl_float> error;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(FeFitWinRealDataTest, SF_OCL_FEFITWIN_REAL_DATA)

BOOST_AUTO_TEST_CASE(FeFitWinRealDataTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep device_time = 0;

	// Ilość kwazarów
	size_t size = 1;
	cl_float lamp = 3000.0f;
	std::vector<cl_float2> feWindows = data::fewinfull;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> spectrumsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size, 1);
	std::vector<cl_float> wavelengthsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size, 2);
	std::vector<cl_float> errorsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size, 3);
	std::vector<cl_float> continuumsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size, 4);

	std::vector<cl_uint> sizesHost(size, spec.size());

	size_t i = 0;
	std::generate(spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(),
			[&]()
			{
				float r = static_cast<cl_float>(i);
				if(i % ASTRO_OBJ_SPEC_SIZE == 0)
				{
					i = 0;
					r = static_cast<cl_float>(i);
				}
				if(i < spec.size())
				{
					r = spec[i];
				}
				i++;
				return r;
			});

	i = 0;
	std::generate(errorsMatrixHost.begin(), errorsMatrixHost.end(),
			[&]()
			{
				float r = static_cast<cl_float>(i);
				if(i % ASTRO_OBJ_SPEC_SIZE == 0)
				{
					i = 0;
				}
				if(i < error.size())
				{
					r = error[i];
				}
				i++;
				return r;
			});

	// Bufory
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_WRITE, sizesHost);

	acl::Continuum ctm(context, queue);
	acl::FeFitWin fefit(context, queue);
	acl::Tools tools(context, queue);
	acl::SpecTools stools(context, queue);
	acl::MAVG mavg(context, queue);
	acl::BasicMatrix bm(context, queue);

	// Generacja lambd
	{
		cl_float4 abz = { quasar.getParams()->a, quasar.getParams()->b, quasar.getParams()->z, 0.0f };
		std::vector<cl_float4> abz_(size, abz);

		auto abz_buffer = au::make_buffer(context, CL_MEM_READ_ONLY, abz_);
		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, abz_.begin(), abz_.end(), abz_buffer);
		});

		device_time += test::measure<>::execution([&]()
		{
			BOOST_CHECK_NO_THROW(stools.generateWavelengthsMatrix(abz_buffer, size, temp));
			BOOST_CHECK_NO_THROW(bm.transpose(temp, ASTRO_OBJ_SPEC_SIZE, size, lambdas));

			cl::copy(queue, lambdas, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end());

		});
	}


	// Sma dla spec (i od razu transpozycja specs, bo jest jeszcze po wierszach)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(), specs);
			cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);
		});

		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		device_time += test::measure<>::execution([&]()
		{
			BOOST_CHECK_NO_THROW(bm.transpose(specs, QUASAR_DATA_SIZE, size, temp));
			BOOST_CHECK_NO_THROW(mavg.centeredMAVG(temp, size, QUASAR_DATA_SIZE, sizes, specs, 10));
			cl::copy(queue, specs, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end());
		});
	}

	//
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

	copy_time += test::measure<>::execution([&]()
	{
		auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), temp);
		BOOST_CHECK_NO_THROW(bm.transpose(temp, QUASAR_DATA_SIZE, size, errors));
		cl::copy(queue, errors, errorsMatrixHost.begin(), errorsMatrixHost.end());
	});

	// USUWANIE WARTOŚĆI, dla których błąd wynosi 0
	device_time += test::measure<>::execution([&]()
	{
		stools.filterZeros(errors, specs, lambdas, sizes, size);
		tools.countIfNotInf(specs, size, ASTRO_OBJ_SPEC_SIZE, sizes);

		tools.copyIfNotInf(errors, size, ASTRO_OBJ_SPEC_SIZE, errors, ASTRO_OBJ_SPEC_SIZE);
		tools.copyIfNotInf(lambdas, size, ASTRO_OBJ_SPEC_SIZE, lambdas, ASTRO_OBJ_SPEC_SIZE);
		tools.copyIfNotInf(specs, size, ASTRO_OBJ_SPEC_SIZE, specs, ASTRO_OBJ_SPEC_SIZE);

		cl::copy(queue, sizes, sizesHost.begin(), sizesHost.end());
		cl::copy(queue, lambdas, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end());
		cl::copy(queue, specs, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end());
		cl::copy(queue, errors, errorsMatrixHost.begin(), errorsMatrixHost.end());
	});

	acl::Continuum::Results result;
	// Czas wykonania
	device_time += test::measure<>::execution([&]()
	{
		BOOST_CHECK_NO_THROW(result = ctm.run(specs, lambdas, errors, sizes, size, data::contwinfull, lamp));
		queue.finish();
	});

	auto spectrumsMatrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto wavelengthsMatrix = lambdas;
	auto errorsMatrix = errors;

	copy_time += test::measure<>::execution([&]()
	{
		cl::copy(queue, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(), spectrumsMatrix);
		cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
		cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), errorsMatrix);
		cl::copy(queue, result.continuumsMatrix, continuumsMatrixHost.begin(), continuumsMatrixHost.end());
		cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);
		queue.finish();
	});
	queue.finish();

	acl::FeFitWin::Results feResult;
	acl::FeFitWin::Data specData = { spectrumsMatrixHost, wavelengthsMatrixHost, errorsMatrixHost, continuumsMatrixHost, sizesHost};
	acl::FeFitWin::Buffers specBuffs = { spectrumsMatrix, wavelengthsMatrix, errorsMatrix, result.continuumsMatrix, sizes };
	// Czas wykonania
	device_time += test::measure<>::execution([&]()
	{
		BOOST_CHECK_NO_THROW(feResult = fefit.run(specData, specBuffs, size, feTemplate, feWindows));
		queue.finish();
	});
	queue.finish();
}

BOOST_AUTO_TEST_SUITE_END()
