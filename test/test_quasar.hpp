#pragma once

#include "test_defines.hpp"

#include "quasar/Quasar/Quasar.hpp"

#include <memory>
#include <chrono>

namespace test
{

template<typename TimeT = std::chrono::milliseconds>
struct measure
{
		template<typename F, typename ...Args>
		static typename TimeT::rep execution(F func, Args&&... args)
		{
			auto start = std::chrono::system_clock::now();

			// Now call the function with all the parameters you need.
			func(std::forward<Args>(args)...);

			auto duration = std::chrono::duration_cast<TimeT>(std::chrono::system_clock::now() - start);

			return duration.count();
		}
};

namespace quasar
{

Quasar generateQuasar(
		const unsigned int data_size = QUASAR_TEST_DATA_NO,
		const bool random = false,
		const float value = QUASAR_TEST_DATA_VALUE);

std::vector<Quasar> generateQuasars(
		const unsigned int n,
		const unsigned int data_size = QUASAR_TEST_DATA_NO,
		const bool random = false,
		const float value = QUASAR_TEST_DATA_VALUE);

std::shared_ptr<Quasar::qparams> generateQuasarParams(const unsigned int n = 0);

std::shared_ptr<Quasar::qdata> generateQuasarData(
		const unsigned int data_size = QUASAR_TEST_DATA_NO,
		const bool random = false,
		const float value = QUASAR_TEST_DATA_VALUE);

std::shared_ptr<Quasar::qdata> generateLambda(const unsigned int data_size = QUASAR_TEST_DATA_NO);

}

}
