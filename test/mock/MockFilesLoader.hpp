#pragma once

#include "quasar/QuasarFilesLoader/QuasarFilesLoader.hpp"

#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

MOCK_BASE_CLASS(MockFilesLoader, QuasarFilesLoader)
{
	public:
		MockFilesLoader(const std::string& path) :
			QuasarFilesLoader(path)
		{

		}

		MOCK_METHOD(init, 0, void())
		MOCK_METHOD(reset, 0, void())
		MOCK_METHOD(loadn, 1, std::vector<Quasar>(unsigned long))
		MOCK_METHOD(getSize, 0, std::size_t())
		MOCK_METHOD(isInitialized, 0, bool())
	};

inline void setup_mock(MockFilesLoader* mqfl)
{
	MOCK_EXPECT(mqfl->isInitialized).returns(false);
	MOCK_EXPECT(mqfl->init).at_most(1).calls([]() {});
	MOCK_EXPECT(mqfl->reset).calls([]() {});
}
