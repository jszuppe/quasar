#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_MONGO_QUASARCURSORT
#endif
#include <boost/test/unit_test.hpp>

#include "../../test_defines.hpp"
#include "../../test_quasar.hpp"

#include "quasar/db/mongo/Connection.hpp"
#include "quasar/db/mongo/QuasarSet.hpp"
#include "quasar/db/mongo/QuasarCursor.hpp"

#include <memory>
#include <vector>

#ifdef MONGODB_TEST_LIVE

struct SF_MONGO_QUASARCURSOR
{
		SF_MONGO_QUASARCURSOR() :
					c(MONGODB_TEST_HOST, MONGODB_TEST_PORT, MONGODB_TEST_DB),
					quasars_number(100),
					data_size(10)
		{
			c.connect();
			shared_mqs = std::shared_ptr<db_mongo::QuasarSet>(
					new db_mongo::QuasarSet("qs_TEST_MONGO_QUASARSET", std::time(nullptr), std::time(nullptr), 0));
			shared_qs = std::dynamic_pointer_cast<db::QuasarSet>(shared_mqs);

			// Musi być pusto, żeby się powiodło.
			BOOST_REQUIRE(c.getQuasarSets()->empty());
			BOOST_REQUIRE(shared_mqs != nullptr);
			BOOST_REQUIRE(shared_qs != nullptr);
			BOOST_CHECK_NO_THROW(c.saveQuasarSet((shared_qs)));

			// Wszystkie kwazary mają takie same widmo
			genereted_quasars = test::quasar::generateQuasars(quasars_number, data_size, true);
			// Wstawienie do bazy danych
			BOOST_CHECK_NO_THROW(shared_qs->addQuasars(genereted_quasars));
		}

		~SF_MONGO_QUASARCURSOR()
		{
			c.getMongoConnection()->dropDatabase(MONGODB_TEST_DB);
			shared_mqs.reset();
			shared_qs.reset();
		}

		db_mongo::Connection c;
		std::shared_ptr<db_mongo::QuasarSet> shared_mqs;
		std::shared_ptr<db::QuasarSet> shared_qs;

		unsigned int quasars_number;
		unsigned int data_size;
		std::vector<Quasar> genereted_quasars;
};

/**
 * Test połączenia do bazy danych.
 * Uwaga: Baza danych musi być włączona.
 */
BOOST_FIXTURE_TEST_SUITE(MongoQuasarCursors, SF_MONGO_QUASARCURSOR)

BOOST_AUTO_TEST_CASE(NextQuasarCursor)
{
	auto qcursor = shared_qs->getQuasarCursor();
	BOOST_CHECK(qcursor->more());

	// Pobieram po jednym
	unsigned int i = 0;
	std::vector<Quasar> quasar_vec;
	while (qcursor->more())
	{
		quasar_vec = qcursor->next(1);
		i++;
	}
	BOOST_CHECK(i == quasars_number);

	// Próbuje pobrać więcej niż jest.
	qcursor = shared_qs->getQuasarCursor();
	i = 0;
	while (qcursor->more())
	{
		quasar_vec = qcursor->next(quasars_number * 2);
		i++;
	}
	BOOST_CHECK(i == 1);
	BOOST_CHECK(quasar_vec.size() == quasars_number);
	BOOST_CHECK(!qcursor->more());
}

BOOST_AUTO_TEST_SUITE_END()

#endif
