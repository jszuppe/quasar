#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_MONGO_COMMON
#endif
#include <boost/test/unit_test.hpp>

#include "../../test_defines.hpp"

#include "quasar/db/mongo/common.hpp"

/**
 * Test narzędzi.
 */
BOOST_AUTO_TEST_SUITE(MongoCommon)

BOOST_AUTO_TEST_CASE(NameSpaceBuilderTest)
{
	auto s = db_mongo::common::namespaceBuilder({ "db", "collection", "test", "test" });
	BOOST_CHECK(s.compare("db.collection.test.test") == 0);
}

BOOST_AUTO_TEST_SUITE_END()
