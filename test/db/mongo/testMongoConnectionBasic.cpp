#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_MONGO_CONNECTION_BASICS
#endif
#include <boost/test/unit_test.hpp>

#include "../../test_defines.hpp"

#include "quasar/db/mongo/Connection.hpp"
#include "quasar/common/common.hpp"

#include <boost/filesystem/path.hpp>
#include <memory>

#ifdef MONGODB_TEST_LIVE

/**
 * Test połączenia do bazy danych.
 * Uwaga: Baza danych musi być włączona.
 */
BOOST_AUTO_TEST_SUITE(MongoConnectionBasics)

/**
 * Test połączenia z bazą danych MongoDB, w tym: możliwość istnienia wielu połączeń jednocześnie.
 */
BOOST_AUTO_TEST_CASE(ConnectionTest)
{
	db_mongo::Connection c1(MONGODB_TEST_HOST, MONGODB_TEST_PORT, MONGODB_TEST_DB);
	db_mongo::Connection c2(MONGODB_TEST_HOST, MONGODB_TEST_PORT, MONGODB_TEST_DB);

	BOOST_CHECK(db_mongo::isInitialized());

	try
	{
		/**
		 * Rzuci wyjątek, jeżeli połączenie się nie uda
		 */
		c1.connect();
	} catch (db::Exception& e)
	{
		BOOST_ERROR(
				"First connection to local MongoDB test failed.\n\tException message: " << e.what() << MONGODB_TEST_LIVE_NOTE);
		common::print_exception(e);
	}
	try
	{
		/**
		 * Rzuci wyjątek, jeżeli połączenie się nie uda
		 */
		c2.connect();
	} catch (db::Exception& e)
	{
		BOOST_ERROR(
				"Second connection to local MongoDB test failed.\n\tException message: " << e.what() << MONGODB_TEST_LIVE_NOTE);
		common::print_exception(e);
	}
}

BOOST_AUTO_TEST_SUITE_END()

#endif
