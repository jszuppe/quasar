#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_MONGO_QUASARSET
#endif
#include <boost/test/unit_test.hpp>

#include "../../test_defines.hpp"
#include "../../test_quasar.hpp"

#include "quasar/db/mongo/Connection.hpp"
#include "quasar/db/mongo/QuasarSet.hpp"

#include <memory>
#include <vector>

#ifdef MONGODB_TEST_LIVE

struct SF_MONGO_QUASARSET
{
		SF_MONGO_QUASARSET() :
					c(MONGODB_TEST_HOST, MONGODB_TEST_PORT, MONGODB_TEST_DB)
		{
			c.connect();
			shared_mqs = std::shared_ptr<db_mongo::QuasarSet>(
					new db_mongo::QuasarSet("qs_TEST_MONGO_QUASARSET", std::time(nullptr), std::time(nullptr), 0));
			shared_qs = std::dynamic_pointer_cast<db::QuasarSet>(shared_mqs);

			// Musi być pusto, żeby się powiodło.
			BOOST_REQUIRE(c.getQuasarSets()->empty());
			BOOST_REQUIRE(shared_mqs != nullptr);
			BOOST_REQUIRE(shared_qs != nullptr);
			BOOST_CHECK_NO_THROW(c.saveQuasarSet((shared_qs)));
		}

		~SF_MONGO_QUASARSET()
		{
			c.getMongoConnection()->dropDatabase(MONGODB_TEST_DB);
			shared_mqs.reset();
			shared_qs.reset();
		}

		db_mongo::Connection c;
		std::shared_ptr<db_mongo::QuasarSet> shared_mqs;
		std::shared_ptr<db::QuasarSet> shared_qs;
};

/**
 * Test połączenia do bazy danych.
 * Uwaga: Baza danych musi być włączona.
 */
BOOST_FIXTURE_TEST_SUITE(MongoQuasarSet, SF_MONGO_QUASARSET)

/**
 *
 */
BOOST_AUTO_TEST_CASE(AddQuasarsTest)
{
	BOOST_MESSAGE("AddQuasarsTest");

	auto quasar_vector = test::quasar::generateQuasars(1, 10, true);

	BOOST_CHECK_NO_THROW(shared_qs->addQuasars(quasar_vector));
	BOOST_CHECK(shared_qs->getSize() == 1);
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(GetQuasarsTest)
{
	BOOST_MESSAGE("GetQuasarsTest");

	unsigned int n = 10;
	unsigned int data_size = 10;
	// Wszystkie kwazary mają takie same widmo
	auto quasars = test::quasar::generateQuasars(n, data_size, true);
	// Wstawienie do bazy danych
	BOOST_CHECK_NO_THROW(shared_qs->addQuasars(quasars));

	auto qsets = c.getQuasarSets();
	BOOST_CHECK(!qsets->empty());
	BOOST_CHECK(qsets->size() == 1);

	for (auto qset : *qsets)
	{
		//auto mqset = std::dynamic_pointer_cast<db_mongo::QuasarSet>(qset);
		std::vector<Quasar> quasars_from_db;
		BOOST_CHECK_NO_THROW(quasars_from_db = qset->getQuasars(n));
		// Czy jest tyle kwazarów ile powinno być
		BOOST_CHECK_EQUAL(quasars.size(), quasars_from_db.size());

		// No i sprawdzenie, czy pierwszy kwazar ma dobre wartości.
		// Nie polegam na kolejności, bo wszystkie kwazary mają takie same widma/
		// (Patrz generacja kwazarów powyżej; zmienna quasars.)
		auto quasars_db_iter = quasars_from_db.cbegin();
		auto quasars_iter = quasars.cbegin();
		for (unsigned int i = 0; i < data_size; i++)
		{
			BOOST_MESSAGE("\t" << quasars_db_iter->getData()->at(i));
			BOOST_CHECK_CLOSE(quasars_iter->getData()->at(i), quasars_db_iter->getData()->at(i), 0.01f);
		}
	}
}

#ifdef MONGODB_TEST_PERFORMANCE

/**
 *
 */
BOOST_AUTO_TEST_CASE(GetQuasarsPerformanceTest)
{
	BOOST_MESSAGE("GetQuasarsTest");

	unsigned int n = 40000;
	unsigned int data_size = 4096;
	BOOST_MESSAGE("\tQuasars number: " << n);
	BOOST_MESSAGE("\tData size (quasar spectrum points): " << data_size);

	// Wszystkie kwazary mają takie same widmo
	std::vector<Quasar> quasars;
	auto generationTime = test::measure<>::execution([&]()
			{
				quasars = test::quasar::generateQuasars(n, data_size, true);
			});
	BOOST_MESSAGE("\t" << "Quasars generation time: " << generationTime << " ms");

	// Wstawienie do bazy danych
	auto saveQuasarsToDatabaseTime = test::measure<>::execution([&]()
			{
				BOOST_CHECK_NO_THROW(shared_qs->addQuasars(quasars));
			});
	BOOST_MESSAGE("\t" << "Save quasar set to database: " << saveQuasarsToDatabaseTime << " ms");

	auto qsets = c.getQuasarSets();
	BOOST_CHECK(!qsets->empty());
	BOOST_CHECK(qsets->size() == 1);

	auto quasars_size = quasars.size();
	// Czyszczę żeby niepotrzebnie nie zajmować pamięci.
	quasars.clear();

	for (auto qset : *qsets)
	{
		std::vector<Quasar> quasars_from_db;
		auto getQuasarsFromDatabaseTime = test::measure<>::execution([&]()
				{
					BOOST_CHECK_NO_THROW(quasars_from_db = qset->getQuasars());
				});
		BOOST_MESSAGE("\t" << "Get all quasars (from quasar set) from database: " << getQuasarsFromDatabaseTime << " ms");

		// Czy jest tyle kwazarów ile powinno być
		BOOST_CHECK_EQUAL(quasars_size, quasars_from_db.size());
	}
}
#endif

BOOST_AUTO_TEST_SUITE_END()

#endif
