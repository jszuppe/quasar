#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_MONGO_CONNECTION
#endif
#include <boost/test/unit_test.hpp>

#include "../../test_defines.hpp"

#include "quasar/db/mongo/Connection.hpp"
#include "quasar/db/mongo/QuasarSet.hpp"
#include "quasar/db/mongo/common.hpp"
#include "quasar/common/common.hpp"

#include <memory>
#include <ctime>

/**
 *
 * @param c połączenie z MongoDB.
 * @param n liczba QuasarSet do dodania (maksymalnie 100).
 * @param name nazwy kwazarów będą wyglądać następująco: qs_<name><numer>.
 * @param start numer QuasarSet od którego zaczynamy (ogranicza ilość wstawianych QuasarSet: np. dla n = 10 i s = 8 zostaną wstawione dwa zestawy kwazarów o numerach 8 i 9).
 */
void saveSomeQuasarSets(db_mongo::Connection& c, unsigned int n = 4, const std::string name = "name",
		unsigned int start = 0)
{
	unsigned int nn = n < 100 ? n : 100;
	std::vector<std::shared_ptr<db::QuasarSet>> qss;
	for (unsigned int i = start; i < nn; i++)
	{
		qss.push_back(
				std::shared_ptr<db::QuasarSet>(
						new db_mongo::QuasarSet("qs_" + name + std::to_string(i), std::time(nullptr),
								std::time(nullptr), 0)));
	}
	for (auto& qs : qss)
	{
		c.saveQuasarSet(qs);
	}
}

#ifdef MONGODB_TEST_LIVE

struct SF
{
		SF() :
					c(MONGODB_TEST_HOST, MONGODB_TEST_PORT, MONGODB_TEST_DB)
		{
			c.connect();
		}
		~SF()
		{
			c.getMongoConnection()->dropDatabase(MONGODB_TEST_DB);
		}

		db_mongo::Connection c;
};

/**
 * Test połączenia do bazy danych.
 * Uwaga: Baza danych musi być włączona.u
 */
BOOST_FIXTURE_TEST_SUITE(MongoConnection, SF)

/**
 *
 */
BOOST_AUTO_TEST_CASE(SaveQuasarSetTest)
{
	BOOST_MESSAGE("SaveQuasarSetTest");

	auto mqs = std::shared_ptr<db_mongo::QuasarSet>(
			new db_mongo::QuasarSet("qs_SaveQuasarSetTest", std::time(nullptr), std::time(nullptr), 0));
	auto qs = std::dynamic_pointer_cast<db::QuasarSet>(mqs);

	BOOST_MESSAGE("\tQuasarSet before saveQuasarSet (BSONObj): " << mqs->getBSONObj().toString());
	BOOST_CHECK_NO_THROW(c.saveQuasarSet((qs)));
	BOOST_MESSAGE("\tQuasarSet after saveQuasarSet (BSONObj): " << mqs->getBSONObj().toString());

	auto cc = c.getMongoConnection();
	auto ns = db_mongo::common::namespaceBuilder({
	MONGODB_TEST_DB, db_mongo::_quasarset_col_name() });
	auto query = mongo::BSONObjBuilder().append("_id", mqs->getOID()).obj();
	auto obj = cc->findOne(ns, query);

	BOOST_MESSAGE("\tQuery: " << query.toString());
	BOOST_MESSAGE("\tQuasarSet (BSONObj) from database: " << obj.toString());

//		std::cout << mqs->getOID().toString() << std::endl;
//		std::time_t t = db_mongo::common::to_time_t(obj.getField("date").Date());
//		std::cout << std::asctime(std::localtime(&t));

	BOOST_CHECK(!obj.isEmpty());
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(QuasarSetNameUniqueIndexTest)
{
	BOOST_MESSAGE("QuasarSetNameUniqueIndexTest");

	// Musi być pusto, żeby się powiodło.
	BOOST_REQUIRE(c.getQuasarSets()->empty());

	// Dodaje QuasarSet
	unsigned int n = 10;
	BOOST_CHECK_NO_THROW(saveSomeQuasarSets(c, n, "QuasarSetNameUniqueIndexTest"));

	for (int i = n - 1; i > 0; i--)
	{
		try
		{
			// Sprawdzam czy rzuca.
			BOOST_CHECK_THROW(saveSomeQuasarSets(c, n, "QuasarSetNameUniqueIndexTest", i), db::Exception);
			// Sprawdzam czy rzuca wyjątek z dobrym kodem.
			saveSomeQuasarSets(c, n, "QuasarSetNameUniqueIndexTest");
		} catch (const std::exception& e)
		{
			try
			{
				std::rethrow_if_nested(e);
			} catch (const mongo::OperationException& e)
			{
				BOOST_CHECK(e.obj().getField("code").Int() == 11000);
			} catch (...)
			{

			}
		}
		n--;
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(GetQuasarSetsTest)
{
	BOOST_MESSAGE("GetQuasarSetsTest");

	// Musi być pusto, żeby się powiodło.
	BOOST_REQUIRE(c.getQuasarSets()->empty());

	// Dodaje QuasarSet do bazy danych
	unsigned int n = 14;
	BOOST_CHECK_NO_THROW(saveSomeQuasarSets(c, n, "GetQuasarSetsTest"));

	std::unique_ptr<std::vector<std::shared_ptr<db::QuasarSet>>>uvqs;
	BOOST_CHECK_NO_THROW(uvqs = c.getQuasarSets());

	BOOST_CHECK(uvqs->size() == n);
	BOOST_MESSAGE("\tuvqs->size(): " << uvqs->size() << ". Excepted " << n << ".");

	unsigned int i = 1;
	unsigned int maxPrint = 10;
	for (auto qs : *uvqs)
	{
		auto mqs = std::dynamic_pointer_cast<db_mongo::QuasarSet>(qs);
		//auto date = mqs->getDate();
		BOOST_MESSAGE("\t" << i << ": "<< mqs->getBSONObj().toString());// << " Date: " << std::ctime(&date));
		i++;
		if (i > maxPrint && n != maxPrint)
		{
			BOOST_MESSAGE("\t...");
			break;
		}
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(DeleteQuasarSetTest)
{
	BOOST_MESSAGE("DeleteQuasarSetTest");

	// Musi być pusto, żeby się powiodło.
	BOOST_REQUIRE(c.getQuasarSets()->empty());

	auto cc = c.getMongoConnection();
	auto ns = db_mongo::common::namespaceBuilder({
	MONGODB_TEST_DB, db_mongo::_quasarset_col_name() });

	// Dodaje QS do bazy danych.
	unsigned int n = 5;
	BOOST_CHECK_NO_THROW(saveSomeQuasarSets(c, n, "DeleteQuasarSetTest"));

	// Pobieram z bazy danych
	std::unique_ptr<db_mongo::Connection::QuasarSets> uvqs;
	BOOST_CHECK_NO_THROW(uvqs = c.getQuasarSets());
	BOOST_CHECK(uvqs->size() == n);

	unsigned int i = 1;
	unsigned int maxPrint = 10;
	for (auto qs : *uvqs)
	{
		auto mqs = std::dynamic_pointer_cast<db_mongo::QuasarSet>(qs);
		//auto date = mqs->getDate();
		BOOST_MESSAGE("\t" << i << ": "<< mqs->getBSONObj().toString());// << " Date: " << std::ctime(&date));
		i++;
		if (i > maxPrint && n != maxPrint)
		{
			BOOST_MESSAGE("\t...");
			break;
		}
	}

	// Usuwam wszystkie
	for (auto qs : *uvqs)
	{
		c.deleteQuasarSet(qs);
	}

	// Powinno nie być już żadnych
	uvqs = c.getQuasarSets();
	BOOST_MESSAGE("\tPozostało:"<< std::boolalpha << uvqs->empty());
	BOOST_CHECK(uvqs->empty());
}

BOOST_AUTO_TEST_SUITE_END()

#endif
