#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_DB_INTERFACE
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"

#include "quasar/db/db.hpp"
#include "quasar/db/mongo/Connection.hpp"

#include <ctime>

#ifdef MONGODB_TEST_LIVE

struct SF_DB_INTERFACE
{
		SF_DB_INTERFACE()
				: c(db::buildConnection( {
						MONGODB_TEST_HOST,
						MONGODB_TEST_PORT,
						MONGODB_TEST_DB }))
		{
			BOOST_CHECK_NO_THROW(c->connect());
			mc = std::dynamic_pointer_cast<db_mongo::Connection>(c);
			// Udał się rzut
			BOOST_CHECK(mc != nullptr);

			// Musi być pusto, żeby testy miały sens.
			BOOST_REQUIRE(c->getQuasarSets()->empty());
		}

		~SF_DB_INTERFACE()
		{
			mc->getMongoConnection()->dropDatabase(MONGODB_TEST_DB);
		}

		std::shared_ptr<db::Connection> c;
		std::shared_ptr<db_mongo::Connection> mc; // hammer
};

/**
 * Test połączenia do bazy danych.
 * Uwaga: Baza danych musi być włączona.
 */
BOOST_FIXTURE_TEST_SUITE(DBInterface, SF_DB_INTERFACE)

/**
 *
 */
	BOOST_AUTO_TEST_CASE(DBTest)
	{
		std::shared_ptr<db::QuasarSet> quasar_set;
		BOOST_CHECK_NO_THROW(quasar_set = db::buildQuasarSet("qs_TEST_MONGO_QUASARSET", std::time(nullptr), std::time(nullptr), 0));

		BOOST_CHECK_NO_THROW(c->saveQuasarSet(quasar_set));
		BOOST_CHECK(c->getQuasarSets()->size() == 1);

		unsigned int n = 10;
		BOOST_CHECK_NO_THROW(quasar_set->addQuasars(test::quasar::generateQuasars(n, QUASAR_TEST_DATA_NO, true)));
		BOOST_CHECK(quasar_set->getSize() == n);
	}

	BOOST_AUTO_TEST_SUITE_END()

#endif
