#include "test_quasar.hpp"

namespace test
{

namespace quasar
{

/**
 * Generuje kwazar.
 * @param data_size wielkość widma.
 * @param random jeżeli prawda, to wartości widma są losowe; w przeciwnym wypadku stałe. Domyślnie fałsz.
 * @param value wartość widma dla parametru random ustawionego na prawdę.
 * @return shared_ptr na kwazar.
 */
Quasar generateQuasar(const unsigned int data_size, const bool random, const float value)
{
	auto data = generateQuasarData(data_size, random, value);
	auto err = generateQuasarData(data_size, random, value);
	auto params = generateQuasarParams(0);
	return Quasar(data, err, params);
}

/**
 * Generuje kwazary.
 * @param data_size wielkość widma.
 * @param random jeżeli prawda, to wartości widma są losowe; w przeciwnym wypadku stałe. Domyślnie fałsz.
 * @param value wartość widma dla parametru random ustawionego na prawdę.
 * @return shared_ptr na wektor kwazarów.
 */
std::vector<Quasar> generateQuasars(const unsigned int n, const unsigned int data_size,
		const bool random, const float value)
{
	std::vector<Quasar> v;
	if(n > 0)
		v.reserve(n);

	if (!random)
	{
		auto quasar = generateQuasar(data_size, random, value);
		for (unsigned int i = 1; i <= n; ++i)
		{
			v.push_back(quasar);
		}
	}
	else
	{
		for (unsigned int i = 1; i <= n; ++i)
		{
			auto data = generateQuasarData(data_size, random, value);
			auto err = generateQuasarData(data_size, random, value);
			auto params = generateQuasarParams(i);
			v.push_back(Quasar(data, err, params));
		}
	}
	return v;
}

/**
 * Generuje parametry kwazaru.
 * @param n numer kwazaru. Pomocne jeżeli generujemy wiele parametrów i chcemy je odróżniać.
 * @return shared_ptr na parametry kwazaru.
 */
std::shared_ptr<Quasar::qparams> generateQuasarParams(const unsigned int n)
{
	std::shared_ptr<Quasar::qparams> params(new Quasar::qparams());

	params->name = "Generated Quasar " + std::to_string(n);
	params->type = "Type";
	params->z = float(n);
	params->ra = float(10 * n);
	params->dec = float(100 * n);
	params->mjd = n;
	params->plate = n;
	params->fiber = n;
	params->a = float(n);
	params->b = float(n);

	return params;
}

/**
 * Generuje widmo kwazaru.
 * @param data_size wielkość widma.
 * @param random jeżeli prawda, to wartości widma są losowe; w przeciwnym wypadku stałe. Domyślnie fałsz.
 * @param value stała wartość widma dla parametru random ustawionego na prawdę.
 * @return shared_ptr na widmo kwazaru, tj. na wektor liczb zmiennoprzecinkowych.
 */
std::shared_ptr<Quasar::qdata> generateQuasarData(const unsigned int data_size, const bool random, const float value)
{
	std::shared_ptr<Quasar::qdata> data(new Quasar::qdata());

	float LO = 100;
	float HI = 300;
	for (unsigned int i = 0; i < data_size; i++)
	{
		float r = LO + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (HI - LO)));
		Quasar::QFLOAT v = random ? Quasar::QFLOAT(r) : Quasar::QFLOAT(value);
		data->push_back(v);
	}
	return data;
}

/**
 * Generuje lambdę.
 * @param data_size wielkość lambdy.
 * @return shared_ptr na lambdę, tj. na wektor liczb zmiennoprzecinkowych.
 */
std::shared_ptr<Quasar::qdata> generateLambda(const unsigned int data_size)
{
	return generateQuasarData(data_size, false, 100);
}

}

}

