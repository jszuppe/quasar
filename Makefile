CC := g++
SRCEXT := cpp
HEADEXT := hpp

# libs and incs paths

OPENCL_LIB_PATH := -L/opt/AMDAPPSDK-2.9-1/lib/x86_64
OPENCL_INC_PATH := -I/opt/AMDAPPSDK-2.9-1/include

MONGO_CXX_DRIVER_LIB_PATH := 
MONGO_CXX_DRIVER_INC_PATH := -I/home/jakub/mongo-client-install/include

CCFITS_LIB_PATH := 
CCFITS_INC_PATH := 

TURTLE_INC_PATH := 

# dirs and targets

SRCDIR := src
TEST_DIR := test
BUILDDIR := build
BINDIR := bin
TARGET := $(BINDIR)/quasar
OBJECT := $(BUILDDIR)/quasar/main.o

TEST_DATA_DIR := test_data
TEST_TARGET := $(BINDIR)/test
TEST_SOURCES := $(shell find $(TEST_DIR) -type f -name *.$(SRCEXT))
TEST_OBJECTS := $(patsubst $(TEST_DIR)/%,$(BUILDDIR)/test/%,$(TEST_SOURCES:.$(SRCEXT)=.o))

# flags and build 

TEST_FLAGS := $(TURTLE_INC_PATH) # -DSTAND_ALONE # -DMONGODB_TEST_LIVE -DMONGODB_TEST_PERFORMANCE
TEST_LIB := -lboost_unit_test_framework

SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS_NO_MAIN := $(filter-out $(OBJECT),$(OBJECTS))
INC := $(foreach path,$(SRCDIR),-I $(path))

CFLAGS := -D__CL_ENABLE_EXCEPTIONS -std=c++11 -Wall -Wextra $(MONGO_CXX_DRIVER_INC_PATH) $(OPENCL_INC_PATH) $(CCFITS_INC_PATH)
LIB := -pthread \
	$(MONGO_CXX_DRIVER_LIB_PATH) -lmongoclient \
	-lboost_thread -lboost_system -lboost_iostreams -lboost_filesystem -lboost_regex -lboost_program_options \
	$(OPENCL_LIB_PATH) -lOpenCL \
	$(CCFITS_LIB_PATH) -lcfitsio -lCCfits

KERNEL_BINDIR = $(BINDIR)/cl
KERNELEXT := cl
SRCKERNELS := $(shell find $(SRCDIR) -type f -name *.$(KERNELEXT))
KERNELS_BINDIRS := $(patsubst $(SRCDIR)/quasar/%,%,$(SRCKERNELS:kernels.$(KERNELEXT)=))
BINKERNELS := $(patsubst $(SRCDIR)/quasar/%,%,$(SRCKERNELS))

all: kernels $(TARGET)

kernels: 
	@$(foreach path,$(KERNELS_BINDIRS), $(shell mkdir -p $(KERNEL_BINDIR)/$(path)))
	@$(foreach path,$(BINKERNELS), $(shell cp $(SRCDIR)/quasar/$(path) $(KERNEL_BINDIR)/$(path:kernels.$(KERNELEXT)='')))	

$(TARGET): $(OBJECTS_NO_MAIN) $(OBJECT)
	@mkdir -p $(BINDIR)
	$(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT) $(SRCDIR)/%.$(HEADEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<
	
test_rebuild: test_clean test_norun

test: kernels $(TEST_TARGET)
	$(TEST_TARGET) $(TEST_RUN_FLAGS)

test_norun: kernels $(TEST_TARGET)

$(TEST_TARGET): $(OBJECTS_NO_MAIN) $(TEST_OBJECTS)
	@mkdir -p $(BINDIR)
	$(CC) $^ -o $(TEST_TARGET) $(LIB) $(TEST_LIB)	
	
$(BUILDDIR)/test/%.o: $(TEST_DIR)/%.$(SRCEXT) $(TEST_DIR)/%.$(HEADEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(TEST_FLAGS) $(INC) -c -o $@ $<

$(BUILDDIR)/test/%.o: $(TEST_DIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(TEST_FLAGS) $(INC) -c -o $@ $<

test_clean:
	$(RM) -rf $(BUILDDIR)/test $(BINDIR)
	
clean:
	$(RM) -rf $(BUILDDIR) $(BINDIR)
	
rebuild: clean all

.PHONY: all clean test test_norun rebuild test_clean test_rebuild kernels
